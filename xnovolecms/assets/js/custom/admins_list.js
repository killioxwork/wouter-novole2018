/* ------------------------------------------------------------------------------
*
*  # Schools List
*
* ---------------------------------------------------------------------------- */
$(function() {
    // Table setup
    // ------------------------------

	// Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        stateSave: true,
		responsive:true,
        columnDefs: [
            {
                width: '180px',
                targets: 0
            },
			{
                width: '110px',
                targets: 2
            },
			{
                width: '120px',
                targets: 3
            },
            {
                orderable: false,
                width: '30px',
                targets: 4
            }
        ],
		order: [[ 0, "desc" ]],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

	// Initialize table
    var movie_library = $('.movies-list').DataTable({
        rowReorder: {
            selector: 'td.dragAndDrop',
            update: false
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$(document).on('click', '.delete-admins', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this admin!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "admins_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									movie_library.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your admin has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your admin has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your admin is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    /****************************************************************
	*  End Delete
	****************************************************************/

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function meetingOnSite(getSid, status) {
		 $.ajax({
			url: "admins_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery MUST BE delegate when there is pagination
	$(document).on('click', 'input.switchery', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function meetingOnSite
		meetingOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/

//#################################################################################################

	// Get page content & pagination after filtering
	function tableMeetingOrder(sids) {
		 $.ajax({
			url: "parts_dragdrop.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}
    // Setup event
    movie_library.on('row-reorder', function (e, diff, edit) {

		var sids = [];

		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}

	tableMeetingOrder(sids);

    //tablePhoto.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
//#################################################################################################


	 // External table additions
    // ------------------------------

    // Switchery toggle
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
	
	// Reinit swichery on click pagination
	$(document).on('click', 'a.paginate_button', function() {

		setTimeout(function(){ 		
			var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery:not([data-switchery="true"])'));
			elems.forEach(function(html) {				
			  	var switchery2 = new Switchery(html);
			});
			
		}, 50);
        
    });
	// Reinit swichery on click oder, search, pagination
	$(document).on( 'order.dt search.dt page.dt', function () {
		
		setTimeout(function(){ 		
			var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery:not([data-switchery="true"])'));		
				elems.forEach(function(html) {
				  var switchery3 = new Switchery(html);
				});
			
		}, 50);
		
	});

    // Lightbox
    $('[data-popup="lightbox"]').fancybox({
        padding: 3
    });


    // Styles checkboxes, radios
    $('.styled').uniform({
    	radioClass: 'choice'
    });


    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

	// Toggle success class
    /*$('.movies-list tbody td input[type=checkbox]').on('change', function () {
        if ($(this).is(':checked')) {
        	$(this).parents('tr').addClass('success');
            $.uniform.update();
        }
        else {
        	$(this).parents('tr').removeClass('success');
            $.uniform.update();
        }
    });*/

	// Reorder events
	/*var _td = $('.pages-list td');

	_td.each(function() {
		var $this = $(this);
		if ( $this.find('a').length ){
	 		 var hasLnk = $(this).addClass('hasLnk');
		} else if ( $this.find('i').length ) {
			var hasLnk = $(this).addClass('hasLnk');			
		} else {
			var noLnk = $(this).addClass('noLnk');
		}
	});

    var table = $('.pages-list').DataTable({
        rowReorder: {
            selector: '.noLnk'
        }
    });*/

    // Setup event
   /* table.on('row-reorder', function (e, diff, edit) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';

        for (var i=0, ien=diff.length ; i<ien ; i++) {
            var rowData = table.row( diff[i].node ).data();

            result += rowData[1]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';
        }

        //$('#event-result').html('Event result:<br>'+result);
    });*/
	
	// Add placeholder to the datatable filter option
    /*$('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });*/

});
