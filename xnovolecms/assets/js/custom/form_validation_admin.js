/* ------------------------------------------------------------------------------
*  # Form validation
* ---------------------------------------------------------------------------- */

$(function() {
	//Autocomplite off
    //$('.noAutoComplete').attr('autocomplete', 'false');

	// Select2 select - init for select box
    // ------------------------------
    $('.select').select2();

    // Checkboxes, radios
	// ------------------------------
    $(".styled").uniform({ radioClass: 'choice' });

    // File input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });
	
	// Switchery
    // ------------------------------

    // Initialize multiple switches
    if (Array.prototype.forEach) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
        elems.forEach(function(html) {
            var switchery = new Switchery(html);
        });
    }
    else {
        var elems = document.querySelectorAll('.switchery');
        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i]);
        }
    }
	

    // Form components
    // ------------------------------

    // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
	
	//Datapicker
	
	$("#date").AnyTime_picker({
        format: "%d-%m-%Y",
        firstDOW: 1
    });

    $("#startdate").AnyTime_picker({
        format: "%d-%m-%Y",
        firstDOW: 1
    });
	
    $("#enddate").AnyTime_picker({
        format: "%d-%m-%Y",
        firstDOW: 1
    });

	// Time picker
    $("#time").AnyTime_picker({
        format: "%H:%i"
    });

    $("#starttime").AnyTime_picker({
        format: "%H:%i"
    });

    // Time picker
    $("#endtime").AnyTime_picker({
        format: "%H:%i"
    });
	
	/*
	// Anytime picker
    // ------------------------------

    // Basic usage
    $("#anytime-date").AnyTime_picker({
        format: "%W, %M %D in the Year %z %E",
        firstDOW: 1
    });


    // Time picker
    $("#anytime-time").AnyTime_picker({
        format: "%H:%i"
    });


    // Display hours only
    $("#anytime-time-hours").AnyTime_picker({
        format: "%l %p"
    });


    // Date and time
    $("#anytime-both").AnyTime_picker({
        format: "%M %D %H:%i",
    });


    // Custom display format
    $("#anytime-weekday").AnyTime_picker({
        format: "%W, %D of %M, %Z"
    });


    // Numeric date
    $("#anytime-month-numeric").AnyTime_picker({
        format: "%d/%m/%Z"
    });


    // Month and day
    $("#anytime-month-day").AnyTime_picker({
        format: "%D of %M"
    });*/
	

    // Bootstrap switch
    //$(".switch").bootstrapSwitch();


    // Bootstrap multiselect
    /*$('.multiselect').multiselect({
        checkboxName: 'vali'
    });*/


    // Touchspin
    /*$(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });*/


    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    //$(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



    // Setup validation
    // ------------------------------

    // Initialize
    var validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }
			
			/*else if ( element.attr('type') === 'text' ) {
				element.parent().addClass('has-error');
				error.insertAfter(element);
			}*/

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength : 5
            },
            repass: {
                minlength : 5,
                equalTo: "#password"
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });

    // Reset form
    $('#reset').on('click', function() {
        validator.resetForm();
    });

});
