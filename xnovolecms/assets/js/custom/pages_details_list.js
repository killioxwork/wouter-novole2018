/* ------------------------------------------------------------------------------
*
*  # MOvie Details List
*
*  Specific JS code additions for movie_details.html page
*
*  Version: 1.0
*  Latest update:25.10.2016
*
* ---------------------------------------------------------------------------- */
$(function() {
	// The responsive plugin can't determine the column requirements for tables that are hidden. So after the table is shown (tab changed) the following needs to 	be called to recalculate the columns:
	$("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
	  console.log( 'show tab' );
		var table = $($.fn.dataTable.tables(true)).DataTable()
		  .columns.adjust()
		  .responsive.recalc();
	});

	// Javascript to enable link to TABS (pu on a lin hush tag like this #basic-rounded-tab6)
	var hash = document.location.hash;
	var prefix = "tab_";
	if (hash) {
		$('.nav-tabs a[href='+hash.replace(prefix,"")+']').tab('show');
	}

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function (e) {

		var hs = window.location.hash;
		hs = e.target.hash.replace("#", "#" + prefix);

	});

	if(window.location.hash) {
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}

	

// Create an array with the values of all the input boxes in a column
/*$.fn.dataTable.ext.order['dom-text'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('input', td).val();
    });
};


// Create an array with the values of all the select options in a column
$.fn.dataTable.ext.order['dom-select'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('select', td).val();
    });
};*/

	// Switchery toggle
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });

	// Lightbox
    $('[data-popup="lightbox"]').fancybox({
        padding: 3
    });

    // Table setup
    // ------------------------------

    // Initialize data table
    $('.showdates-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
        columnDefs: [
            {
                width: '100px',
                targets: 2
            }
        ],
        //order: [[ 0, "desc" ]],
        dom: '<"datatable-scroll-wrap">',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

	// Initialize data table
    $('.filters-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
		ordering: false,
        columnDefs: [
            {
                /*width: '100px',
                targets: 2*/
            }
        ],
        dom: '<"datatable-scroll-wrap">',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
	
/*
* PANORAMIC
*/
//#################################################################################################
// Reorder events for PANORAMIC VISUAL lists
/*	var _td2 = $('.panoramic-list').find('td');

	_td2.each(function() {
		var $this = $(this);
		if ( $this.find('a').length ){
	 		 var hasLnk = $(this).addClass('hasLnk');
		} else if ( $this.find('i').length ) {
			var hasLnk = $(this).addClass('hasLnk');
		} else {
			var noLnk = $(this).addClass('noLnk');
		}
	});*/
	

	// Initialize data table PANORAMIC's
    var tablePanoramic = $('.panoramic-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
		rowReorder: {
            selector: 'td.dragAndDrop',
            update: false
        },
		//order: [[ 1, "asc" ]],
        columnDefs: [
			{
                width: '15px',
                targets: 0
            },
			{
				width: '100px',
				targets: 1,
				orderable: false
			},
			{
                width: '110px',
                targets: 3
            },
			{
                width: '100px',
				orderable: false,
                targets: 4
            }
        ],
        dom: '<"datatable-scroll-wrap"t>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$('.delete-panoramic').on('click', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this image!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "panoramic_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									tablePanoramic.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your image has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your image has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your image is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    // end delete section

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function publishOnSite(getSid, status) {
		 $.ajax({
			url: "panoramic_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery
	$('.panoramic-list').find('input.switchery').on('click', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function publishOnSite
		publishOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/
	
	// Get page content & pagination after filtering
	function tablePanoramicOrder(sids) {
		 $.ajax({
			url: "panoramic_action.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);				

			}
		});

	}
    // Setup event
    tablePanoramic.on('row-reorder', function (e, diff, edit) {
		
		var sids = [];
		
		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}
		
	tablePanoramicOrder(sids);
	
    //tablePhoto.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
//#################################################################################################
/*
* PHOTOS
*/
//#################################################################################################
// Reorder events for photo lists
	/*var _td = $('.photos-list').find('td');

	_td.each(function() {
		var $this = $(this);
		if ( $this.find('a').length ){
	 		 var hasLnk = $(this).addClass('hasLnk');
		} else if ( $this.find('i').length ) {
			var hasLnk = $(this).addClass('hasLnk');
		} else {
			var noLnk = $(this).addClass('noLnk');
		}
	});*/


	// Initialize data table Foto's
    var tablePhoto = $('.photos-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
		rowReorder: {
            selector: 'td.dragAndDrop',
            update: false
        },
		//order: [[ 1, "asc" ]],
        columnDefs: [
            {
                width: '15px',
                targets: 0
            },
			{
				width: '100px',
				targets: 1
			},
            {
                width: '110px',
                targets: 3
            },
			{
                width: '100px',
                targets: 4
            }
        ],
        dom: '<"datatable-scroll-wrap"t>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$('.delete-photo').on('click', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this image!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "photo_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									tablePhoto.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your image has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your image has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your image is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    // end delete section

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function photoOnSite(getSid, status) {
		 $.ajax({
			url: "photo_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery
	$('.photos-list').find('input.switchery').on('click', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function photoOnSite
		photoOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/

	// Get page content & pagination after filtering
	function tablePhotoOrder(sids) {
		 $.ajax({
			url: "photo_action.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}
    // Setup event
    tablePhoto.on('row-reorder', function (e, diff, edit) {

		var sids = [];

		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}

	tablePhotoOrder(sids);

    //tablePhoto.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
 //#################################################################################################
/************************************************************
* DOCUMENTS
*************************************************************/
//#################################################################################################
// Reorder events for DOCUMENTS VISUAL lists
	/*var _td3 = $('.documents-list').find('td');

	_td3.each(function() {
		var $this = $(this);
		if ( $this.find('a').length ){
	 		 var hasLnk = $(this).addClass('hasLnk');
		} else if ( $this.find('i').length ) {
			var hasLnk = $(this).addClass('hasLnk');
		} else {
			var noLnk = $(this).addClass('noLnk');
		}
	});*/

	// Initialize data table DOCUMENT's
    var tableDocuments = $('.documents-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
		rowReorder: {
            selector: 'td.dragAndDrop',
			update: false
        },
		//order: [[ 1, "asc" ]],
        columnDefs: [
			{
                width: '15px',
                targets: 0
            },
			{
                width: '150px',
                targets: 2
            },
			{
                width: '150px',
                targets: 3
            },
			{
                width: '110px',
                targets: 4
            },
			{
                width: '100px',
				orderable: false,
                targets: 5
            }
        ],
        dom: '<"datatable-scroll-wrap"t>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$('.delete-document').on('click', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "documents_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									tableDocuments.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your file has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your file has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your file is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    // end delete section

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function documentOnSite(getSid, status) {
		 $.ajax({
			url: "documents_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery
	$('.documents-list').find('input.switchery').on('click', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function documentOnSite
		documentOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/

	// Get page content & pagination after filtering
	function tableDocumentsOrder(sids) {
		 $.ajax({
			url: "documents_action.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}
    // Setup event
    tableDocuments.on('row-reorder', function (e, diff, edit) {

		var sids = [];

		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}

	tableDocumentsOrder(sids);

    //tablePhoto.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
//#################################################################################################
/************************************************************
* END DOCUMENTS
*************************************************************/
/*
* LOGO
*/
//#################################################################################################
// Reorder events for photo lists

	// Initialize data table Foto's
    var tableLogo = $('.logo-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
		rowReorder: {
            selector: 'td.dragAndDrop',
            update: false
        },
		//order: [[ 1, "asc" ]],
        columnDefs: [
            {
                width: '15px',
                targets: 0
            },
			{
				width: '100px',
				targets: 1
			},
            {
                width: '110px',
                targets: 3
            },
			{
                width: '100px',
                targets: 4
            }
        ],
        dom: '<"datatable-scroll-wrap"t>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$('.delete-logo').on('click', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this image!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "logo_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									tableLogo.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your image has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your image has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your image is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    // end delete section

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function logoOnSite(getSid, status) {
		 $.ajax({
			url: "logo_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery
	$('.logo-list').find('input.switchery').on('click', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function logoOnSite
		logoOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/

	// Get page content & pagination after filtering
	function tableLogoOrder(sids) {
		 $.ajax({
			url: "logo_action.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}
    // Setup event
    tableLogo.on('row-reorder', function (e, diff, edit) {

		var sids = [];

		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}

	tableLogoOrder(sids);

    //tableLogo.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
 //#################################################################################################


/*
* SPONSORS
*/
//#################################################################################################
// Reorder events for photo lists

	// Initialize data table Foto's
    var tableSponsor = $('.sponsor-list').DataTable({
        autoWidth: false,
		responsive: true,
		paging: false,
		info: false,
        ordering: false,
		rowReorder: {
            selector: 'td.dragAndDrop',
            update: false
        },
		//order: [[ 1, "asc" ]],
        columnDefs: [
            {
                width: '15px',
                targets: 0
            },
			{
				width: '100px',
				targets: 1
			},
            {
                width: '110px',
                targets: 3
            },
			{
                width: '100px',
                targets: 4
            }
        ],
        dom: '<"datatable-scroll-wrap"t>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
		drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


	/****************************************************************
	*  Delete Action on buttons (data behavier needed or just class)
	****************************************************************/
	$('.delete-sponsor').on('click', function(e) {
			e.preventDefault();

			var _this = $(this);
			var sid = _this.closest('tr').data('sid');

			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this image!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel please!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {

					$.ajax({
						type: "GET",
						url: "sponsor_action.php?action=delete&dsid=" + sid,
						success: function(results) {

								if( results.length > 0 ) {

									swal({
										title: "Deleted!",
										text: results,
										confirmButtonColor: "#66BB6A",
										type: "success"
									});

									// Delete ROW from table
									tableSponsor.row( _this.closest('tr') ).remove().draw( false );

								} else {
									// no results or empty string
									swal({
										 title: "An error occurred",
										 text: "Your image has not been deleted.",
										 confirmButtonColor: "#66BB6A",
										 type: "error"
										});
								}

						  },
						  fail: function() {
									 swal({
									 title: "An error occurred",
									 text: "Your image has not been deleted.",
									 confirmButtonColor: "#66BB6A",
									 type: "error"
									});
						  }
				   });
				}
				else {
					swal({
						title: "Cancelled",
						text: "Your image is safe",
						confirmButtonColor: "#2196F3",
						type: "error"
					});
				}
			});
		});
    // end delete section

	/****************************************************************
	*  Publish - visibility action
	****************************************************************/
	function sponsorOnSite(getSid, status) {
		 $.ajax({
			url: "sponsor_action.php",
			method: "POST",
			data: {
				sid: getSid,
				status: status
			},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);
			}
		});
	}

	// Listen for click switchery
	$('.sponsor-list').find('input.switchery').on('click', function(e) {
		var $this = $(this);
		// check if it's selected (true/false)
		var status = this.checked;
		var getSid = $this.closest('tr').data('sid');
		// Call function sponsorOnSite
		sponsorOnSite(getSid, status);
	});


	/****************************************************************
	*  End Publish - visibility action
	****************************************************************/

	// Get page content & pagination after filtering
	function tableSponsorOrder(sids) {
		 $.ajax({
			url: "sponsor_action.php",
			method: "POST",
			data: {filterOpts: sids},
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}
    // Setup event
    tableSponsor.on('row-reorder', function (e, diff, edit) {

		var sids = [];

		for(var i = 1; i < e.target.rows.length-1; i++){
			sids.push(e.target.rows[i].dataset.sid);
		}

	tableSponsorOrder(sids);

    //tableSponsor.ajax.url("Ajax_where_you_save_new_order.php").load();


   });
 //#################################################################################################


    // External table additions
    // ------------------------------


    // Styles checkboxes, radios
    $('.styled').uniform({
    	radioClass: 'choice'
    });


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });


    // Editable text field
    $('.editPhotoInput').editable({
            type: 'text',
            url: 'photo_action.php',
            success: function(response, newValue) {
                console.log(response);
                if(response.status == 'error') return response.msg; //msg will be shown in editable form
            }
    });

    // Editable text field
    $('.editDocInput').editable({
            type: 'text',
            url: 'documents_action.php',
            success: function(response, newValue) {
                console.log(response);
                if(response.status == 'error') return response.msg; //msg will be shown in editable form
            }
    });    

    // Editable text field
    $('.editLogoInput').editable({
            type: 'text',
            url: 'logo_action.php',
            success: function(response, newValue) {
                console.log(response);
                if(response.status == 'error') return response.msg; //msg will be shown in editable form
            }
    });

    // Editable text field
    $('.editSponsorInput').editable({
            type: 'text',
            url: 'sponsor_action.php',
            success: function(response, newValue) {
                console.log(response);
                if(response.status == 'error') return response.msg; //msg will be shown in editable form
            }
    });

});

/************************************************************
* FILTERS
*************************************************************/
//#################################################################################################
var Movie = Movie || {};

Movie.Output = (function() {

	//Check selected filters and get data-name
	function selectedFilters($checkBoxList) {
		var selected = [];
		var identification;


		$checkBoxList.find('input.switchery').each(function(){

			var _self = $(this);
            identification = _self.data('idn');

			if( _self.prop('checked') === true ){

				selected.push( _self.data('name') );
			}

		});

		console.log(identification);
		console.log(selected);

		//Call ajax load page content & pagination function
		getPageContent(selected, identification);

	}


	// Get page content & pagination after filtering
	function getPageContent(selectedFilters, identification) {
		 $.ajax({
			url: "filters_parts.php",
			method: "POST",
			data: {
			    filterOpts: selectedFilters,
				filterid : identification
            },
			cache: false,
			beforeSend: function(){},
			complete: function(){},
			success: function(result) {
				console.log(result);

			}
		});

	}

	//Returns
	return {
		selectedFilters: selectedFilters
	};

}());


/*jQuery Document Ready*/
$(function() {

	// Listen for click on filter menu
	$('.filters-list').find('input.switchery').on('click', function(e) {
		//e.preventDefault();

		var $this = $(this);

		// Get All Selected checkboxes
		var $checkBoxList = $('.filters-list');
		Movie.Output.selectedFilters($checkBoxList);

	});

});
//################################################################################################# 
