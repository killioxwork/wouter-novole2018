<?php
    include_once ('../../lib/includes.inc.php');

    $ipcheck = new protection_class();

    if(!$ipcheck->white_ip() or $ipcheck->blocked_ip()){
        header("Location: ".website_root);
        exit();
    }

    require('../../smarty3/Smarty.class.php');

    $smarty = new Smarty;

    $smarty->setTemplateDir('../templates/');
    $smarty->setCompileDir('../templates_c/');
    $smarty->setConfigDir('../configs/');
    $smarty->setCacheDir('../cache/');

    $smarty->assign('siteLink', website_url);
?>