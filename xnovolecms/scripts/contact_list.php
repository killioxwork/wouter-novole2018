<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'contact-list');
    $smarty->assign('leftblock' , 'contact');

    // contact list
   #################################################################################################

    $pInfo = array();

    $i = 0;

    $aQ = db::$mysqli->query("SELECT * FROM contacts ORDER BY pageDate DESC");
    while ($aInfo = $aQ->fetch_assoc()) {

        $fullComent = $db->decodeString($aInfo['pageText']);
        $fullComent = str_replace("\r\n", "%0D%0A> ", $fullComent);
        $fullComent = str_replace("\n\r", "%0D%0A>", $fullComent);
        $fullComent = str_replace("\r", "%0D%0A>", $fullComent);
        $fullComent = str_replace("\n", "%0D%0A>", $fullComent);

        $pInfo[$i]['code'] = "RES-".$aInfo['pageID'];
        $pInfo[$i]['id'] = $aInfo['pageID'];
        $pInfo[$i]['date'] = $multifunc->showDateTimefront($aInfo['pageDate'],'/');
        $pInfo[$i]['name'] = $db->decodeString($aInfo['pageName']);
        $pInfo[$i]['email'] = $aInfo['pageMail'];
        $pInfo[$i]['phone'] = $aInfo['pagePhone'];
        $pInfo[$i]['country'] = $aInfo['pageCountry'];
        $pInfo[$i]['coment'] = $db->decodeString($aInfo['pageText']);
        $pInfo[$i]['fullcoment'] = "> ".$fullComent;
        $pInfo[$i]['status'] = $aInfo['pageRead'];

        $i++;
    }

    $smarty->assign('pInfo' , $pInfo);

   #################################################################################################

    $smarty->display("contact_list.tpl");
?>