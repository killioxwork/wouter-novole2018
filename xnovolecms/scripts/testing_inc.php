<?php
    if($_SESSION[cmscheck]!=cmscheckvalue or $_SESSION[SESSION_KEY]['adminInfo']==""
       or $_SESSION[SESSION_KEY]['adminInfo']['adminID']==0 or $_SESSION[SESSION_KEY]['adminInfo']['adminID']==""){

        header("Location: logout.php");
        exit();
    }

    $smarty->assign('admlvl' , $_SESSION[SESSION_KEY]['adminInfo']['adminLevel']);
    $smarty->assign('loginname' , $db->decodeString($_SESSION[SESSION_KEY]['adminInfo']['adminName']));

    // default language
    if($_SESSION['lang']==""){
        $_SESSION['lang'] = 1;
    }
?>