<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    // js define
    $smarty->assign('jsblock' , '');
    $smarty->assign('lang' , $_SESSION['lang']);

    $lQ = db::$mysqli->query("SELECT * FROM languages WHERE pageID='".$_SESSION['lang']."'");
    $lInfo = $lQ->fetch_assoc();

    $smarty->assign('langTitle' , $db->decodeString($lInfo['longLang']));

    $upload = new upload_class();

    $tablename = $_GET['part'];

   // documents info
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageLabel, catID
                                                                FROM pages
                                                          INNER JOIN pages_lang
                                                                  ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                               WHERE pages.pageID='%s'",
                                                                 db::$mysqli->escape_string($_SESSION['lang']),
                                                                 db::$mysqli->escape_string($_GET['pID'])));
    if($aQ->num_rows>0){
        $aInfo = $aQ->fetch_assoc();
    } else {
        header("Location: pages_list.php?part=main");
        exit();
    }

    $pInfo = array();

    // specila links
    //-----------------------------------------------------------------------------------------------//
    if($_GET['part']=='pages'){
        $smarty->assign('leftblock' , $aInfo['pageLabel']);

        if($aInfo['pageLabel']=='main') $headTitle = 'Menu pages';
        if($aInfo['pageLabel']=='solo') $headTitle = 'Independent pages';

        if($aInfo['catID']!="0"){
            $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$aInfo['catID']."'
                                                                   AND langID='".$_SESSION['lang']."'");
            $m00Info = $m00Q->fetch_assoc();
            $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);
        }
        $smarty->assign('headTitle' , $headTitle);

        $pInfo['title'] = $db->decodeString($aInfo['pageMenu']);

    } else {
        $smarty->assign('leftblock' , $_GET['part']);

        $pInfo['title'] = $db->decodeString($aInfo['pageTitle']);
    }
    //-----------------------------------------------------------------------------------------------//

    $pInfo['id'] = $aInfo['pID'];

    $smarty->assign('pInfo' , $pInfo);
   #################################################################################################

    $smarty->display("documents_".$_GET['part'].".tpl");
?>