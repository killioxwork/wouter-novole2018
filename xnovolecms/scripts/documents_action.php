<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    $upload = new upload_class();
    $backaction = new backaction_class();

    // prevent direct access
    $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
                    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    if(!$isAjax) {
        $user_error = 'Access denied - not an AJAX request...';
        trigger_error($user_error, E_USER_ERROR);
    }

    // inline title edit
   #################################################################################################
    if(isset($_POST['pk'])){
	    $sid = isset($_POST['pk'])? $_POST['pk'] : '';
	    $logotitle = isset($_POST['value'])? $_POST['value'] : '';
        if ($sid!="") {
            list($part, $partid, $docid, $lang) = preg_split("/-/",$sid);

            $pInfo = $backaction->getTableName($part);

            db::$mysqli->query("UPDATE documents SET pageTitle='".$db->encodeString($logotitle)."' WHERE pageLabel='".$pInfo['label']."'
                                                                                                     AND partID='".$partid."'
                                                                                                     AND pageID='".$docid."'
                                                                                                     AND langID='".$lang."'");
        }
    }
   #################################################################################################

    // publish
   #################################################################################################
    if(isset($_POST['sid']) and isset($_POST['status'])){
    	$sid = isset($_POST['sid'])? $_POST['sid'] : '';
    	$status = isset($_POST['status'])? $_POST['status'] : '';
        if ($sid!="" and $status!='') {
            list($part, $partid, $docid, $lang) = preg_split("/-/",$sid);
            if($status=='true') $upstatus = 1;
            if($status=='false') $upstatus = 0;

            $pInfo = $backaction->getTableName($part);

            db::$mysqli->query("UPDATE documents SET pageOnline='".$upstatus."' WHERE pageLabel='".$pInfo['label']."'
                                                                                  AND partID='".$partid."'
                                                                                  AND pageID='".$docid."'
                                                                                  AND langID='".$lang."'");
        }
    }
   #################################################################################################

    // dragdrop
   #################################################################################################
    if(isset($_POST['filterOpts'])){

        $opts = isset($_POST['filterOpts'])? $_POST['filterOpts'] : array(''); // array from filer

        if(count($opts)!=0){
            for($i=0;$i<count($opts);$i++){
                list($part, $partid, $docid, $lang) = preg_split("/-/",$opts[$i]);
                $next_seq = $i+1;

                $pInfo = $backaction->getTableName($part);

                db::$mysqli->query("UPDATE documents SET seqID='".$next_seq."' WHERE pageLabel='".$pInfo['label']."'
                                                                                 AND partID='".$partid."'
                                                                                 AND pageID='".$docid."'
                                                                                 AND langID='".$lang."'");
            }
        }
    }
   #################################################################################################

    // upload
   #################################################################################################
    if($_POST['action']=="add"){
        if($_FILES['docupload']!="" and $_POST['pID']!=''  and $_POST['part']!=''){

            sleep(1);

            //max id
            $idIDQ = db::$mysqli->query("SELECT max(pageID) as maxs FROM documents");
            $idmaxs = $idIDQ->fetch_assoc();
            $idss = $idmaxs["maxs"];
            if(!$idss) { $idss = 1; } else { $idss = $idss + 1; }

            //max seq
            $maxIDQ = db::$mysqli->query("SELECT max(seqID) as maxs FROM documents WHERE pageLabel='".$_POST['part']."'
                                                                                     AND partID='".$_POST['pID']."'
                                                                                     AND langID='".$_POST['lang']."'");
            $rsmaxs = $maxIDQ->fetch_assoc();
            $maxs = $rsmaxs["maxs"];
            if(!$maxs) { $maxs = 1; } else { $maxs = $maxs + 1; }


            // upload image
            $errormsg = 'Error!';
            if($_FILES['docupload']!=''){
                $changeName = $nameArrayInfo[$_POST['part']]."_".$idss."_".$upload->remove_accents($multifunc->removeExten($_FILES['docupload']['name']));
                $pageFile = $upload->upload_file($_FILES['docupload'], 'documents', $changeName);
                if($pageFile!='' and $pageFile!='-1'){
                    $errormsg = '';
                } else {
    		        if($pageFile=='') $errormsg = 'Upload Problem!';
                    if($pageFile=='-1') $errormsg = 'File with that name already exists';
                }
            }

            if(!$errormsg){
                $q1= db::$mysqli->query("INSERT INTO documents SET pageID='".$idss."',
                                                                   pageLabel='".$_POST['part']."',
                                                                   partID='".$_POST['pID']."',
                                                                   langID='".$_POST['lang']."',
                                                                   pageCode='".md5($idss)."',
                                                                   pageFile='".$pageFile."',
                                                                   pageSize='".$_FILES['docupload']['size']."',
                                                                   pageOnline='1',
                                                                   seqID='".$maxs."'");
                if($q1){
                    //echo '{"status":"success"}';
                    $output = [];
                    echo json_encode($output);
                    exit();
                }
            } else {
    		    //echo '{"status":"error", "errormsg": "'.$errormsg.'"}';
                $output = ['error'=>$errormsg];
                echo json_encode($output);
    		    exit();
            }

        }
    }
   #################################################################################################

    // delete
   #################################################################################################
    if($_GET['action']=="delete"){
        if ($_GET['dsid']!="") {
            list($part, $partid, $docid, $lang) = preg_split("/-/",$_GET['dsid']);

            //old info of the file
            $cQ = db::$mysqli->query(sprintf("SELECT * FROM documents WHERE pageID='%s'
                                                                        AND langID='%s'",
                                                                         db::$mysqli->escape_string($docid),
                                                                         db::$mysqli->escape_string($lang)));
            $cInfo = $cQ->fetch_assoc();

            // delete of document
            $upload->delete_file($cInfo['pageFile'], 'documents', 'true');

            $del1 = db::$mysqli->query("DELETE FROM documents WHERE pageID='".$docid."'");

            if($del1){

                $pInfo = $backaction->getTableName($part);

                // now we need to resequence the pages.
                $j=1;
                $rs = db::$mysqli->query("SELECT pageID FROM documents WHERE pageLabel='".$pInfo['label']."'
                                                                         AND partID='".$partid."'
                                                                         AND langID='".$lang."' ORDER BY seqID");
                while($row = $rs->fetch_assoc()){
                    db::$mysqli->query("UPDATE documents SET seqID='".$j."' WHERE pageID='".$row['pageID']."'");
                    $j++;
                }

                echo $cInfo['pageFile'];
                exit();
            } else {
                echo"";
                exit();
            }

        }
    }
   #################################################################################################

?>