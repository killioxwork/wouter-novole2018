<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'pages-details');

   // pages info
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageLabel, catID, pageType
                                                            FROM pages
                                                      INNER JOIN pages_lang
                                                              ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                           WHERE pages.pageID='%s'",
                                                             db::$mysqli->escape_string($_SESSION['lang']),
                                                             db::$mysqli->escape_string($_GET['pID'])));
    if($aQ->num_rows>0){
        $aInfo = $aQ->fetch_assoc();
    } else {
        header("Location: pages_list.php?part=main");
        exit();
    }

    $pInfo = array();

    //weekDay

    $pInfo['id'] = $aInfo['pID'];
    $pInfo['menu'] = $db->decodeString($aInfo['pageMenu']);
    $pInfo['type'] = $aInfo['pageType'];

    $smarty->assign('pInfo' , $pInfo);
   #################################################################################################

    $_GET['part'] = $aInfo['pageLabel'];
    $_GET['cID'] = $aInfo['catID'];

    $smarty->assign('leftblock' , $_GET['part']);
    $smarty->assign('part' , $_GET['part']);

    if($_GET['part']=='main') $headTitle = 'Menu pages';
    if($_GET['part']=='solo') $headTitle = 'Independent pages';


    if($_GET['cID']=="" or $_GET['cID']==0){
        $_GET['cID'] = 0;
    } else {
        $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$_GET['cID']."'
                                                               AND langID='".$_SESSION['lang']."'");
        $m00Info = $m00Q->fetch_assoc();
        $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);

        $smarty->assign('actionTxt' , '&cID='.$_GET['cID']);
    }

    $smarty->assign('cID' , $_GET['cID']);
    $smarty->assign('headTitle' , $headTitle);
    $smarty->assign('lang' , $_SESSION['lang']);


   // pages panoramic
   #################################################################################################
    $panInfo = array();

    $j = 0;

    $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='pages'
                                                                 AND pageSection='panoramic'
                                                                 AND partID='%s' ORDER BY seqID",
                                                                        db::$mysqli->escape_string($_GET['pID'])));
    while ($photoInfo = $photoQ->fetch_assoc()) {

        $panInfo[$j]['id'] = $photoInfo['pageID'];
        $panInfo[$j]['code'] = "PGPANO"."-".$_GET['pID']."-".$photoInfo['pageID'];
        $panInfo[$j]['status'] = $photoInfo['pageOnline'];

        // there should be two images big/small
        if($multifunc->checkImagelocation(site_files_depth."images/panoramic/column_1/", $photoInfo['pageImage'])){
             $panInfo[$j]['smallimage'] = website_url."images/panoramic/column_1/".$photoInfo['pageImage'];
             $panInfo[$j]['bigimage'] = website_url."images/panoramic/column_2/".$photoInfo['pageImage'];
        } else {
             $panInfo[$j]['smallimage'] = website_cms.cms_default_listimage;
             $panInfo[$j]['bigimage'] = website_cms.cms_default_listimage;
        }

        $j++;

    }

    $smarty->assign('panInfo' , $panInfo);
   #################################################################################################

   // pages photos
   #################################################################################################
    $phInfo = array();

    $j = 0;

    $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='pages'
                                                                 AND pageSection='photo'
                                                                 AND partID='%s' ORDER BY seqID",
                                                                        db::$mysqli->escape_string($_GET['pID'])));
    while ($photoInfo = $photoQ->fetch_assoc()) {

        $phInfo[$j]['id'] = $photoInfo['pageID'];
        $phInfo[$j]['code'] = "PG"."-".$_GET['pID']."-".$photoInfo['pageID'];
        $phInfo[$j]['status'] = $photoInfo['pageOnline'];

        $langList = "";
        $langListQ = db::$mysqli->query("SELECT longLang FROM images_lang
                                                   INNER JOIN languages
                                                           ON languages.pageID = images_lang.langID
                                                        WHERE images_lang.pageID='".$photoInfo['pageID']."'");
        while ($langInfo = $langListQ->fetch_assoc()) {
            if($langList==""){
                $langList .= $langInfo['longLang'];
            } else {
                $langList .= ", ".$langInfo['longLang'];
            }
        }

        $phInfo[$j]['lang'] = $langList;

        // there should be two images big/small
        if($multifunc->checkImagelocation(site_files_depth."images/photo/column_1/", $photoInfo['pageImage'])){
             $phInfo[$j]['smallimage'] = website_url."images/photo/column_1/".$photoInfo['pageImage'];
             $phInfo[$j]['bigimage'] = website_url."images/photo/column_2/".$photoInfo['pageImage'];
        } else {
             $phInfo[$j]['smallimage'] = website_cms.cms_default_listimage;
             $phInfo[$j]['bigimage'] = website_cms.cms_default_listimage;
        }

        $j++;
    }

    $smarty->assign('phInfo' , $phInfo);
   #################################################################################################


    $smarty->display("pages_photo.tpl");
?>