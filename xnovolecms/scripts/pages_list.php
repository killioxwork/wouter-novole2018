<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'pages-list');
    $smarty->assign('leftblock' , $_GET['part']);
    $smarty->assign('part' , $_GET['part']);

    if($_GET['part']=='main') $headTitle = 'Menu pages';
    if($_GET['part']=='solo') $headTitle = 'Independent pages';

    // language
    if($_GET['lang']!=''){
        $_SESSION['lang'] = $_GET['lang'];
    }

   // languages list
   ######################################################################
    $l = array();

    $i = 0;

    $lQ = db::$mysqli->query("SELECT * FROM languages ORDER BY seqID");
    while ($lInfo = $lQ->fetch_assoc()) {

        $l[$i]['id'] = $lInfo['pageID'];
        $l[$i]['long'] = $db->decodeString($lInfo['longLang']);
        $l[$i]['short'] = mb_strtoupper($db->decodeString($lInfo['shortLang']));

        $i++;
    }

    $smarty->assign('l' , $l);
    $smarty->assign('lang' , $_SESSION['lang']);
   ######################################################################

    if($_GET['cID']=="" or $_GET['cID']==0){
        $_GET['cID'] = 0;
    } else {
        $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$_GET['cID']."'
                                                               AND langID='".$_SESSION['lang']."'");
        $m00Info = $m00Q->fetch_assoc();
        $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);

        $smarty->assign('actionTxt' , '&cID='.$_GET['cID']);
    }

    $smarty->assign('cID' , $_GET['cID']);
    $smarty->assign('headTitle' , $headTitle);

    // pages list
   #################################################################################################

    $pInfo = array();

    $i = 0;

    $aQ = db::$mysqli->query("SELECT pages.pageID as pID, pageMenu, pageType, pages_lang.pageOnline AS pOnline, pageLabel
                                                   FROM pages
                                              LEFT JOIN pages_lang
                                                     ON pages.pageID = pages_lang.pageID AND pages_lang.langID='".$_SESSION['lang']."'
                                                  WHERE pageLabel='".$_GET['part']."'
                                                    AND catID='".$_GET['cID']."' ORDER BY seqID");

    while ($aInfo = $aQ->fetch_assoc()) {

        if($aInfo['pageLabel']=='main') $mainCode = "MM";
        if($aInfo['pageLabel']=='solo') $mainCode = "SM";

        $pInfo[$i]['code'] = $mainCode."-".$_GET['cID']."-".$aInfo['pID']."-".$_SESSION['lang'];
        $pInfo[$i]['id'] = $aInfo['pID'];
        if($aInfo['pageMenu']!=''){
            $pInfo[$i]['title'] = $db->decodeString($aInfo['pageMenu']);
        } else {
            $cQ = db::$mysqli->query("SELECT * FROM pages_lang WHERE langID='1'
                                                                 AND pageID='".$aInfo['pID']."'");
            $cInfo = $cQ->fetch_assoc();

            $pInfo[$i]['title'] = "<i>".$db->decodeString($cInfo['pageMenu'])." - enter in this langauge</i>";
        }
        $pInfo[$i]['type'] = $aInfo['pageType'];
        $pInfo[$i]['status'] = $aInfo['pOnline'];

        // test for delete the course
        $delTest = 'YES';
        $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='pages'
                                                                     AND partID='".$aInfo['pID']."'"));
        $docQ = db::$mysqli->query(sprintf("SELECT * FROM documents WHERE pageLabel='pages'
                                                                      AND partID='".$aInfo['pID']."'"));
        $subQ = db::$mysqli->query(sprintf("SELECT * FROM pages WHERE catID='".$aInfo['pID']."'"));
        if($photoQ->num_rows>0 or $docQ->num_rows>0 or $subQ->num_rows>0
           or $_SESSION[SESSION_KEY]['adminInfo']['adminLevel']>1
           or $aInfo['pageLabel']=='solo' or $aInfo['pageType']=='home'){
            $delTest = 'NO';
        }

        $pInfo[$i]['del'] = $delTest;

        $i++;
    }

    $smarty->assign('pInfo' , $pInfo);

   #################################################################################################

    $smarty->display("pages_list.tpl");
?>