<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'pages-details');

   // pages info
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageTitle, pageType,
                                             pageMenutext, pageMenucolor, pageText, pageLabel, catID
                                                            FROM pages
                                                      INNER JOIN pages_lang
                                                              ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                           WHERE pages.pageID='%s'",
                                                             db::$mysqli->escape_string($_GET['lang']),
                                                             db::$mysqli->escape_string($_GET['pID'])));
    if($aQ->num_rows>0){
        $aInfo = $aQ->fetch_assoc();
    } else {
        header("Location: pages_list.php?part=main");
        exit();
    }

    $pInfo = array();

    //weekDay

    $pInfo['id'] = $aInfo['pID'];
    $pInfo['menu'] = $db->decodeString($aInfo['pageMenu']);
    $pInfo['title'] = $db->decodeString($aInfo['pageTitle']);
    $pInfo['type'] = $aInfo['pageType'];
    $pInfo['submenu'] = $db->decodeString($aInfo['pageMenutext']);
    $pInfo['menucolor'] = $db->decodeString($aInfo['pageMenucolor']);
    $pInfo['text'] = $db->decodeString($aInfo['pageText']);

    $smarty->assign('pInfo' , $pInfo);
   #################################################################################################

    $_GET['part'] = $aInfo['pageLabel'];
    $_GET['cID'] = $aInfo['catID'];

    $smarty->assign('leftblock' , $_GET['part']);
    $smarty->assign('part' , $_GET['part']);

    if($_GET['part']=='main') $headTitle = 'Menu pages';
    if($_GET['part']=='solo') $headTitle = 'Independent pages';


    if($_GET['cID']=="" or $_GET['cID']==0){
        $_GET['cID'] = 0;
    } else {
        $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$_GET['cID']."'
                                                               AND langID='".$_SESSION['lang']."'");
        $m00Info = $m00Q->fetch_assoc();
        $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);

        $smarty->assign('actionTxt' , '&cID='.$_GET['cID']);
    }

    $smarty->assign('cID' , $_GET['cID']);
    $smarty->assign('headTitle' , $headTitle);
    $smarty->assign('lang' , $_SESSION['lang']);

    $lQ = db::$mysqli->query("SELECT * FROM languages WHERE pageID='".$_SESSION['lang']."'");
    $lInfo = $lQ->fetch_assoc();

    $smarty->assign('langTitle' , $db->decodeString($lInfo['longLang']));

   // pages documents
   #################################################################################################
    $docInfo = array();

    $j = 0;

    $docQ = db::$mysqli->query(sprintf("SELECT * FROM documents WHERE pageLabel='pages'
                                                                  AND langID='".$_SESSION['lang']."'
                                                                  AND partID='%s' ORDER BY seqID",
                                                                        db::$mysqli->escape_string($_GET['pID'])));
    while ($dInfo = $docQ->fetch_assoc()) {

        if($multifunc->checkImagelocation(site_files_depth."documents/", $dInfo['pageFile'])){
            $docInfo[$j]['id'] = $dInfo['pageID'];
            $docInfo[$j]['code'] = "PGDOC"."-".$_GET['pID']."-".$dInfo['pageID']."-".$_SESSION['lang'];
            $docInfo[$j]['name'] = $dInfo['pageFile'];
            $docInfo[$j]['size'] = $multifunc->getfilesize($dInfo['pageSize']);
            $docInfo[$j]['link'] = website_url."documents/".$dInfo['pageFile'];
            $docInfo[$j]['dlink'] = 'download.php?t=d_c&c='.$dInfo['pageCode'];
            $docInfo[$j]['status'] = $dInfo['pageOnline'];
            if($dInfo['pageTitle']!=''){
                $docInfo[$j]['title'] = $db->decodeString($dInfo['pageTitle']);
            } else {
                $docInfo[$j]['title'] = $dInfo['pageFile'];
            }

            $iconIfo = $multifunc->showIcon($dInfo['pageFile'], $iconsType);

            $docInfo[$j]['iconcode'] = $iconIfo['icon'];
            $docInfo[$j]['icontext'] = $iconIfo['text'];

            $j++;
        }

    }

    $smarty->assign('docInfo' , $docInfo);
   #################################################################################################

    $smarty->display("pages_details.tpl");
?>