<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'admins-details-form');
    $smarty->assign('leftblock' , 'admins');

    $levelArray = array('Superadmin','Admin');

   #################################################################################################
    if($_GET['action']=='add' and $_SESSION[SESSION_KEY]['adminInfo']['adminLevel']<=1){

        if($_POST['sub']=='add'){
            if($_POST['username']!="" and $_POST['password']!="" and $_POST['password']==$_POST['repass'] and $_POST['name']!=""
               and preg_match("/[[:alnum:]]{5,}/", $_POST['password']) and preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $_POST['username'])){

                if($_SESSION[SESSION_KEY]['adminInfo']['adminLevel']<2){

                    // there cannt be same names
                    $rQ = db::$mysqli->query("SELECT * FROM admin_accounts WHERE userName ='".$_POST['username']."'");
                    if($rQ->num_rows==0){

                        // password hasher
                        $hasher = new PasswordHash(8, false);
                        $hash = $hasher->HashPassword($_POST['password']);

                        if (strlen($hash) >= 20) {
                            //max id
                            $rsidqs = db::$mysqli->query("SELECT max(adminID) as maxs FROM admin_accounts");
                            $rsids = $rsidqs->fetch_assoc();
                            $ids = $rsids["maxs"];
                            if(!$ids) { $ids = 1; } else { $ids = $ids + 1; }

                            //enter data
                            $query  = "INSERT INTO admin_accounts SET adminID='".$ids."',
                                                                      adminName='".$db->encodeString($_POST['name'])."',
                                                                      userName='".$_POST['username']."',
                                                                      passWord='".$hash."',
                                                                      adminLevel='".$_POST['level']."',
                                                                      adminOnline='0'";
                            $q1= db::$mysqli->query($query);

                            $goto = "admins_details.php?pID=".$ids;

                        } else {
                            $goto = "admins_list.php";
                        }
                    } else {
                        $goto = "admins_list.php";
                    }

                } else {
                    $goto = "admins_list.php";
                }

                header("Location: ".$goto);
                exit();
            }
        }

        $smarty->assign('action' , $_GET['action']);

    }
   #################################################################################################

   // courses info - edit
   #################################################################################################
    if($_GET['action']=='edit'){
        $aQ = db::$mysqli->query(sprintf("SELECT * FROM admin_accounts WHERE adminID='%s'",
                                                                          db::$mysqli->escape_string($_GET['pID'])));
        if($aQ->num_rows>0){
            $aInfo = $aQ->fetch_assoc();
        } else {
            header("Location: admins_list.php");
            exit();
        }

        if($_POST['sub']=='edit'){
            if($_POST['ppID']!="" and $_POST['ppID']==$_GET['pID']
                and $_POST['username']!="" and preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $_POST['username'])){
                // there cannt be same names
                $rQ = db::$mysqli->query("SELECT * FROM admin_accounts WHERE userName ='".$db->encodeString($_POST['username'])."'
                                                                         AND adminID<>'".$_POST['ppID']."'");
                if($rQ->num_rows==0){

                    $extraQ = "";

                    if($_POST['password']!="" ){
                        if($_POST['password']==$_POST['repass'] and preg_match("/[[:alnum:]]{5,}/", $_POST['password'])){

                            $hasher = new PasswordHash(8, false);
                            $hash = $hasher->HashPassword($_POST['password']);

                            if (strlen($hash) >= 20) {
                                $extraQ = ", passWord='".$hash."' ";
                            }
                        }
                    }


                    $query = "UPDATE admin_accounts SET adminName='".$db->encodeString($_POST['name'])."',
                                                        userName='".$_POST['username']."',
                                                        adminLevel='".$_POST['level']."'
                                                        ".$extraQ."
                                                  WHERE adminID='".$_POST['ppID']."'";
                    $rs = db::$mysqli->query($query);

                    $goto = "admins_details.php?pID=".$_POST['ppID'];

                } else {
                    $goto = "admins_details_edit.php?action=edit&pID=".$_POST['ppID'];
                }
                header("Location: ".$goto);
                exit();
            }

        } else {
            $goto = "admins_details.php";
        }


        $smarty->assign('action' , $_GET['action']);

        $pInfo = array();

        //weekDay

        $pInfo['id'] = $aInfo['adminID'];
        $pInfo['name'] = $db->decodeString($aInfo['adminName'],'noquote');
        $pInfo['username'] = $aInfo['userName'];
        $pInfo['level'] = $aInfo['adminLevel'];

        // list of multiselects in array
        $smarty->assign('admLvl' , $levelArray);

        $smarty->assign('pInfo' , $pInfo);
    }
   #################################################################################################

    $smarty->display("admins_details_edit.tpl");
?>