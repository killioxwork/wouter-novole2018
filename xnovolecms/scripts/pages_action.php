<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    // prevent direct access
    $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
                    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    if(!$isAjax) {
        $user_error = 'Access denied - not an AJAX request...';
        trigger_error($user_error, E_USER_ERROR);
    }

    // publish
   #################################################################################################
    if(isset($_POST['sid']) and isset($_POST['status'])){
    	$sid = isset($_POST['sid'])? $_POST['sid'] : '';
    	$status = isset($_POST['status'])? $_POST['status'] : '';
        if ($sid!="" and $status!='') {
            list($part, $partid, $filterid, $lang) = preg_split("/-/",$sid);
            if($status=='true') $upstatus = 1;
            if($status=='false') $upstatus = 0;

            if($part=='MM') $mainCode = "main";
            if($part=='SM') $mainCode = "solo";

            db::$mysqli->query("UPDATE pages_lang SET pageOnline='".$upstatus."' WHERE pageID='".$filterid."'
                                                                                   AND langID='".$lang."'");
        }
    }
   #################################################################################################

    // dragdrop
   #################################################################################################
    if(isset($_POST['filterOpts'])){

        $opts = isset($_POST['filterOpts'])? $_POST['filterOpts'] : array(''); // array from filer

        if(count($opts)!=0){
            for($i=0;$i<count($opts);$i++){
                list($part, $partid, $filterid, $lang) = preg_split("/-/",$opts[$i]);
                $next_seq = $i+1;

                if($part=='MM') $mainCode = "main";
                if($part=='SM') $mainCode = "solo";

                db::$mysqli->query("UPDATE pages SET seqID='".$next_seq."' WHERE pageLabel='".$mainCode."'
                                                                             AND catID='".$partid."'
                                                                             AND pageID='".$filterid."'");
            }
        }
    }
   #################################################################################################


    //delete
   #################################################################################################
    if($_GET['action']=="delete"){
        if ($_GET['dsid']!="") {
            list($part, $partid, $docid, $lang) = preg_split("/-/",$_GET['dsid']);

            if($part=='MM') $mainCode = "main";
            if($part=='SM') $mainCode = "solo";

            //old info of the file
            $cQ = db::$mysqli->query(sprintf("SELECT * FROM pages_lang WHERE langID='".$lang."'
                                                                         AND pageID='%s'",
                                                                          db::$mysqli->escape_string($docid)));
            $cInfo = $cQ->fetch_assoc();

            $del1 = db::$mysqli->query("DELETE FROM pages WHERE pageID='".$docid."'");
            $del1 = db::$mysqli->query("DELETE FROM pages_lang WHERE pageID='".$docid."'");

            // now we need to resequence the pages.
            $j=1;
            $rs = db::$mysqli->query("SELECT pageID FROM pages WHERE pageLabel='".$mainCode."'
                                                                 AND catID='".$partid."' ORDER BY seqID");
            while($row = $rs->fetch_assoc()){
                db::$mysqli->query("UPDATE pages SET seqID='".$j."' WHERE pageID='".$row['pageID']."'");
                $j++;
            }

            if($del1){
                echo db::decodeString($cInfo['pageMenu']);
                exit();
            } else {
                echo"";
                exit();
            }

        }
    }
   #################################################################################################

?>