<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'languages-list');
    $smarty->assign('leftblock' , 'languages');

    // language list
   #################################################################################################

    $pInfo = array();

    $i = 0;

    $aQ = db::$mysqli->query("SELECT * FROM languages ORDER BY seqID");
    while ($aInfo = $aQ->fetch_assoc()) {

        $pInfo[$i]['code'] = "LANG-".$aInfo['pageID'];
        $pInfo[$i]['id'] = $aInfo['pageID'];
        $pInfo[$i]['title'] = $db->decodeString($aInfo['longLang']);
        $pInfo[$i]['short'] = $db->decodeString($aInfo['shortLang']);
        $pInfo[$i]['status'] = $aInfo['pageOnline'];

        // test for delete the course
        $delTest = 'YES';
        $langQ = db::$mysqli->query(sprintf("SELECT * FROM pages_lang WHERE langID='".$aInfo['pageID']."'"));
        if($horsesQ->num_rows>0 or $_SESSION[SESSION_KEY]['adminInfo']['adminLevel']>1){
            $delTest = 'NO';
        }

        $pInfo[$i]['del'] = $delTest;

        $i++;
    }

    $smarty->assign('pInfo' , $pInfo);

   #################################################################################################

    $smarty->display("languages_list.tpl");
?>