<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    // js define
    if($_GET['set']=='crop'){
        $smarty->assign('jsblock' , 'crop-photo');
        $template = "photo_".$_GET['part']."_crop.tpl";
    } elseif($_GET['set']=='add' or $_GET['set']=='edit'){
        $smarty->assign('jsblock' , 'global-form');
        $template = "photo_".$_GET['part']."_edit.tpl";
    } else {
        $smarty->assign('jsblock' , '');
        $template = "photo_".$_GET['part'].".tpl";
    }

    $smarty->assign('lang' , $_SESSION['lang']);

    $upload = new upload_class();

    $tablename = $_GET['part'];

    $smarty->assign('imageMinSize' , $imageMinSize);
    $smarty->assign('set' , $_GET['set']);

   // page info
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageLabel, catID, pageType
                                                                FROM pages
                                                          INNER JOIN pages_lang
                                                                  ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                               WHERE pages.pageID='%s'",
                                                                 db::$mysqli->escape_string($_SESSION['lang']),
                                                                 db::$mysqli->escape_string($_GET['pID'])));
    if($aQ->num_rows>0){
        $aInfo = $aQ->fetch_assoc();
    } else {
        header("Location: pages_list.php?part=main");
        exit();
    }

    $pInfo = array();

     // specila links
    if($_GET['part']=='pages'){
        $smarty->assign('leftblock' , $aInfo['pageLabel']);

        if($aInfo['pageLabel']=='main') $headTitle = 'Menu pages';
        if($aInfo['pageLabel']=='solo') $headTitle = 'Independent pages';

        if($aInfo['catID']!="0"){
            $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$aInfo['catID']."'
                                                                   AND langID='".$_SESSION['lang']."'");
            $m00Info = $m00Q->fetch_assoc();
            $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);
        }
        $smarty->assign('headTitle' , $headTitle);

        $pInfo['title'] = $db->decodeString($aInfo['pageMenu']);

    } else {
        $smarty->assign('leftblock' , $_GET['part']);

        $pInfo['title'] = $db->decodeString($aInfo['pageTitle']);
    }

    $pInfo['id'] = $aInfo['pID'];

    list($minWidth, $minHeight) = explode("x", $imageMinSize);
    $smarty->assign('minWidth' , $minWidth);
    $smarty->assign('minHeight' , $minHeight);

    $smarty->assign('pInfo' , $pInfo);
   #################################################################################################


   // croper
   #################################################################################################
    if($_POST['action']=="crop"){

        //old info of the object
        $cQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageID='%s'",
                                                                  db::$mysqli->escape_string($_POST['cID'])));
        $cInfo = $cQ->fetch_assoc();

        $result = $upload->create_with_coordinates(site_files_depth.'images/photo/'.$cInfo['pageImage'],
                                                   site_files_depth.'images/photo/column_'.$_POST['size'].'/'.$cInfo['pageImage'],
                                                   $_POST['x'], $_POST['y'], $_POST['width'], $_POST['height']);

        // copy and resize
        for($i=1;$i<=count($imageArrayinfo);$i++){
            list($width, $height) = explode("x", $imageArrayinfo['column_'.$i]);
            try {
                $resizeclass = new abeautifulsite\SimpleImage(site_files_depth.'images/photo/column_'.$_POST['size'].'/'.$cInfo['pageImage']);
                if($width!='' and $height!=''){
                    $resizeclass->adaptive_resize($width,$height)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                } elseif($height==''){
                    $resizeclass->fit_to_width($width)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                } elseif($width==''){
                    $resizeclass->fit_to_height($height)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                }
                $errormsg = '';
            } catch(Exception $e) {
                $errormsg = 'Error: ' . $e->getMessage();
            }
        }

/*        list($width, $height) = explode("x", $imageArrayinfo['column_'.$_POST['size']]);
        try {
            $resizeclass = new abeautifulsite\SimpleImage(site_files_depth.'images/photo/column_'.$_POST['size'].'/'.$cInfo['pageImage']);
            $resizeclass->adaptive_resize($width,$height)->save(site_files_depth.'images/photo/column_'.$_POST['size'].'/'.$cInfo['pageImage'], 95);

            if($_POST['size']==2){
                list($width_small, $height_small) = explode("x", $imageArrayinfo['column_1']);
                $resizeclass = new abeautifulsite\SimpleImage(site_files_depth.'images/photo/column_'.$_POST['size'].'/'.$cInfo['pageImage']);
                $resizeclass->adaptive_resize($width_small,$height_small)->save(site_files_depth.'images/photo/column_1/'.$cInfo['pageImage'], 95);
            }
        } catch(Exception $e) {
            //echo 'Error: ' . $e->getMessage();
        }*/

    }
   #################################################################################################

   // adding video
   #################################################################################################
    if($_POST['action']=="edit"){
        if($_POST['pID']!='' and $_POST['part']!='' and $_POST['cID']!=''){

            $phtestQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageID='%s'",
                                                                           db::$mysqli->escape_string($_POST['cID'])));
            if($phtestQ->num_rows>0){

                $langListQ = db::$mysqli->query("SELECT languages.pageID AS langID, pageTitle
                                                                                       FROM languages
                                                                                  LEFT JOIN images_lang
                                                                                         ON languages.pageID = images_lang.langID
                                                                                            AND images_lang.pageID = '".$_POST['cID']."'
                                                                                   ORDER BY languages.seqID");
                while ($lInfo = $langListQ->fetch_assoc()) {

                    if($lInfo['pageTitle']!=''){
                        if($_POST['text_'.$lInfo['langID']]!=''){
                            $q1= db::$mysqli->query("UPDATE images_lang SET pageTitle='".$db->encodeString($_POST['text_'.$lInfo['langID']])."'
                                                                      WHERE langID='".$lInfo['langID']."'
                                                                        AND pageID='".$_POST['cID']."'");
                        } else {
                            $del1 = db::$mysqli->query("DELETE FROM images_lang WHERE langID='".$lInfo['langID']."'
                                                                                  AND pageID='".$_POST['cID']."'");
                        }
                    } else {
                        if($_POST['text_'.$lInfo['langID']]!=''){
                            $q1= db::$mysqli->query("INSERT INTO images_lang SET pageTitle='".$db->encodeString($_POST['text_'.$lInfo['langID']])."',
                                                                                 langID='".$lInfo['langID']."',
                                                                                 pageID='".$_POST['cID']."'");
                        }                                                         
                    }
                }

                header("Location: ".$_POST['part']."_photo.php?pID=".$_POST['pID']."#basic-rounded-tab5");
                exit();
            }
        }
    }
   #################################################################################################


   //image info
   #################################################################################################
    $panInfo = array();

    $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='".$_GET['part']."'
                                                                 AND pageSection='photo'
                                                                 AND partID='".$aInfo['pID']."'
                                                                 AND pageID='%s'",
                                                                  db::$mysqli->escape_string($_GET['crID'])));
    if($photoQ->num_rows>0){

        $photoInfo = $photoQ->fetch_assoc();

        if($multifunc->checkImagelocation(site_files_depth."images/photo/column_1/", $photoInfo['pageImage'])){

            $panInfo['old'] = website_url."images/photo/column_2/".$photoInfo['pageImage']."?dammy=".date("YmdHis");
            $panInfo['orginal'] = website_url."images/photo/".$photoInfo['pageImage'];

        }

    }

    $panInfo['pID'] = $_GET['pID'];
    $panInfo['crID'] = $_GET['crID'];

    $smarty->assign('panInfo' , $panInfo);

    $langInfo = array();

    $i = 0;

    $langListQ = db::$mysqli->query("SELECT languages.pageID AS langID, pageTitle, longLang
                                                                                  FROM languages
                                                                             LEFT JOIN images_lang
                                                                                    ON languages.pageID = images_lang.langID
                                                                                       AND images_lang.pageID = '".$_GET['crID']."'
                                                                              ORDER BY languages.seqID");
    while ($lInfo = $langListQ->fetch_assoc()) {
        $langInfo[$i]['langID'] = $lInfo['langID'];
        $langInfo[$i]['lang'] = $db->decodeString($lInfo['longLang']);
        $langInfo[$i]['text'] = $db->decodeString($lInfo['pageTitle']);

        $i++;
    }

    $smarty->assign('langInfo' , $langInfo);

   #################################################################################################


    $smarty->display($template);
?>