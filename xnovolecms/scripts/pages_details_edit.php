<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'global-form');
    $smarty->assign('leftblock' , $_GET['part']);
    $smarty->assign('part' , $_GET['part']);

    if($_GET['part']=='main') $headTitle = 'Menu pages';
    if($_GET['part']=='solo') $headTitle = 'Independent pages';

    if($_GET['cID']=="" or $_GET['cID']==0){
        $_GET['cID'] = 0;
    } else {
        $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$_GET['cID']."'
                                                               AND langID='".$_SESSION['lang']."'");
        $m00Info = $m00Q->fetch_assoc();
        $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);

        $smarty->assign('actionTxt' , '&cID='.$_GET['cID']);
    }

    $smarty->assign('cID' , $_GET['cID']);
    $smarty->assign('headTitle' , $headTitle);
    $smarty->assign('lang' , $_SESSION['lang']);


   #################################################################################################
    if($_GET['action']=='add'){

        if($_POST['sub']=='add'){
            if($_POST['menu']!="" and $_POST['pID']=='' and $_POST['lang']==1){
                //max id
                $rsidqs = db::$mysqli->query("SELECT max(pageID) as maxs FROM pages");
                $rsids = $rsidqs->fetch_assoc();
                $ids = $rsids["maxs"];
                if(!$ids) { $ids = 1; } else { $ids = $ids + 1; }

                //max seq
                $maxIDQ = db::$mysqli->query("SELECT max(seqID) as maxs FROM pages WHERE pageLabel='".$_GET['part']."'
                                                                                     AND catID='".$_GET['cID']."'");
                $rsmaxs = $maxIDQ->fetch_assoc();
                $maxs = $rsmaxs["maxs"];
                if(!$maxs) { $maxs = 1; } else { $maxs = $maxs + 1; }

                //insert title
                $query  = "INSERT INTO pages SET pageID='".$ids."',
                                                 pageLabel='".$_GET['part']."',
                                                 catID='".$_GET['cID']."',
                                                 pageType='".$_POST['type']."',
                                                 pageMenucolor='".$db->encodeString($_POST['menucolor'])."',
                                                 seqID='".$maxs."'";
                $q1= db::$mysqli->query($query);

                $query  = "INSERT INTO pages_lang SET pageID='".$ids."',
                                                      langID='".$_POST['lang']."',
                                                      pageMenu='".$db->encodeString($_POST['menu'])."',
                    	                              pageTitle='".$db->encodeString($_POST['title'])."',
                                                      pageMenutext='".$db->encodeString($_POST['submenu'])."',
                                                      pageText='".$db->encodeString($_POST['ptext'])."',
                    	                              pageOnline='0'";
                $q2= db::$mysqli->query($query);

                $goto = "pages_details.php?pID=".$ids."&lang=".$_POST['lang'];
                header("Location: ".$goto);
                exit();
            }
            if($_POST['menu']!="" and $_POST['ppID']!='' and $_POST['lang']!=''){
                //insert title
                $query  = "INSERT INTO pages_lang SET pageID='".$_POST['ppID']."',
                                                      langID='".$_POST['lang']."',
                                                      pageMenu='".$db->encodeString($_POST['menu'])."',
                    	                              pageTitle='".$db->encodeString($_POST['title'])."',
                                                      pageMenutext='".$db->encodeString($_POST['submenu'])."',
                                                      pageText='".$db->encodeString($_POST['ptext'])."',
                    	                              pageOnline='0'";
                $q2= db::$mysqli->query($query);

                $goto = "pages_details.php?pID=".$_POST['ppID']."&lang=".$_POST['lang'];
                header("Location: ".$goto);
                exit();
            }
        }

        $lQ = db::$mysqli->query("SELECT * FROM languages WHERE pageID='".$_SESSION['lang']."'");
        $lInfo = $lQ->fetch_assoc();

        $smarty->assign('action' , $_GET['action']);
        $smarty->assign('langTitle' , $db->decodeString($lInfo['longLang']));

        $aQ = db::$mysqli->query(sprintf("SELECT * FROM pages_lang WHERE langID=1
                                                                     AND pageID='%s'",
                                                                      db::$mysqli->escape_string($_GET['pID'])));
        if($aQ->num_rows>0){
            $aInfo = $aQ->fetch_assoc();

            //weekDay

            $pInfo['id'] = $aInfo['pageID'];
            $pInfo['menu'] = $db->decodeString($aInfo['pageMenu'],'noquote');

            $smarty->assign('pInfo' , $pInfo);
        }

    }
   #################################################################################################

   // pages - edit
   #################################################################################################
    if($_GET['action']=='edit'){
        $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageTitle, pageType,
                                                 pageMenutext, pageMenucolor, pageText, pageLabel, catID
                                                                FROM pages
                                                          INNER JOIN pages_lang
                                                                  ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                               WHERE pages.pageID='%s'",
                                                                 db::$mysqli->escape_string($_GET['lang']),
                                                                 db::$mysqli->escape_string($_GET['pID'])));
        if($aQ->num_rows>0){
            $aInfo = $aQ->fetch_assoc();
        } else {
            header("Location: pages_list.php?part=main");
            exit();
        }

        $extraQ = "";
        if($_SESSION[SESSION_KEY]['adminInfo']['adminLevel']==0){
            $extraQ .= ", pageType='".$_POST['type']."' ";
        }

        if($_POST['sub']=='edit'){
            if($_POST['menu']!="" and $_POST['ppID']!="" and $_POST['ppID']==$_GET['pID']){

                //insert title
                $query  = "UPDATE pages_lang SET pageMenu='".$db->encodeString($_POST['menu'])."',
                                                 pageTitle='".$db->encodeString($_POST['title'])."',
                                                 pageMenutext='".$db->encodeString($_POST['submenu'])."',
                                                 pageText='".$db->encodeString($_POST['ptext'])."'
                                           WHERE pageID='".$_POST['ppID']."'
                                             AND langID='".$_POST['lang']."'";

                $q1= db::$mysqli->query($query);

                //insert title
                $query  = "UPDATE pages SET pageMenucolor='".$db->encodeString($_POST['menucolor'])."'
                                           ".$extraQ."
                                      WHERE pageID='".$_POST['ppID']."'";

                $q2= db::$mysqli->query($query);

                $goto = "pages_details.php?pID=".$_POST['ppID']."&lang=".$_POST['lang'];
                header("Location: ".$goto);
                exit();
            }
        }
        $lQ = db::$mysqli->query("SELECT * FROM languages WHERE pageID='".$_SESSION['lang']."'");
        $lInfo = $lQ->fetch_assoc();

        $smarty->assign('action' , $_GET['action']);
        $smarty->assign('langTitle' , $db->decodeString($lInfo['longLang']));

        $pInfo = array();

        //weekDay

        $pInfo['id'] = $aInfo['pID'];
        $pInfo['menu'] = $db->decodeString($aInfo['pageMenu'],'noquote');
        $pInfo['title'] = $db->decodeString($aInfo['pageTitle'],'noquote');
        $pInfo['type'] = $aInfo['pageType'];
        $pInfo['submenu'] = $db->decodeString($aInfo['pageMenutext']);
        $pInfo['menucolor'] = $db->decodeString($aInfo['pageMenucolor']);
        $pInfo['text'] = $db->decodeString($aInfo['pageText']);

        $smarty->assign('pInfo' , $pInfo);
    }
   #################################################################################################

    $smarty->display("pages_details_edit.tpl");
?>