<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    $upload = new upload_class();
    $backaction = new backaction_class();

    // prevent direct access
    $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
                    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    if(!$isAjax) {
        $user_error = 'Access denied - not an AJAX request...';
        trigger_error($user_error, E_USER_ERROR);
    }

    // inline url edit
   #################################################################################################
    if(isset($_POST['pk'])){
	    $sid = isset($_POST['pk'])? $_POST['pk'] : '';
	    $editcode = isset($_POST['value'])? $_POST['value'] : '';
        if ($sid!="") {
            list($part, $partid, $photoid) = preg_split("/-/",$sid);

            $pInfo = $backaction->getTableName($part);

            db::$mysqli->query("UPDATE images SET pageVideocode='".$editcode."' WHERE pageLabel='".$pInfo['label']."'
                                                                                  AND pageSection='photo'
                                                                                  AND partID='".$partid."'
                                                                                  AND pageID='".$photoid."'");
        }
    }
   #################################################################################################

    // publish
   #################################################################################################
    if(isset($_POST['sid']) and isset($_POST['status'])){
    	$sid = isset($_POST['sid'])? $_POST['sid'] : '';
    	$status = isset($_POST['status'])? $_POST['status'] : '';
        if ($sid!="" and $status!='') {
            list($part, $partid, $panoid) = preg_split("/-/",$sid);
            if($status=='true') $upstatus = 1;
            if($status=='false') $upstatus = 0;

            $pInfo = $backaction->getTableName($part);

            db::$mysqli->query("UPDATE images SET pageOnline='".$upstatus."' WHERE pageLabel='".$pInfo['label']."'
                                                                               AND pageSection='photo'
                                                                               AND partID='".$partid."'
                                                                               AND pageID='".$panoid."'");
        }
    }
   #################################################################################################

    // dragdrop
   #################################################################################################
    if(isset($_POST['filterOpts'])){

        $opts = isset($_POST['filterOpts'])? $_POST['filterOpts'] : array(''); // array from filer

        if(count($opts)!=0){
            for($i=0;$i<count($opts);$i++){
                list($part, $partid, $panoid) = preg_split("/-/",$opts[$i]);
                $next_seq = $i+1;

                $pInfo = $backaction->getTableName($part);

                db::$mysqli->query("UPDATE images SET seqID='".$next_seq."' WHERE pageLabel='".$pInfo['label']."'
                                                                              AND pageSection='photo'
                                                                              AND partID='".$partid."'
                                                                              AND pageID='".$panoid."'");
            }
        }
    }
   #################################################################################################

    // upload
   #################################################################################################
    if($_POST['action']=="add"){
        if($_FILES['imageupload']!="" and $_POST['pID']!='' and $_POST['part']!=''){

            sleep(1);

            //max id
            $idIDQ = db::$mysqli->query("SELECT max(pageID) as maxs FROM images");
            $idmaxs = $idIDQ->fetch_assoc();
            $idss = $idmaxs["maxs"];
            if(!$idss) { $idss = 1; } else { $idss = $idss + 1; }

            //max seq
            $maxIDQ = db::$mysqli->query("SELECT max(seqID) as maxs FROM images WHERE pageLabel='".$_POST['part']."'
                                                                                  AND pageSection='photo'
                                                                                  AND partID='".$_POST['pID']."'");
            $rsmaxs = $maxIDQ->fetch_assoc();
            $maxs = $rsmaxs["maxs"];
            if(!$maxs) { $maxs = 1; } else { $maxs = $maxs + 1; }

            // upload image
            $errormsg = 'Error!';
            if($_FILES['imageupload']!=''){
                if($upload->check_image_dimension($_FILES['imageupload']['tmp_name'], $imageMinSize, 'fitsize')){
                    $changeName = $nameArrayInfo[$_POST['part']]."_".$idss."_".date("His");
                    $pageImage = $upload->upload_image($_FILES['imageupload'], 'images/photo', $changeName, $imageMinSize, 'fitsize');
                    if($pageImage!='' and $pageImage!='-1' and $pageImage!='-2'){
                        // extra step - if bigger then maxsize resize
                        if($upload->upload_image_maxsize($pageImage, 'images/photo', $imageMaxSize)){
                            list($maxwidth, $maxheight) = explode("x", $imageMaxSize);
                            $resizeclassfit = new abeautifulsite\SimpleImage(site_files_depth.'images/photo/'.$pageImage);
                            $resizeclassfit->best_fit($maxwidth,$maxheight)->save(site_files_depth.'images/photo/'.$pageImage, 95);
                        }
                        // copy and resize
                        for($i=1;$i<=count($imageArrayinfo);$i++){
                            list($width, $height) = explode("x", $imageArrayinfo['column_'.$i]);
                            try {
                                $resizeclass = new abeautifulsite\SimpleImage(site_files_depth.'images/photo/'.$pageImage);
                                if($width!='' and $height!=''){
                                    $resizeclass->adaptive_resize($width,$height)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                                } elseif($height==''){
                                    $resizeclass->fit_to_width($width)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                                } elseif($width==''){
                                    $resizeclass->fit_to_height($height)->save(site_files_depth.'images/photo/column_'.$i.'/'.$pageImage, 95);
                                }
                                $errormsg = '';
                            } catch(Exception $e) {
                                $errormsg = 'Error: ' . $e->getMessage();
                            }
                        }
                    } else {
        		        if($pageImage=='') $errormsg = 'Upload Problem!';
                        if($pageImage=='-1') $errormsg = 'Image with that name already exists';
                        if($pageImage=='-2') $errormsg = 'Selected image '.$_FILES['imageupload']['name'].' is smaller then requested';
                    }
                } else {
                    $errormsg = 'The image: '.$_FILES['imageupload']['name'].' does not meet the minimum image size as specified below.';
                }
            }

            if(!$errormsg){
                $q1= db::$mysqli->query("INSERT INTO images SET pageLabel='".$_POST['part']."',
                                                                pageSection='photo',
                                                                partID='".$_POST['pID']."',
                                                                pageImage='".$pageImage."',
                                                                pageOnline='1',
                                                                seqID='".$maxs."'");

                if($q1){
                    //echo '{"status":"success"}';
                    $output = [];
                    echo json_encode($output);
                    exit();
                }
            } else {
    		    //echo '{"status":"error", "errormsg": "'.$errormsg.'"}';
                $output = ['error'=>$errormsg];
                echo json_encode($output);
    		    exit();
            }

        }
    }
   #################################################################################################

    // delete
   #################################################################################################
    if($_GET['action']=="delete"){
        if ($_GET['dsid']!="") {
            list($part, $partid, $docid) = preg_split("/-/",$_GET['dsid']);

            //old info of the file
            $cQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageID='%s'",
                                                                      db::$mysqli->escape_string($docid)));
            $cInfo = $cQ->fetch_assoc();

            // delete of document
            $upload->delete_file($cInfo['pageImage'], 'images/photo', 'true');
            //delete of small images
            for($i=1;$i<=count($imageArrayinfo);$i++){
                $upload->delete_file($cInfo['pageImage'], 'images/photo/column_'.$i, 'true');
            }

            $fileName = $cInfo['pageImage'];

            $del1 = db::$mysqli->query("DELETE FROM images WHERE pageID='".$docid."'");
            $del2 = db::$mysqli->query("DELETE FROM images_lang WHERE pageID='".$docid."'");

            if($del1){

                $pInfo = $backaction->getTableName($part);

                // now we need to resequence the pages.
                $j=1;
                $rs = db::$mysqli->query("SELECT pageID FROM images WHERE pageLabel='".$pInfo['label']."'
                                                                      AND pageSection='photo'
                                                                      AND partID='".$partid."' ORDER BY seqID");
                while($row = $rs->fetch_assoc()){
                    db::$mysqli->query("UPDATE images SET seqID='".$j."' WHERE pageID='".$row['pageID']."'");
                    $j++;
                }

                echo $fileName;
                exit();
            } else {
                echo"";
                exit();
            }

        }
    }
   #################################################################################################
?>