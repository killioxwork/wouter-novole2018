<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    // prevent direct access
    $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
                    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    if(!$isAjax) {
        $user_error = 'Access denied - not an AJAX request...';
        trigger_error($user_error, E_USER_ERROR);
    }

    // read
   #################################################################################################
    if(isset($_POST['sid']) and isset($_POST['status'])){
    	$sid = isset($_POST['sid'])? $_POST['sid'] : '';
    	$status = isset($_POST['status'])? $_POST['status'] : '';
        if ($sid!="" and $status!='') {
            list($part, $partid) = preg_split("/-/",$sid);
            if($status=='true') $upstatus = 1;
            if($status=='false') $upstatus = 0;

            db::$mysqli->query("UPDATE contacts SET pageRead='".$upstatus."' WHERE pageID='".$partid."'");
        }
    }
   #################################################################################################

    // delete
   #################################################################################################
    if($_GET['action']=="delete"){
        if ($_GET['dsid']!="") {
            list($part, $docid) = preg_split("/-/",$_GET['dsid']);

            //old info of the file
            $cQ = db::$mysqli->query(sprintf("SELECT pageName FROM contacts WHERE pageID='%s'",
                                                                               db::$mysqli->escape_string($docid)));
            $cInfo = $cQ->fetch_assoc();

            $del1 = db::$mysqli->query("DELETE FROM contacts WHERE pageID='".$docid."'");

            if($del1){
                echo $db->decodeString($cInfo['pageName']);
                exit();
            } else {
                echo"";
                exit();
            }

        }
    }
   #################################################################################################

?>