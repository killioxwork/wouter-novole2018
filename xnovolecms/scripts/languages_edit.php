<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    // js define
    $smarty->assign('jsblock' , 'global-form');
    $smarty->assign('leftblock' , 'languages');


   #################################################################################################
    if($_GET['action']=='add'){

        if($_POST['sub']=='add'){
            if($_POST['title']!="" and $_POST['short']!=""){

                //max seq
                $maxIDQ = db::$mysqli->query("SELECT max(seqID) as maxs FROM languages");
                $rsmaxs = $maxIDQ->fetch_assoc();
                $maxs = $rsmaxs["maxs"];
                if(!$maxs) { $maxs = 1; } else { $maxs = $maxs + 1; }

                //insert title
                $query  = "INSERT INTO languages SET longLang='".$db->encodeString($_POST['title'])."',
                                                     shortLang='".$db->encodeString($_POST['short'])."',
                                                     pageOnline='0',
                                                     seqID='".$maxs."'";
                $q1= db::$mysqli->query($query);

                $goto = "languages_list.php";
                header("Location: ".$goto);
                exit();
            }
        }

        $smarty->assign('action' , $_GET['action']);

    }
   #################################################################################################

   // languages - edit
   #################################################################################################
    if($_GET['action']=='edit'){
        $aQ = db::$mysqli->query(sprintf("SELECT * FROM languages WHERE pageID='%s'",
                                                                     db::$mysqli->escape_string($_GET['pID'])));
        if($aQ->num_rows>0){
            $aInfo = $aQ->fetch_assoc();
        } else {
            header("Location: languages_list.php");
            exit();
        }

        if($_POST['sub']=='edit'){
            if($_POST['title']!="" and $_POST['short']!="" and $_POST['ppID']!="" and $_POST['ppID']==$_GET['pID']){

                //insert title
                $query  = "UPDATE languages SET longLang='".$db->encodeString($_POST['title'])."',
                                                shortLang='".$db->encodeString($_POST['short'])."'
                                          WHERE pageID='".$_POST['ppID']."'";
                                        
                $q1= db::$mysqli->query($query);

                $goto = "languages_list.php";
                header("Location: ".$goto);
                exit();
            }
        }


        $smarty->assign('action' , $_GET['action']);

        $pInfo = array();

        //weekDay

        $pInfo['id'] = $aInfo['pageID'];
        $pInfo['title'] = $db->decodeString($aInfo['longLang'],'noquote');
        $pInfo['short'] = $db->decodeString($aInfo['shortLang'],'noquote');

        $smarty->assign('pInfo' , $pInfo);
    }
   #################################################################################################

    $smarty->display("languages_edit.tpl");
?>