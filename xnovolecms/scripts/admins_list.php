<?php
    include ("db_connect.php");
    include ("testing_inc.php");
    include ("level_inc.php");

    if($_SESSION[SESSION_KEY]['adminInfo']['adminLevel']!=0 and $_SESSION[SESSION_KEY]['adminInfo']['adminLevel']!=1){
        header("Location: logout.php");
        exit();
    }

    // js define
    $smarty->assign('jsblock' , 'admins-list');
    $smarty->assign('leftblock' , 'admins');

    // admin list
   #################################################################################################

    $pInfo = array();

    $i = 0;

    $aQ = db::$mysqli->query("SELECT * FROM admin_accounts WHERE adminLevel>='".$_SESSION[SESSION_KEY]['adminInfo']['adminLevel']."'
                                                        ORDER BY lastTimelog DESC");

    while ($aInfo = $aQ->fetch_assoc()) {

        $pInfo[$i]['code'] = "ADM-".$aInfo['adminID'];
        $pInfo[$i]['id'] = $aInfo['adminID'];
        $pInfo[$i]['date'] = $aInfo['lastTimelog'];
        $pInfo[$i]['name'] = $db->decodeString($aInfo['adminName']);
        $pInfo[$i]['username'] = $db->decodeString($aInfo['userName']);
        $pInfo[$i]['userip'] = $db->decodeString($aInfo['adminIP']);
        $pInfo[$i]['label'] = $db->decodeString($aInfo['label']);
        $pInfo[$i]['status'] = $aInfo['adminOnline'];

        $i++;
    }

    $smarty->assign('pInfo' , $pInfo);

   #################################################################################################

    $smarty->display("admins_list.tpl");
?>