<?php
    include ("db_connect.php");
    include ("testing_inc.php");

    // js define
    if($_GET['set']=='crop'){
        if($_GET['size']==2){
            $smarty->assign('jsblock' , 'crop');
        }
        if($_GET['size']==3){
            $smarty->assign('jsblock' , 'crop-details');
        }
        $template = "panoramic_".$_GET['part']."_crop.tpl";
    } else {
        $smarty->assign('jsblock' , '');
        $template = "panoramic_".$_GET['part'].".tpl";
    }

    $smarty->assign('lang' , $_SESSION['lang']);

    $upload = new upload_class();

    $tablename = $_GET['part'];

    $smarty->assign('panoramMinSize' , $panoramMinSize);
    $smarty->assign('pansize' , $_GET['size']);

   // movie info
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT pages.pageID as pID, pageMenu, pageLabel, catID, pageType
                                                                FROM pages
                                                          INNER JOIN pages_lang
                                                                  ON pages.pageID = pages_lang.pageID AND pages_lang.langID='%s'
                                                               WHERE pages.pageID='%s'",
                                                                 db::$mysqli->escape_string($_SESSION['lang']),
                                                                 db::$mysqli->escape_string($_GET['pID'])));
    if($aQ->num_rows>0){
        $aInfo = $aQ->fetch_assoc();
    } else {
        header("Location: pages_list.php?part=main");
        exit();
    }

    $pInfo = array();

     // specila links
    if($_GET['part']=='pages'){
        $smarty->assign('leftblock' , $aInfo['pageLabel']);

        if($aInfo['pageLabel']=='main') $headTitle = 'Menu pages';
        if($aInfo['pageLabel']=='solo') $headTitle = 'Independent pages';

        if($aInfo['catID']!="0"){
            $m00Q = db::$mysqli->query("SELECT * FROM pages_lang WHERE pageID='".$aInfo['catID']."'
                                                                   AND langID='".$_SESSION['lang']."'");
            $m00Info = $m00Q->fetch_assoc();
            $headTitle .= " - ".$db->decodeString($m00Info['pageMenu']);
        }
        $smarty->assign('headTitle' , $headTitle);

        $pInfo['title'] = $db->decodeString($aInfo['pageMenu']);

    } else {
        $smarty->assign('leftblock' , $_GET['part']);

        $pInfo['title'] = $db->decodeString($aInfo['pageTitle']);
    }

    $pInfo['id'] = $aInfo['pID'];
    $pInfo['type'] = $aInfo['pageType'];

    list($minWidth, $minHeight) = explode("x", $panoramMinSize);
    $smarty->assign('minWidth' , $minWidth);
    $smarty->assign('minHeight' , $minHeight);

    $smarty->assign('pInfo' , $pInfo);
   #################################################################################################


   // croper
   #################################################################################################
    if($_POST['action']=="crop"){

        //old info of the object
        $cQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageID='%s'",
                                                                  db::$mysqli->escape_string($_POST['cID'])));
        $cInfo = $cQ->fetch_assoc();

        $result = $upload->create_with_coordinates(site_files_depth.'images/panoramic/'.$cInfo['pageImage'],
                                                   site_files_depth.'images/panoramic/column_'.$_POST['size'].'/'.$cInfo['pageImage'],
                                                   $_POST['x'], $_POST['y'], $_POST['width'], $_POST['height']);

        // copy and resize (resize all)
        //for($i=1;$i<=count($panoramArrayinfo);$i++){
            list($width, $height) = explode("x", $panoramArrayinfo['column_'.$_POST['size']]);
            try {
                $resizeclass = new abeautifulsite\SimpleImage(site_files_depth.'images/panoramic/column_'.$_POST['size'].'/'.$cInfo['pageImage']);
                $resizeclass->adaptive_resize($width,$height)->save(site_files_depth.'images/panoramic/column_'.$_POST['size'].'/'.$cInfo['pageImage'], 95);
                $errormsg = '';
            } catch(Exception $e) {
                $errormsg = 'Error: ' . $e->getMessage();
            }
        //}
    }
   #################################################################################################

   //image info
   #################################################################################################
    $panInfo = array();

    $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='".$_GET['part']."'
                                                                 AND pageSection='panoramic'
                                                                 AND partID='".$aInfo['pID']."'
                                                                 AND pageID='%s'",
                                                                  db::$mysqli->escape_string($_GET['crID'])));
    if($photoQ->num_rows>0){

        $photoInfo = $photoQ->fetch_assoc();

        if($multifunc->checkImagelocation(site_files_depth."images/panoramic/column_1/", $photoInfo['pageImage'])){

            $panInfo['pID'] = $_GET['pID'];
            $panInfo['crID'] = $_GET['crID'];
            $panInfo['old'] = website_url."images/panoramic/column_".$_GET['size']."/".$photoInfo['pageImage']."?dammy=".date("YmdHis");
            $panInfo['orginal'] = website_url."images/panoramic/".$photoInfo['pageImage'];

            $smarty->assign('panInfo' , $panInfo);
        }
    }

   #################################################################################################


    $smarty->display($template);
?>