{* Smarty *}
<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admins</span> · Details</h4>
						</div>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                        		<a href="admins_list.php" class="btn btn-default"><b><i class="icon-arrow-left13"></i></b>Back To List</a>
                        	</div>
                        </div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Details - {$pInfo.name}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="#basic-rounded-tab2" data-toggle="tab"><i class="icon-file-text2"></i> Details</a></li>
										</ul>

										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div style="text-align:right;">
													<a href="admins_details_edit.php?action=edit&pID={$pInfo.id}#basic-rounded-tab2" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Edit Admin</a>
												</div>

												<form action="#">
													<div class="panel-heading">
														<h5 class="panel-title">Details</h5>
													</div>

													<div class="panel-body">
														<div class="row">
															<div class="col-md-6">

																<fieldset>
																	<legend class="text-semibold">Basic info</legend>

																	<div class="form-group">
																		<label class="text-semibold">Name</label>
																		<div class="form-control-static">{$pInfo.name}</div>
																	</div>

                                                                    <div class="form-group">
																		<label class="text-semibold">Username</label>
																		<div class="form-control-static">{$pInfo.username}</div>
																	</div>

																</fieldset>

															</div>

															<div class="col-md-6">

																<fieldset>

																</fieldset>
															</div>
														</div>
													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">

											</div>
											<!-- /filters -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
