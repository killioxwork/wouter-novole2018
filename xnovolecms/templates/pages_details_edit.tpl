<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{$headTitle}</span> · {$action|ucfirst} Details</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">{$action|ucfirst} Details {if $pInfo.menu!=''}- {$pInfo.menu}{/if}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-text2"></i> Details - {$langTitle}</a></li>
                                {if $action=='edit' && $pInfo.type!="parent"}
                                    {if $pInfo.type=="pages"}
											<li><a href="pages_details.php?pID={$pInfo.id}&lang={$lang}#basic-rounded-tab6"><i class="icon-stack-down"></i> Documents</a></li>
                                    {/if}
                                {/if}
										</ul>


										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div class="text-right">
								{if $action=='edit'}
													<a href="pages_details.php?pID={$pInfo.id}&lang={$lang}#basic-rounded-tab2" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To Overview</a>
                                {else}
                                                    <a href="pages_list.php?part={$part}&catID={$cID}&lang={$lang}" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
                                {/if}
												</div>

												<form action="pages_details_edit.php?action={$action}&part={$part}&cID={$cID}&pID={$pInfo.id}&lang={$lang}" class="form-validate-jquery" method="post" autocomplete="false">
                                                    <input type="hidden" name="ppID" value="{$pInfo.id}">
                                                    <input type="hidden" name="sub" value="{$action}">
													<!--div class="panel-heading">
														<legend style="margin-bottom:0;">{$action|ucfirst} Details</legend>
													</div-->

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">
                                                                															
                                                                <fieldset>
                                                                <legend class="text-semibold">Basic info</legend>

                                                                <div class="form-group">
    																<label class="text-semibold">Language </label>
                                                    {if $action=='add' or $action=='edit'}
    																<input type="hidden" name="lang" value="{$lang}" required="required">
                                                    {/if}
                                                                    <div class="form-control-static">{$langTitle}</div>
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Menu title <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="menu" value="{$pInfo.menu}" required="required">
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Menu subtitle </label>
    																<input type="text" class="form-control" name="submenu" value="{$pInfo.submenu}">
    															</div>
                                                    {if ($action=='add' and $pInfo.id=='') or $action=='edit'}
                                                                <div class="form-group">
    																<label class="text-semibold">Menu color <span class="text-danger">*</span></label>
    																<select class="select" name="menucolor" required="required">
                                                                        <option value="black"{if $pInfo.menucolor=="black"} selected{/if}>black</option>
																		<option value="white"{if $pInfo.menucolor=="white"} selected{/if}>white</option>
    																</select>
    															</div>
                                                    {/if}
                                                                <div class="form-group">
    																<label class="text-semibold">Page title </label>
    																<input type="text" class="form-control" name="title" value="{$pInfo.title}">
    															</div>
                                                    {if (($action=='add' and $pInfo.id=='') or $action=='edit') and $admlvl==0}
    															<div class="form-group">
    																<label class="text-semibold">Type <span class="text-danger">*</span></label>
    																<select class="select" name="type" required="required">
																		<!--<option value="parent"{if $pInfo.type=="parent"} selected{/if}>parent</option>-->
																		<option value="pages"{if $pInfo.type=="pages"} selected{/if}>page</option>
    																</select>
    															</div>
                                                    {/if}
                                                    {if $action=='add' and $admlvl==1}
                                                                <input type="text" class="form-control" name="type" value="pages" readonly>
                                                    {/if}

                                                                </fieldset>                                                                

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>
                                                                    <legend class="text-semibold">PAGE DESCRIPTION</legend>
                                                                        <!--TextArea-->
                                                                        <div class="form-group">
                                                                            <!--label class="text-semibold">Text</label-->
                                                                            <textarea rows="5" cols="5" id="ptext" name="ptext" class="form-control">{$pInfo.text}</textarea>
                                                                        </div>
                                                                        <!--End Textarea-->
																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">
								{if $action=='edit'}
													        <a href="pages_details.php?pID={$pInfo.id}&lang={$lang}#basic-rounded-tab2" class="btn btn-default">Cancel</a>
                                {else}
                                                            <a href="pages_list.php?part={$part}&catID={$cID}&lang={$lang}" class="btn btn-default">Cancel</a>
                                {/if}
															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">
											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script>
    CKEDITOR.replace( 'pintro',
    {
        toolbar : 'MyToolbarmini',
        height : '150px'
    });
</script>
<script>
    CKEDITOR.replace( 'ptext' );
</script>
</body>
</html>
