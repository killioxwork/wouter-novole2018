<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}
<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{$headTitle}</span> · Photo</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - {$pInfo.title}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li><a href="pages_photo.php?pID={$pInfo.id}#basic-rounded-tab4"><i class="icon-file-picture"></i> Main Photo</a></li>
											<li class="active"><a href=""><i class="icon-stack-picture"></i> Photos</a></li>
										</ul>

										<!-- Overview -->
										<div class="tab-content">


											<!-- CROP -->
											<div class="tab-pane active" id="basic-rounded-tab4">

												<div id="vpn_back">
													<a href="pages_photo.php?pID={$pInfo.id}#basic-rounded-tab5" class="btn btn-default btn-sm"><b><i class="icon-arrow-left13"></i></b> Back to List</a>
												</div>


													<div class="panel-body">

														<!-- start form -->
    												<form method="post" action="photo_list.php?part=pages&set=crop&pID={$panInfo.pID}&crID={$panInfo.crID}" name="cropForm" id="cropForm">
											            <input type="hidden" name="action" value="crop" />
                                                        <input type="hidden" name="cID" value="{$panInfo.crID}" />
                                                        <input type="hidden" name="size" value="2" />
														<div class="row">
															<div class="col-md-6">
																<div class="image-cropper-container content-group" style="height: 400px;">
																	<img src="{$panInfo.orginal}" alt="" class="cropper">
																</div>
																<div class="row">
																	<!--<div class="col-lg-4"><p><button id="reset" type="button" class="btn btn-info btn-block">Reset</button></p></div>
																	<div class="col-lg-4"><p><button id="release" type="button" class="btn btn-info btn-block">Release</button></p></div>-->
																	<div class="col-lg-4"><p><a href="javascript:document.cropForm.submit();" id="setData" type="button" class="btn btn-primary btn-block">Save Crop</a></p></div>
																</div>
															</div>

															<div class="col-md-6">
																<div class="content-group text-center">
																    <!--<label class="text-semibold control-label">Current croped image</label>-->
																    <img src="{$panInfo.old}" alt="" class="img-responsive" style="max-height:290px;">
																	<!--<div class="eg-preview">
																		<div class="preview preview-lg"></div>
																		<div class="preview preview-md"></div>
																		<div class="preview preview-sm"></div>
																		<div class="preview preview-xs"></div>
																	</div>-->
																</div>

																<div class="row">
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label class="text-semibold control-label">X value:</label>
																			<input type="text" class="form-control" id="dataX" name="x" readonly>
																		</div>

																		<div class="form-group">
																			<label class="text-semibold control-label">Width:</label>
																			<input type="text" class="form-control" id="dataW" name="width" readonly>
																		</div>

																	</div>


																	<div class="col-lg-6">
																		<div class="form-group">
																			<label class="text-semibold control-label">Y value:</label>
																			<input type="text" class="form-control" id="dataY" name="y" readonly>
																		</div>

																		<div class="form-group">
																			<label class="text-semibold control-label">Height:</label>
																			<input type="text" class="form-control" id="dataH" name="height" readonly>
																		</div>

																	</div>
																</div>
															</div>
														</div>

														</form>
														<!--end form-->
													</div>

  										<!-- /CROP -->






										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
