<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admins</span> · {$action|ucfirst} Details</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">{$action|ucfirst} Details {if $pInfo.name!=''}- {$pInfo.name}{/if}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
            		                		<li><a data-action="collapse"></a></li>
            		                	</ul>
            	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-text2"></i> Details</a></li>
										</ul>


										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div class="text-right">
								{if $action=='edit'}
													<a href="admins_details.php?pID={$pInfo.id}#basic-rounded-tab2" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To Overview</a>
                                {else}
                                    {if $admlvl<=1}
                                                    <a href="admins_list.php" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
                                    {/if}
                                {/if}
												</div>

												<form action="admins_details_edit.php?action={$action}&pID={$pInfo.id}" class="form-validate-jquery" method="post" autocomplete="false">
                                                    <input type="hidden" name="ppID" value="{$pInfo.id}">
                                                    <input type="hidden" name="sub" value="{$action}">
													<!--div class="panel-heading">
														<legend style="margin-bottom:0;">{$action|ucfirst} Details</legend>
													</div-->

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">
                                                                															
                                                                <fieldset>
                                                                <legend class="text-semibold">Basic info</legend>

                                                                <div class="form-group">
    																<label class="text-semibold">Name <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="name" value="{$pInfo.name}" required="required">
    															</div>

    															<div class="form-group">
    																<label class="text-semibold">Username <span class="text-danger">*</span></label>
    																<input type="email" class="form-control" name="username" value="{$pInfo.username}" required="required" remote="mailcheck.php?ad={$pInfo.id}">
    															</div>

    															<div class="form-group">
    																<label class="text-semibold">Level <span class="text-danger">*</span></label>
    																<select class="select" name="level" required="required">
    													{if $admlvl==0}
																		<option value="0"{if $pInfo.level==0} selected{/if}>superadmin</option>
                                                        {/if}
                                                        {if $admlvl<=1}
                                                                        <option value="1"{if $pInfo.level==1} selected{/if}>admin</option>
                                                        {/if}
    																</select>
    															</div>

                                                                </fieldset>

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>
                                                                <legend class="text-semibold">&nbsp;</legend>

    															<div class="form-group">
    																<label class="text-semibold">Password</label>
    																<input type="password" class="form-control" name="password" id="password" value="">
    															</div>

    															<div class="form-group">
    																<label class="text-semibold">Retype pasword</label>
    																<input type="password" class="form-control" name="repass" id="repass" value="">
    															</div>


																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">
								{if $action=='edit'}
													        <a href="admins_details.php?pID={$pInfo.id}#basic-rounded-tab2" class="btn btn-default">Cancel</a>
                                {else}
                                    {if $admlvl<=1}
                                                            <a href="admins_list.php" class="btn btn-default">Cancel</a>
                                    {/if}
                                {/if}
															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">
											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script>
    CKEDITOR.replace( 'ptext' );
</script>
</body>
</html>
