<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Novole</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
<link href="../assets/css/colors.css" rel="stylesheet" type="text/css">
<link href="../assets/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/row_reorder.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/editable/editable.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
{if $jsblock=='crop'}
<script type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"></script>
<script type="text/javascript" src="../assets/js/custom/cropper_extension.js"></script>
{/if}
{if $jsblock=='crop-photo'}
<script type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"></script>
<script type="text/javascript" src="../assets/js/custom/cropper_extension_photo.js"></script>
{/if}
{if $jsblock=='crop-details'}
<script type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"></script>
<script type="text/javascript" src="../assets/js/custom/cropper_extension_details.js"></script>
{/if}
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/datetime-moment.js"></script>
<script type="text/javascript" src="../assets/js/core/app.js"></script>
{if $jsblock=='pages-list'}
<script type="text/javascript" src="../assets/js/custom/pages_list.js"></script>
{/if}
{if $jsblock=='pages-details'}
<script type="text/javascript" src="../assets/js/custom/pages_details_list.js"></script>
{/if}
{if $jsblock=='languages-list'}
<script type="text/javascript" src="../assets/js/custom/languages_list.js"></script>
{/if}
{if $jsblock=='global-form'}
<script type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../assets/js/custom/form_validation.js"></script>
{/if}
{if $jsblock=='admins-list'}
<script type="text/javascript" src="../assets/js/custom/admins_list.js"></script>
{/if}
{if $jsblock=='admins-details'}
<script type="text/javascript" src="../assets/js/custom/admins_details.js"></script>
{/if}
{if $jsblock=='admins-details-form'}
<script type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../assets/js/custom/form_validation_admin.js"></script>
{/if}
{if $jsblock=='contact-list'}
<script type="text/javascript" src="../assets/js/custom/contact_list.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/datetime-moment.js"></script>
{/if}


<!-- /theme JS files -->

</head>