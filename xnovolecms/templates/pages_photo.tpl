{* Smarty *}
<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{$headTitle}</span> · Photo</h4>
						</div>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <!--<a href="pages_details.php?part={$part}&pID={$pInfo.id}&lang=1" class="btn btn-default"><b><i class="icon-eye"></i></b> Show details</a>-->
                        		<a href="pages_list.php?part={$part}&cID={$cID}" class="btn btn-default"><b><i class="icon-arrow-left13"></i></b>Back To List</a>
                        	</div>
                        </div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - {$pInfo.menu}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
                        {if $pInfo.type!="parent"}
											<li class="active"><a href="#basic-rounded-tab4" data-toggle="tab"><i class="icon-file-picture"></i> Main Photo</a></li>
                            {if $pInfo.type=="pages"}
											<li><a href="#basic-rounded-tab5" data-toggle="tab"><i class="icon-stack-picture"></i> Photos</a></li>
                            {/if}
                        {/if}
										</ul>

										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane" id="basic-rounded-tab2">
                                            </div>
											<!-- /details -->

				  							<!-- Panoramic image -->
											<div class="tab-pane active" id="basic-rounded-tab4">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="panoramic_list.php?part=pages&set=upload&pID={$pInfo.id}" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Images</a>
												</div>
												<table class="table panoramic-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>Preview</th>
															<th>Caption title</th>
                                                            <th class="text-center">Publish</th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
										{section name=panKey loop=$panInfo}
														<tr data-sid="{$panInfo[panKey].code}">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<td><a href="{$panInfo[panKey].bigimage}" data-popup="lightbox"> <img src="{$panInfo[panKey].smallimage}" alt="" class="img-rounded img-preview"></a></td>
															<td>{$pInfo.title}</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"{if $panInfo[panKey].status==1} checked="checked"{/if}>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="panoramic_list.php?part=pages&set=crop&pID={$pInfo.id}&crID={$panInfo[panKey].id}&size=3"><i class="icon-crop"></i> Crop details page</a></li>
                                                                            <!--<li><a href="panoramic_list.php?part=pages&set=crop&pID={$pInfo.id}&crID={$panInfo[panKey].id}&size=3"><i class="icon-crop"></i> Crop details</a></li>-->
                                                                            <li><a href="#" class="delete-panoramic" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        {sectionelse}
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>There is no images uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        {/section}
													</tbody>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
															<th></th>
                                                            <th></th>
                                                            <th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /panoramic image -->

				  							<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="photo_list.php?part=pages&set=upload&pID={$pInfo.id}" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Images</a>
                                                </div>
												<table class="table photos-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>Preview</th>
															<th>Languages</th>
                                                            <th class="text-center">Publish</th>
                                                            <th>Actions</th>
														</tr>
													</thead>
													<tbody>
								        {section name=phKey loop=$phInfo}
														<tr data-sid="{$phInfo[phKey].code}">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<td>
                                                {if $phInfo[phKey].type=="image"}
															    <a href="{$phInfo[phKey].bigimage}" data-popup="lightbox"> <img src="{$phInfo[phKey].smallimage}" alt="" class="img-rounded img-preview"></a>
                                                {else}
                                                                <img src="{$phInfo[phKey].smallimage}" alt="" class="img-rounded img-preview">
                                                {/if}
                                                            </td>
															<td>{$phInfo[phKey].lang}</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"{if $phInfo[phKey].status==1} checked="checked"{/if}>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="photo_list.php?part=pages&set=crop&pID={$pInfo.id}&crID={$phInfo[phKey].id}"><i class="icon-crop"></i> Crop</a></li>
                                                                            <li><a href="photo_list.php?part=pages&set=edit&pID={$pInfo.id}&crID={$phInfo[phKey].id}"><i class="icon-pencil7"></i> Edit</a></li>
                                                                            <li><a href="#" class="delete-photo" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        {sectionelse}
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>There is no images uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        {/section}
													</tbody>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

				  							<!-- Logo -->
											<div class="tab-pane" id="basic-rounded-tab7">

											</div>
											<!-- /logo -->

				  							<!-- Sponsor logo -->
											<div class="tab-pane" id="basic-rounded-tab8">

											</div>
											<!-- /Sponsor logo -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
