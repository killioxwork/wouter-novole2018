{* Smarty *}
<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
    <!-- /main sidebar --> 
    
    <!-- Main content -->
    <div class="content-wrapper"> 
      
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{$headTitle}</span> - Listing</h4>
          </div>
		  <div class="heading-elements">
			 <div class="heading-btn-group">
	{if $lang==1}
				 <a href="pages_details_edit.php?part={$part}&action=add{$actionTxt}" class="btn btn-primary btn-lg btn-labeled"><b><i class="icon-plus-circle2"></i></b>Add Page</a>
    {/if}
			 </div>
		  </div>
        </div>
      </div>
      <!-- /page header --> 
      
      <!-- Content area -->
      <div class="content">

        <!-- Media library -->
        <div class="panel panel-white">
          <div class="panel-heading">
            <h6 class="panel-title text-semibold">{$headTitle}</h6>
            <div class="heading-elements">
              <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!--li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li-->
              </ul>
            </div>
          </div>
          <div style="text-align:right; padding-top: 20px; padding-right: 20px">
    {section name=lKey loop=$l}
    	      <a href="pages_list.php?part={$part}&lang={$l[lKey].id}" class="btn btn-primary btn-sm{if $l[lKey].id==$lang} disabled{/if}" title="{$l[lKey].long}">{$l[lKey].short}</a>
    {/section}
          </div>
          <table class="table movies-list table-hover">
            <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Type</th>
                <th class="text-center">Publish</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
    {section name=pKey loop=$pInfo}
              <tr data-sid="{$pInfo[pKey].code}">
                <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
                <td>{$pInfo[pKey].title}</td>
                <td>{$pInfo[pKey].type}</td>
                <td class="text-center">
				    <label class="checkbox-inline checkbox-switchery switchery-xs">
					     <input type="checkbox" class="switchery"{if $pInfo[pKey].status==1} checked="checked"{/if}{if $pInfo[pKey].status==''} disabled{/if}>&nbsp;
				    </label>
			    </td>
                <td class="text-center"><ul class="icons-list">
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
                      <ul class="dropdown-menu dropdown-menu-right">
        {if $pInfo[pKey].status!=''}
                        <li><a href="pages_details.php?part={$part}&pID={$pInfo[pKey].id}&lang={$lang}"><i class="icon-eye"></i> Show details</a></li>
                        <li><a href="pages_photo.php?part={$part}&pID={$pInfo[pKey].id}"><i class="icon-stack-picture"></i> Photos</a></li>
        {else}
                        <li><a href="pages_details_edit.php?part={$part}&action=add&pID={$pInfo[pKey].id}&lang={$lang}"><i class="icon-plus-circle2"></i> Add Language</a></li>
        {/if}
		{if $pInfo[pKey].type=='parent'}
                		<li><a href="pages_list.php?part={$part}&cID={$pInfo[pKey].id}&lang={$lang}"><i class="icon-folder-open"></i> Open </a></li>
        {/if}
        {if $pInfo[pKey].del=='YES'}
                        <li><a href="#" class="delete-pages" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
        {/if}                
                      </ul>
                    </li>
                  </ul></td>
              </tr>
    {sectionelse}
              <tr>
                  <td></td>
                  <td>There is no data in table at this moment</td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
    {/section}
            </tbody>
        	<tfoot>
        		<tr>
        			<th></th>
        			<th></th>
                    <th></th>
                    <th></th>
                    <th></th>
        		</tr>
        	</tfoot>
          </table>
        </div>
        <!-- /media library --> 
        
        <!-- Footer -->
{include file="x-footer.tpl"}
        <!-- /footer -->
        
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container -->

</body>
</html>
