{* Smarty *}
<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
    <!-- /main sidebar --> 
    
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Contact requests </span> - Listing</h4>
          </div>
		  <div class="heading-elements">
		  </div>
        </div>
      </div>
      <!-- /page header --> 
      
      <!-- Content area -->
      <div class="content">

        <!-- Media library -->
        <div class="panel panel-white">
          <div class="panel-heading">
            <h6 class="panel-title text-semibold">Contact requests</h6>
            <div class="heading-elements">
              <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!--li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li-->
              </ul>
            </div>
          </div>
          <table class="table movies-list table-hover">
            <thead>
              <tr>
                <th>Log date</th>
                <th>Contact</th>
                <th>Remark</th>
                <th class="text-center">Checked</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
    {section name=pKey loop=$pInfo}
              <tr data-sid="{$pInfo[pKey].code}">
                <td>{$pInfo[pKey].date}</td>
                <td>
                    {$pInfo[pKey].name}<br />
                    {if $pInfo[pKey].phone!=''}T {$pInfo[pKey].phone}<br />{/if}
                    {if $pInfo[pKey].country!=''}{$pInfo[pKey].country}<br />{/if}
                    <a href="mailto:{$pInfo[pKey].email}">{$pInfo[pKey].email}</a>
                </td>
                <td>
        {if $pInfo[pKey].coment!=''}
                    <a href="#" data-placement="top" data-popup="tooltip" title="{$pInfo[pKey].coment}"><i class="icon-bubble2"></i></a>
        {/if}
                </td>
                <td class="text-center">
				    <label class="checkbox-inline checkbox-switchery switchery-xs">
					     <input type="checkbox" class="switchery"{if $pInfo[pKey].status==1} checked="checked"{/if}>&nbsp;
				    </label>
			    </td>
                <td class="text-center"><ul class="icons-list">
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="delete-reservation" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
                      </ul>
                    </li>
                  </ul></td>
              </tr>
    {sectionelse}
              <tr>
                  <td></td>
                  <td>There is no data in table at this moment</td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
    {/section}
            </tbody>
        	<tfoot>
        		<tr>
        			<th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
        		</tr>
        	</tfoot>
          </table>
        </div>
        <!-- /media library --> 
        
        <!-- Footer -->
{include file="x-footer.tpl"}
        <!-- /footer -->
        
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container -->

</body>
</html>
