<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header"> <a class="navbar-brand" href="#"><img src="../../images/logoWhite.svg" alt="Novole"></a>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            <li><a class="sidebar-mobile-secondary-toggle"><i class="icon-more"></i></a></li>
        </ul>
    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li> <a class="sidebar-control sidebar-main-toggle hidden-xs"> <i class="icon-paragraph-justify3"></i> </a> </li>
        </ul>

        <!-- Account Settings dropdown -->
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user"> <a class="dropdown-toggle" data-toggle="dropdown"><div class="avatar">{$loginname|substr:0:1|upper}</div><span>{$loginname}</span> <i class="caret"></i> </a>
                    <ul class="dropdown-menu dropdown-menu-right">
        {if $admlvl==0}
                        <li><a href="admins_details.php"><i class="icon-cog5"></i> Account settings</a></li>
        {/if}                
                        <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /account Settings dropdown -->

    </div>
</div>