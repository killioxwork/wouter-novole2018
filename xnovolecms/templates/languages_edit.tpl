<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Languages</span> · {$action|ucfirst} Details</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">{$action|ucfirst} Details {if $pInfo.title!=''}- {$pInfo.title}{/if}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
            		                		<li><a data-action="collapse"></a></li>
            		                	</ul>
            	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-text2"></i> Details</a></li>
										</ul>


										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div class="text-right">
                                                    <a href="languages_list.php" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
												</div>

												<form action="languages_edit.php?action={$action}&pID={$pInfo.id}" class="form-validate-jquery" method="post" autocomplete="false">
                                                    <input type="hidden" name="ppID" value="{$pInfo.id}">
                                                    <input type="hidden" name="sub" value="{$action}">
													<!--div class="panel-heading">
														<legend style="margin-bottom:0;">{$action|ucfirst} Details</legend>
													</div-->

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">
                                                                															
                                                                <fieldset>
                                                                <legend class="text-semibold">Basic info</legend>

                                                                <div class="form-group">
    																<label class="text-semibold">Title <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="title" value="{$pInfo.title}" required="required">
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Short <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="short" value="{$pInfo.short}" required="required">
    															</div>

                                                                </fieldset>

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>

																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">

                                                            <a href="languages_list.php" class="btn btn-default">Cancel</a>

															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">
											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script>
    CKEDITOR.replace( 'ptext' );
</script>
</body>
</html>
