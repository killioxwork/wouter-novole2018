        <div class="sidebar sidebar-main sidebar-fixed">
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Content</span> <i class="icon-menu" title="Content"></i></li>

                            <li> <a href="#"><i class="icon-file-text3"></i> <span>Pages</span></a>
                                <ul>
                                    <li{if $leftblock=='main'} class="active"{/if}><a href="pages_list.php?part=main">Menu pages</a></li>
                                    <li{if $leftblock=='solo'} class="active"{/if}><a href="pages_list.php?part=solo">Independent pages</a></li>
                                </ul>
                            </li>

                            <li{if $leftblock=='contact'} class="active"{/if}> <a href="contact_list.php"><i class="icon-database"></i> <span>Contacts</span></a></li>

                {if $admlvl<=1}
                            <li class="navigation-header"><span>Management</span> <i class="icon-menu" title="Management"></i></li>
                            <li> <a href="#"><i class="icon-cog3"></i> <span>Settings</span></a>
                                <ul>
                                    <li{if $leftblock=='languages'} class="active"{/if}><a href="languages_list.php">Languages</a></li>
                                    <li{if $leftblock=='admins'} class="active"{/if}><a href="admins_list.php">Admin CMS</a></li>
                                </ul>
                            </li>
                {/if}
                            <!-- /page kits -->

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>