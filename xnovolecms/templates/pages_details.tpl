{* Smarty *}
<!DOCTYPE html>
<html lang="en">
{include file="x-header.tpl"}

<body class="navbar-top">

	<!-- Main navbar -->
{include file="x-navigation_top.tpl"}
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
{include file="x-navigation_left.tpl"}
			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{$headTitle}</span> · Details</h4>
						</div>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="pages_photo.php?part={$part}&catID={$cID}&pID={$pInfo.id}" class="btn btn-default"><b><i class="icon-stack-picture"></i></b> Photo</a>
                        		<a href="pages_list.php?part={$part}&cID={$cID}" class="btn btn-default"><b><i class="icon-arrow-left13"></i></b>Back To List</a>
                        	</div>
                        </div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Details - {$pInfo.menu}</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="#basic-rounded-tab2" data-toggle="tab"><i class="icon-file-text2"></i> Details - {$langTitle}</a></li>
                        {if $pInfo.type!="parent"}
                            {if $pInfo.type=="pages"}
											<li><a href="#basic-rounded-tab6" data-toggle="tab"><i class="icon-stack-down"></i> Documents</a></li>
                            {/if}
                        {/if}
										</ul>

										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div style="text-align:right;">
													<a href="pages_details_edit.php?action=edit&part={$part}&catID={$cID}&pID={$pInfo.id}&lang={$lang}#basic-rounded-tab2" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Edit Page</a>
												</div>

												<form action="#">
													<div class="panel-heading">
														<h5 class="panel-title">Details</h5>
													</div>

													<div class="panel-body">
														<div class="row">
															<div class="col-md-6">

																<fieldset>
																	<legend class="text-semibold">Basic info</legend>

                                                                    <div class="form-group">
        																<label class="text-semibold">Language </label>
                                                                        <div class="form-control-static">{$langTitle}</div>
        															</div>

																	<div class="form-group">
																		<label class="text-semibold">Menu title</label>
																		<div class="form-control-static">{$pInfo.menu}</div>
																	</div>

                                                                    <div class="form-group">
        																<label class="text-semibold">Menu subtitle </label>
        																<div class="form-control-static">{$pInfo.submenu}</div>
        															</div>

                                                                    <div class="form-group">
        																<label class="text-semibold">Menu color </label>
        																<div class="form-control-static">{$pInfo.menucolor}</div>
        															</div>

																	<div class="form-group">
																		<label class="text-semibold">Page title</label>
																		<div class="form-control-static">{$pInfo.title}</div>
																	</div>

                                                                    <!--<div class="form-group">
																		<label class="text-semibold">Type</label>
																		<div class="form-control-static">{$pInfo.type}</div>
																	</div>-->

																</fieldset>

															</div>

															<div class="col-md-6">

																<fieldset>
																	<legend class="text-semibold">Page description</legend>
						                                        <div class="switch-group">

																	<div class="form-group">
																		<label class="text-semibold">Text</label>
																		<div class="form-control-static">
																			{$pInfo.text}
																		</div>
																	</div>
						                                        </div>
																</fieldset>
															</div>
														</div>
													</div>
												</form>

											</div>
											<!-- /details -->

				  							<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

				  							<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="documents_list.php?part=pages&set=upload&pID={$pInfo.id}&lang={$lang}" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Document</a>
												</div>
												<table class="table documents-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>File name</th>
															<th>File type</th>
															<th>File size</th>
                                                            <th class="text-center">Publish</th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
                                        {section name=dKey loop=$docInfo}
														<tr data-sid="{$docInfo[dKey].code}">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<!--<td>{$docInfo[dKey].name}</td>-->
                                                            <td><a href="#" class="editDocInput" data-name="doctitle" data-type="text" data-inputclass="form-control" data-pk="{$docInfo[dKey].code}" data-title="{$docInfo[dKey].title}">{$docInfo[dKey].title}</a></td>
															<td><i class="fa fa-{$docInfo[dKey].iconcode}"></i> {$docInfo[dKey].icontext}</td>
															<td>{$docInfo[dKey].size}</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"{if $docInfo[dKey].status==1} checked="checked"{/if}>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="{$docInfo[dKey].dlink}"><i class="icon-eye"></i> Download</a></li>
																			<li><a href="#" class="delete-document" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        {sectionelse}
                                                        <tr>
                                                            <td></td>
                                                            <td>There is no document uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        {/section}
													</tbody>
													<tfoot>
														<tr>
														    <th></th>
                                                            <th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /documents -->

				  							<!-- Logo -->
											<div class="tab-pane" id="basic-rounded-tab7">

											</div>
											<!-- /logo -->

				  							<!-- Sponsor logo -->
											<div class="tab-pane" id="basic-rounded-tab8">

											</div>
											<!-- /Sponsor logo -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
{include file="x-footer.tpl"}
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
