<?php
/* Smarty version 3.1.30, created on 2018-07-17 11:22:46
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\panoramic_pages_crop.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4db566d74028_48154689',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3e26c5f833834e7bcdf34f3e1d2f80bc0db6777' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\panoramic_pages_crop.tpl',
      1 => 1531819249,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4db566d74028_48154689 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Main Photo</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-picture"></i> Main Photo</a></li>
                                        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?>
											<li><a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab5" ><i class="icon-stack-picture"></i> Photos</a></li>
                                        <?php }?>
										</ul>

										<!-- Overview -->
										<div class="tab-content">


											<!-- CROP -->
											<div class="tab-pane active" id="basic-rounded-tab4">

												<div id="vpn_back">
													<a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab4" class="btn btn-default btn-sm"><b><i class="icon-arrow-left13"></i></b> Back to List</a>
												</div>


													<div class="panel-body">

														<!-- start form -->
    												<form method="post" action="panoramic_list.php?part=pages&set=crop&pID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['pID'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['crID'];?>
&size=<?php echo $_smarty_tpl->tpl_vars['pansize']->value;?>
" name="cropForm" id="cropForm">
											            <input type="hidden" name="action" value="crop" />
                                                        <input type="hidden" name="cID" value="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['crID'];?>
" />
                                                        <input type="hidden" name="size" value="<?php echo $_smarty_tpl->tpl_vars['pansize']->value;?>
" />
														<div class="row">
															<div class="col-md-6">
																<div class="image-cropper-container content-group" style="height: 400px;">
																	<img src="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['orginal'];?>
" alt="" class="cropper">
																</div>
																<div class="row">
																	<!--<div class="col-lg-4"><p><button id="reset" type="button" class="btn btn-info btn-block">Reset</button></p></div>
																	<div class="col-lg-4"><p><button id="release" type="button" class="btn btn-info btn-block">Release</button></p></div>-->
																	<div class="col-lg-4"><p><a href="javascript:document.cropForm.submit();" id="setData" type="button" class="btn btn-primary btn-block">Save Crop</a></p></div>
																</div>
															</div>

															<div class="col-md-6">
																<div class="content-group text-center">
																    <!--<label class="text-semibold control-label">Current croped image</label>-->
																    <img src="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['old'];?>
" alt="" class="img-responsive" style="max-height:290px;">
																	<!--<div class="eg-preview">
																		<div class="preview preview-lg"></div>
																		<div class="preview preview-md"></div>
																		<div class="preview preview-sm"></div>
																		<div class="preview preview-xs"></div>
																	</div>-->
																</div>

																<div class="row">
																	<div class="col-lg-6">
																		<div class="form-group">
																			<label class="text-semibold control-label">X value:</label>
																			<input type="text" class="form-control" id="dataX" name="x" readonly>
																		</div>

																		<div class="form-group">
																			<label class="text-semibold control-label">Width:</label>
																			<input type="text" class="form-control" id="dataW" name="width" readonly>
																		</div>

																	</div>


																	<div class="col-lg-6">
																		<div class="form-group">
																			<label class="text-semibold control-label">Y value:</label>
																			<input type="text" class="form-control" id="dataY" name="y" readonly>
																		</div>

																		<div class="form-group">
																			<label class="text-semibold control-label">Height:</label>
																			<input type="text" class="form-control" id="dataH" name="height" readonly>
																		</div>

																	</div>
																</div>
															</div>
														</div>

														</form>
														<!--end form-->
													</div>

  										<!-- /CROP -->






										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php }
}
