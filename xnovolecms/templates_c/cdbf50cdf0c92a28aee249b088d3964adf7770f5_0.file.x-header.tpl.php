<?php
/* Smarty version 3.1.30, created on 2018-07-20 17:04:45
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\x-header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b51fa0d39c8d0_08267627',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cdbf50cdf0c92a28aee249b088d3964adf7770f5' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\x-header.tpl',
      1 => 1532098816,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b51fa0d39c8d0_08267627 (Smarty_Internal_Template $_smarty_tpl) {
?>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Novole</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
<link href="../assets/css/colors.css" rel="stylesheet" type="text/css">
<link href="../assets/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"><?php echo '</script'; ?>
>
<!-- /core JS files -->

<!-- Theme JS files -->
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/media/fancybox.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/row_reorder.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/responsive.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/notifications/sweet_alert.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/editable/editable.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['jsblock']->value == 'crop') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/cropper_extension.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'crop-photo') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/cropper_extension_photo.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'crop-details') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/media/cropper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/cropper_extension_details.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/datetime-moment.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/app.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['jsblock']->value == 'pages-list') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/pages_list.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'pages-details') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/pages_details_list.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'languages-list') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/languages_list.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'global-form') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../ckeditor/ckeditor.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/form_validation.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'admins-list') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/admins_list.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'admins-details') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/admins_details.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'admins-details-form') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../ckeditor/ckeditor.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/form_validation_admin.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['jsblock']->value == 'contact-list') {
echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/contact_list.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/datetime-moment.js"><?php echo '</script'; ?>
>
<?php }?>


<!-- /theme JS files -->

</head><?php }
}
