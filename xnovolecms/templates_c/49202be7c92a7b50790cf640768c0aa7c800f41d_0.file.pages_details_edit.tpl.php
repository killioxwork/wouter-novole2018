<?php
/* Smarty version 3.1.30, created on 2018-07-19 18:25:34
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\pages_details_edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50bb7ea66972_76353001',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49202be7c92a7b50790cf640768c0aa7c800f41d' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\pages_details_edit.tpl',
      1 => 1532017523,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b50bb7ea66972_76353001 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · <?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title"><?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['menu'] != '') {?>- <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menu'];
}?></h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-text2"></i> Details - <?php echo $_smarty_tpl->tpl_vars['langTitle']->value;?>
</a></li>
                                <?php if ($_smarty_tpl->tpl_vars['action']->value == 'edit' && $_smarty_tpl->tpl_vars['pInfo']->value['type'] != "parent") {?>
                                    <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?>
											<li><a href="pages_details.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab6"><i class="icon-stack-down"></i> Documents</a></li>
                                    <?php }?>
                                <?php }?>
										</ul>


										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div class="text-right">
								<?php if ($_smarty_tpl->tpl_vars['action']->value == 'edit') {?>
													<a href="pages_details.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab2" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To Overview</a>
                                <?php } else { ?>
                                                    <a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&catID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
                                <?php }?>
												</div>

												<form action="pages_details_edit.php?action=<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
&part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&cID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" class="form-validate-jquery" method="post" autocomplete="false">
                                                    <input type="hidden" name="ppID" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
">
                                                    <input type="hidden" name="sub" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
													<!--div class="panel-heading">
														<legend style="margin-bottom:0;"><?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details</legend>
													</div-->

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">
                                                                															
                                                                <fieldset>
                                                                <legend class="text-semibold">Basic info</legend>

                                                                <div class="form-group">
    																<label class="text-semibold">Language </label>
                                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'add' || $_smarty_tpl->tpl_vars['action']->value == 'edit') {?>
    																<input type="hidden" name="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" required="required">
                                                    <?php }?>
                                                                    <div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['langTitle']->value;?>
</div>
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Menu title <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="menu" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menu'];?>
" required="required">
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Menu subtitle </label>
    																<input type="text" class="form-control" name="submenu" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['submenu'];?>
">
    															</div>
                                                    <?php if (($_smarty_tpl->tpl_vars['action']->value == 'add' && $_smarty_tpl->tpl_vars['pInfo']->value['id'] == '') || $_smarty_tpl->tpl_vars['action']->value == 'edit') {?>
                                                                <div class="form-group">
    																<label class="text-semibold">Menu color <span class="text-danger">*</span></label>
    																<select class="select" name="menucolor" required="required">
                                                                        <option value="black"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value['menucolor'] == "black") {?> selected<?php }?>>black</option>
																		<option value="white"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value['menucolor'] == "white") {?> selected<?php }?>>white</option>
    																</select>
    															</div>
                                                    <?php }?>
                                                                <div class="form-group">
    																<label class="text-semibold">Page title </label>
    																<input type="text" class="form-control" name="title" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
">
    															</div>
                                                    <?php if ((($_smarty_tpl->tpl_vars['action']->value == 'add' && $_smarty_tpl->tpl_vars['pInfo']->value['id'] == '') || $_smarty_tpl->tpl_vars['action']->value == 'edit') && $_smarty_tpl->tpl_vars['admlvl']->value == 0) {?>
    															<div class="form-group">
    																<label class="text-semibold">Type <span class="text-danger">*</span></label>
    																<select class="select" name="type" required="required">
																		<!--<option value="parent"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "parent") {?> selected<?php }?>>parent</option>-->
																		<option value="pages"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?> selected<?php }?>>page</option>
    																</select>
    															</div>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'add' && $_smarty_tpl->tpl_vars['admlvl']->value == 1) {?>
                                                                <input type="text" class="form-control" name="type" value="pages" readonly>
                                                    <?php }?>

                                                                </fieldset>                                                                

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>
                                                                    <legend class="text-semibold">PAGE DESCRIPTION</legend>
                                                                        <!--TextArea-->
                                                                        <div class="form-group">
                                                                            <!--label class="text-semibold">Text</label-->
                                                                            <textarea rows="5" cols="5" id="ptext" name="ptext" class="form-control"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['text'];?>
</textarea>
                                                                        </div>
                                                                        <!--End Textarea-->
																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">
								<?php if ($_smarty_tpl->tpl_vars['action']->value == 'edit') {?>
													        <a href="pages_details.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab2" class="btn btn-default">Cancel</a>
                                <?php } else { ?>
                                                            <a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&catID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" class="btn btn-default">Cancel</a>
                                <?php }?>
															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">
											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<?php echo '<script'; ?>
>
    CKEDITOR.replace( 'pintro',
    {
        toolbar : 'MyToolbarmini',
        height : '150px'
    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    CKEDITOR.replace( 'ptext' );
<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
