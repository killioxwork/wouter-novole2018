<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:52:55
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\documents_pages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b538f1773a380_12630564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0e2d704ea5a68e066b69f083c25fa993ed7ef8ee' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\documents_pages.tpl',
      1 => 1532202772,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b538f1773a380_12630564 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Documents</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Details - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li><a href="pages_details.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab2"><i class="icon-file-text2"></i> Details - <?php echo $_smarty_tpl->tpl_vars['langTitle']->value;?>
</a></li>
											<li class="active"><a href=""  data-toggle="tab"><i class="icon-stack-down"></i> Documents</a></li>
										</ul>

										<!-- Showdates -->
										<div class="tab-content">
											<div class="tab-pane active" id="basic-rounded-tab1">
											</div>
											<!-- /showdates -->

											<!-- Details -->
											<div class="tab-pane" id="basic-rounded-tab2">
											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">

											</div>
											<!-- /filters -->

											<!-- Documents -->
											<div class="tab-pane active" id="basic-rounded-tab4">
												<div class="text-right">
													<a href="pages_details.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab6" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
												</div>
												<div class="panel-body">

														<!--Uploader-->
														<form class="form-horizontal">
															<div class="form-group">
																<label class="col-lg-2 control-label text-semibold">Upload documents:</label>
																<div class="col-lg-10">
																	<div id="errorBlock"></div>
																	<input type="file" class="file-input-ajax" multiple="multiple" name="docupload">
																</div>
															</div>
														</form>
														<!--/Uploader-->
                                                        
														<?php echo '<script'; ?>
>
														$(function() {
															// AJAX upload
															$(".file-input-ajax").fileinput({
																	uploadUrl: "documents_action.php", // server upload action
																	uploadAsync: true,
																	maxFileCount: 5,
																	initialPreview: [],
															        //allowedFileExtensions: ["jpg", "jpeg", "png"],
															        elErrorContainer: "#errorBlock",
        															uploadExtraData: {
        																pID: "<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
",
                                                                        lang: "<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
",
        																part: "pages",
                                                                        action: "add"
        															},
																	fileActionSettings: {
																			removeIcon: '<i class="icon-bin"></i>',
																			removeClass: 'btn btn-link btn-xs btn-icon',
																			uploadIcon: '<i class="icon-upload"></i>',
																			uploadClass: 'btn btn-link btn-xs btn-icon',
																			indicatorNew: '<i class="icon-file-plus text-slate"></i>',
																			indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
																			indicatorError: '<i class="icon-cross2 text-danger"></i>',
																			indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
																	}
															});

														});
														<?php echo '</script'; ?>
>
                                                        
												</div>
												<div id="kv-success-1" class="alert alert-success" style="margin-top:10px;display:none"></div>

											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">

											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php }
}
