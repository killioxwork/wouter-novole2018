<?php
/* Smarty version 3.1.30, created on 2018-07-16 23:17:56
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\pages_details.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4d0b848bb5f1_09138266',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0bafc2372e4060f2861c3b677e77f17a4823b7c5' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\pages_details.tpl',
      1 => 1531775398,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4d0b848bb5f1_09138266 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Details</h4>
						</div>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                        		<a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&cID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
" class="btn btn-default"><b><i class="icon-arrow-left13"></i></b>Back To List</a>
                        	</div>
                        </div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Details - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menu'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="#basic-rounded-tab2" data-toggle="tab"><i class="icon-file-text2"></i> Details - <?php echo $_smarty_tpl->tpl_vars['langTitle']->value;?>
</a></li>
                        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] != "parent") {?>
                            <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?>
											<li><a href="#basic-rounded-tab6" data-toggle="tab"><i class="icon-stack-down"></i> Documents</a></li>
                            <?php }?>
                        <?php }?>
										</ul>

										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div style="text-align:right;">
													<a href="pages_details_edit.php?action=edit&part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&catID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
#basic-rounded-tab2" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Edit Page</a>
												</div>

												<form action="#">
													<div class="panel-heading">
														<h5 class="panel-title">Details</h5>
													</div>

													<div class="panel-body">
														<div class="row">
															<div class="col-md-6">

																<fieldset>
																	<legend class="text-semibold">Basic info</legend>

                                                                    <div class="form-group">
        																<label class="text-semibold">Language </label>
                                                                        <div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['langTitle']->value;?>
</div>
        															</div>

																	<div class="form-group">
																		<label class="text-semibold">Menu title</label>
																		<div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menu'];?>
</div>
																	</div>

                                                                    <div class="form-group">
        																<label class="text-semibold">Menu subtitle </label>
        																<div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['submenu'];?>
</div>
        															</div>

                                                                    <div class="form-group">
        																<label class="text-semibold">Menu color </label>
        																<div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menucolor'];?>
</div>
        															</div>

																	<div class="form-group">
																		<label class="text-semibold">Page title</label>
																		<div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</div>
																	</div>

                                                                    <!--<div class="form-group">
																		<label class="text-semibold">Type</label>
																		<div class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['type'];?>
</div>
																	</div>-->

																</fieldset>

															</div>

															<div class="col-md-6">

																<fieldset>
																	<legend class="text-semibold">Page description</legend>
						                                        <div class="switch-group">

																	<div class="form-group">
																		<label class="text-semibold">Text</label>
																		<div class="form-control-static">
																			<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['text'];?>

																		</div>
																	</div>
						                                        </div>
																</fieldset>
															</div>
														</div>
													</div>
												</form>

											</div>
											<!-- /details -->

				  							<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

				  							<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="documents_list.php?part=pages&set=upload&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Document</a>
												</div>
												<table class="table documents-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>File name</th>
															<th>File type</th>
															<th>File size</th>
                                                            <th class="text-center">Publish</th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
                                        <?php
$__section_dKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey'] : false;
$__section_dKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['docInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_dKey_0_total = $__section_dKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_dKey'] = new Smarty_Variable(array());
if ($__section_dKey_0_total != 0) {
for ($__section_dKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] = 0; $__section_dKey_0_iteration <= $__section_dKey_0_total; $__section_dKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']++){
?>
														<tr data-sid="<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['code'];?>
">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<!--<td><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['name'];?>
</td>-->
                                                            <td><a href="#" class="editDocInput" data-name="doctitle" data-type="text" data-inputclass="form-control" data-pk="<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['code'];?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['title'];?>
</a></td>
															<td><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['iconcode'];?>
"></i> <?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['icontext'];?>
</td>
															<td><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['size'];?>
</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"<?php if ($_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['status'] == 1) {?> checked="checked"<?php }?>>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['dlink'];?>
"><i class="icon-eye"></i> Download</a></li>
																			<li><a href="#" class="delete-document" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        <?php }} else {
 ?>
                                                        <tr>
                                                            <td></td>
                                                            <td>There is no document uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        <?php
}
if ($__section_dKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_dKey'] = $__section_dKey_0_saved;
}
?>
													</tbody>
													<tfoot>
														<tr>
														    <th></th>
                                                            <th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /documents -->

				  							<!-- Logo -->
											<div class="tab-pane" id="basic-rounded-tab7">

											</div>
											<!-- /logo -->

				  							<!-- Sponsor logo -->
											<div class="tab-pane" id="basic-rounded-tab8">

											</div>
											<!-- /Sponsor logo -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php }
}
