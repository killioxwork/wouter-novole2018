<?php
/* Smarty version 3.1.30, created on 2018-07-20 16:54:11
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\x-navigation_left.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b51f7937a12f2_30604989',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4696316f4f7931c22b216c690c0a0fd0689bcfb' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\x-navigation_left.tpl',
      1 => 1532098397,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b51f7937a12f2_30604989 (Smarty_Internal_Template $_smarty_tpl) {
?>
        <div class="sidebar sidebar-main sidebar-fixed">
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Content</span> <i class="icon-menu" title="Content"></i></li>

                            <li> <a href="#"><i class="icon-file-text3"></i> <span>Pages</span></a>
                                <ul>
                                    <li<?php if ($_smarty_tpl->tpl_vars['leftblock']->value == 'main') {?> class="active"<?php }?>><a href="pages_list.php?part=main">Menu pages</a></li>
                                    <li<?php if ($_smarty_tpl->tpl_vars['leftblock']->value == 'solo') {?> class="active"<?php }?>><a href="pages_list.php?part=solo">Independent pages</a></li>
                                </ul>
                            </li>

                            <li<?php if ($_smarty_tpl->tpl_vars['leftblock']->value == 'contact') {?> class="active"<?php }?>> <a href="contact_list.php"><i class="icon-database"></i> <span>Contacts</span></a></li>

                <?php if ($_smarty_tpl->tpl_vars['admlvl']->value <= 1) {?>
                            <li class="navigation-header"><span>Management</span> <i class="icon-menu" title="Management"></i></li>
                            <li> <a href="#"><i class="icon-cog3"></i> <span>Settings</span></a>
                                <ul>
                                    <li<?php if ($_smarty_tpl->tpl_vars['leftblock']->value == 'languages') {?> class="active"<?php }?>><a href="languages_list.php">Languages</a></li>
                                    <li<?php if ($_smarty_tpl->tpl_vars['leftblock']->value == 'admins') {?> class="active"<?php }?>><a href="admins_list.php">Admin CMS</a></li>
                                </ul>
                            </li>
                <?php }?>
                            <!-- /page kits -->

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div><?php }
}
