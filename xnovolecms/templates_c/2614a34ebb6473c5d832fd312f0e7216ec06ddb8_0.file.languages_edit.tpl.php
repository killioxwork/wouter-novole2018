<?php
/* Smarty version 3.1.30, created on 2018-07-19 17:51:39
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\languages_edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50b38ba5dcd7_20022063',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2614a34ebb6473c5d832fd312f0e7216ec06ddb8' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\languages_edit.tpl',
      1 => 1531732370,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b50b38ba5dcd7_20022063 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Languages</span> · <?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title"><?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['title'] != '') {?>- <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];
}?></h5>
									<div class="heading-elements">
										<ul class="icons-list">
            		                		<li><a data-action="collapse"></a></li>
            		                	</ul>
            	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-text2"></i> Details</a></li>
										</ul>


										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane active" id="basic-rounded-tab2">
												<div class="text-right">
                                                    <a href="languages_list.php" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
												</div>

												<form action="languages_edit.php?action=<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
" class="form-validate-jquery" method="post" autocomplete="false">
                                                    <input type="hidden" name="ppID" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
">
                                                    <input type="hidden" name="sub" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
													<!--div class="panel-heading">
														<legend style="margin-bottom:0;"><?php echo ucfirst($_smarty_tpl->tpl_vars['action']->value);?>
 Details</legend>
													</div-->

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">
                                                                															
                                                                <fieldset>
                                                                <legend class="text-semibold">Basic info</legend>

                                                                <div class="form-group">
    																<label class="text-semibold">Title <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="title" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
" required="required">
    															</div>

                                                                <div class="form-group">
    																<label class="text-semibold">Short <span class="text-danger">*</span></label>
    																<input type="text" class="form-control" name="short" value="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['short'];?>
" required="required">
    															</div>

                                                                </fieldset>

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>

																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">

                                                            <a href="languages_list.php" class="btn btn-default">Cancel</a>

															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>
												</form>

											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">
											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane" id="basic-rounded-tab4">
											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<?php echo '<script'; ?>
>
    CKEDITOR.replace( 'ptext' );
<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
