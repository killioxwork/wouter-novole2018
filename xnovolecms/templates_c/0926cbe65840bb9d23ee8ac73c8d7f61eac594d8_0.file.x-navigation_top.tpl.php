<?php
/* Smarty version 3.1.30, created on 2018-07-16 21:34:45
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\x-navigation_top.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4cf355b293d1_70846127',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0926cbe65840bb9d23ee8ac73c8d7f61eac594d8' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\x-navigation_top.tpl',
      1 => 1531481568,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b4cf355b293d1_70846127 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header"> <a class="navbar-brand" href="#"><img src="../../images/logoWhite.svg" alt="Novole"></a>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            <li><a class="sidebar-mobile-secondary-toggle"><i class="icon-more"></i></a></li>
        </ul>
    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li> <a class="sidebar-control sidebar-main-toggle hidden-xs"> <i class="icon-paragraph-justify3"></i> </a> </li>
        </ul>

        <!-- Account Settings dropdown -->
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user"> <a class="dropdown-toggle" data-toggle="dropdown"><div class="avatar"><?php echo mb_strtoupper(substr($_smarty_tpl->tpl_vars['loginname']->value,0,1), 'UTF-8');?>
</div><span><?php echo $_smarty_tpl->tpl_vars['loginname']->value;?>
</span> <i class="caret"></i> </a>
                    <ul class="dropdown-menu dropdown-menu-right">
        <?php if ($_smarty_tpl->tpl_vars['admlvl']->value == 0) {?>
                        <li><a href="admins_details.php"><i class="icon-cog5"></i> Account settings</a></li>
        <?php }?>                
                        <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /account Settings dropdown -->

    </div>
</div><?php }
}
