<?php
/* Smarty version 3.1.30, created on 2018-07-17 09:58:01
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4da18974b659_32667969',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e8a644d8b8697dfda865731e3bb095e0f611aa32' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\login.tpl',
      1 => 1531481547,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b4da18974b659_32667969 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Novole</title>
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"><?php echo '</script'; ?>
>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/core/app.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="../assets/js/custom/login.js"><?php echo '</script'; ?>
>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="../../images/logoWhite.svg" alt="Novole"></a>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Form with validation -->
					<form action="login.php" class="form-validate" method="post" novalidate>
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object-dd"><img src="../../images/logo.svg" alt="Novole"></div>
								<h5 class="content-group">Login to your account <small class="display-block">Your credentials</small>
                    <?php if ($_smarty_tpl->tpl_vars['logError']->value != '') {?>
                                <span class="validation-error-label" style="font-size:14px; display:inline-block; margin:15px 0 0 0;"><?php echo $_smarty_tpl->tpl_vars['logError']->value;?>
</span>
                    <?php }?>
                                </h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control noAutoComplete" placeholder="Username" name="username">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<!--div class="form-group login-options">
								<div class="row">
									<div class="col-sm-6">
										<label class="checkbox-inline">
											<input type="checkbox" class="styled" checked="checked">
											Remember
										</label>
									</div>

									<div class="col-sm-6 text-right">
										<a href="forgot_password.html">Forgot password?</a>
									</div>
								</div>
							</div-->

							<div class="form-group">
							    <input type="hidden" name="btn_login" value="log" />
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
							</div>

							<!--div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
							<a href="registration.html" class="btn btn-default btn-block content-group">Sign up</a>
							<span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> <a href="#">Cookie Policy</a></span-->
						</div>
					</form>
					<!-- /advanced login -->


					<!-- Footer -->
                    <div class="footer text-muted text-center"> &copy; Copyright 2018 · <a href="http://www.deeldrie.nl" target="_blank">deeldrie</a> </div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html><?php }
}
