<?php
/* Smarty version 3.1.30, created on 2018-07-20 17:10:22
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\contact_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b51fb5e6fae46_94842868',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '529a4e3f7fc0e21ca281557817aa61f14df542d2' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\contact_list.tpl',
      1 => 1532099418,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b51fb5e6fae46_94842868 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- /main sidebar --> 
    
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Contact requests </span> - Listing</h4>
          </div>
		  <div class="heading-elements">
		  </div>
        </div>
      </div>
      <!-- /page header --> 
      
      <!-- Content area -->
      <div class="content">

        <!-- Media library -->
        <div class="panel panel-white">
          <div class="panel-heading">
            <h6 class="panel-title text-semibold">Contact requests</h6>
            <div class="heading-elements">
              <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!--li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li-->
              </ul>
            </div>
          </div>
          <table class="table movies-list table-hover">
            <thead>
              <tr>
                <th>Log date</th>
                <th>Contact</th>
                <th>Remark</th>
                <th class="text-center">Checked</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
    <?php
$__section_pKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey'] : false;
$__section_pKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['pInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_pKey_0_total = $__section_pKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_pKey'] = new Smarty_Variable(array());
if ($__section_pKey_0_total != 0) {
for ($__section_pKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] = 0; $__section_pKey_0_iteration <= $__section_pKey_0_total; $__section_pKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']++){
?>
              <tr data-sid="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['code'];?>
">
                <td><?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['date'];?>
</td>
                <td>
                    <?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['name'];?>
<br />
                    <?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['phone'] != '') {?>T <?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['phone'];?>
<br /><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['country'] != '') {
echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['country'];?>
<br /><?php }?>
                    <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['email'];?>
</a>
                </td>
                <td>
        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['coment'] != '') {?>
                    <a href="#" data-placement="top" data-popup="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['coment'];?>
"><i class="icon-bubble2"></i></a>
        <?php }?>
                </td>
                <td class="text-center">
				    <label class="checkbox-inline checkbox-switchery switchery-xs">
					     <input type="checkbox" class="switchery"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['status'] == 1) {?> checked="checked"<?php }?>>&nbsp;
				    </label>
			    </td>
                <td class="text-center"><ul class="icons-list">
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="delete-reservation" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
                      </ul>
                    </li>
                  </ul></td>
              </tr>
    <?php }} else {
 ?>
              <tr>
                  <td></td>
                  <td>There is no data in table at this moment</td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
    <?php
}
if ($__section_pKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_pKey'] = $__section_pKey_0_saved;
}
?>
            </tbody>
        	<tfoot>
        		<tr>
        			<th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
        		</tr>
        	</tfoot>
          </table>
        </div>
        <!-- /media library --> 
        
        <!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- /footer -->
        
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container -->

</body>
</html>
<?php }
}
