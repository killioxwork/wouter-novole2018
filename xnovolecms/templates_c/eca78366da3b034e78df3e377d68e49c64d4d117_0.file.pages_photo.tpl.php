<?php
/* Smarty version 3.1.30, created on 2018-07-17 11:29:35
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\pages_photo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4db6ff889b18_28094529',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eca78366da3b034e78df3e377d68e49c64d4d117' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\pages_photo.tpl',
      1 => 1531819772,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4db6ff889b18_28094529 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Photo</h4>
						</div>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                        		<a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&cID=<?php echo $_smarty_tpl->tpl_vars['cID']->value;?>
" class="btn btn-default"><b><i class="icon-arrow-left13"></i></b>Back To List</a>
                        	</div>
                        </div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['menu'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
                        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] != "parent") {?>
											<li class="active"><a href="#basic-rounded-tab4" data-toggle="tab"><i class="icon-file-picture"></i> Main Photo</a></li>
                            <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?>
											<li><a href="#basic-rounded-tab5" data-toggle="tab"><i class="icon-stack-picture"></i> Photos</a></li>
                            <?php }?>
                        <?php }?>
										</ul>

										<div class="tab-content">
											<!-- Details -->
											<div class="tab-pane" id="basic-rounded-tab2">
                                            </div>
											<!-- /details -->

				  							<!-- Panoramic image -->
											<div class="tab-pane active" id="basic-rounded-tab4">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="panoramic_list.php?part=pages&set=upload&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Images</a>
												</div>
												<table class="table panoramic-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>Preview</th>
															<th>Caption title</th>
                                                            <th class="text-center">Publish</th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
										<?php
$__section_panKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey'] : false;
$__section_panKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['panInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_panKey_0_total = $__section_panKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_panKey'] = new Smarty_Variable(array());
if ($__section_panKey_0_total != 0) {
for ($__section_panKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] = 0; $__section_panKey_0_iteration <= $__section_panKey_0_total; $__section_panKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']++){
?>
														<tr data-sid="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['code'];?>
">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<td><a href="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['bigimage'];?>
" data-popup="lightbox"> <img src="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['smallimage'];?>
" alt="" class="img-rounded img-preview"></a></td>
															<td><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"<?php if ($_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['status'] == 1) {?> checked="checked"<?php }?>>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="panoramic_list.php?part=pages&set=crop&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['id'];?>
&size=3"><i class="icon-crop"></i> Crop details page</a></li>
                                                                            <!--<li><a href="panoramic_list.php?part=pages&set=crop&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_panKey']->value['index'] : null)]['id'];?>
&size=3"><i class="icon-crop"></i> Crop details</a></li>-->
                                                                            <li><a href="#" class="delete-panoramic" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        <?php }} else {
 ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>There is no images uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        <?php
}
if ($__section_panKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_panKey'] = $__section_panKey_0_saved;
}
?>
													</tbody>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
															<th></th>
                                                            <th></th>
                                                            <th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /panoramic image -->

				  							<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
												<div style="text-align:right; padding-bottom:20px;">
													<a href="photo_list.php?part=pages&set=upload&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
" class="btn btn-primary btn-sm btn-labeled"><b><i class="icon-pencil7"></i></b>Add Images</a>
                                                </div>
												<table class="table photos-list table-hover">
													<thead>
														<tr>
														    <th></th>
															<th>Preview</th>
															<th>Languages</th>
                                                            <th class="text-center">Publish</th>
                                                            <th>Actions</th>
														</tr>
													</thead>
													<tbody>
								        <?php
$__section_phKey_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey'] : false;
$__section_phKey_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['phInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_phKey_1_total = $__section_phKey_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_phKey'] = new Smarty_Variable(array());
if ($__section_phKey_1_total != 0) {
for ($__section_phKey_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] = 0; $__section_phKey_1_iteration <= $__section_phKey_1_total; $__section_phKey_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']++){
?>
														<tr data-sid="<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['code'];?>
">
														    <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
															<td>
                                                <?php if ($_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['type'] == "image") {?>
															    <a href="<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['bigimage'];?>
" data-popup="lightbox"> <img src="<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['smallimage'];?>
" alt="" class="img-rounded img-preview"></a>
                                                <?php } else { ?>
                                                                <img src="<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['smallimage'];?>
" alt="" class="img-rounded img-preview">
                                                <?php }?>
                                                            </td>
															<td><?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['lang'];?>
</td>
                                                            <td class="text-center">
																<label class="checkbox-inline checkbox-switchery switchery-xs">
																	<input type="checkbox" class="switchery"<?php if ($_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['status'] == 1) {?> checked="checked"<?php }?>>&nbsp;
																</label>
															</td>
															<td class="text-center"><ul class="icons-list">
																	<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
																		<ul class="dropdown-menu dropdown-menu-right">
																			<li><a href="photo_list.php?part=pages&set=crop&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['id'];?>
"><i class="icon-crop"></i> Crop</a></li>
                                                                            <li><a href="photo_list.php?part=pages&set=edit&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['phInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_phKey']->value['index'] : null)]['id'];?>
"><i class="icon-pencil7"></i> Edit</a></li>
                                                                            <li><a href="#" class="delete-photo" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
																		</ul>
																	</li>
																</ul></td>
														</tr>
                                        <?php }} else {
 ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>There is no images uploaded for this page</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                        <?php
}
if ($__section_phKey_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_phKey'] = $__section_phKey_1_saved;
}
?>
													</tbody>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
														</tr>
													</tfoot>
												</table>
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">
											</div>
											<!-- /documents -->

				  							<!-- Logo -->
											<div class="tab-pane" id="basic-rounded-tab7">

											</div>
											<!-- /logo -->

				  							<!-- Sponsor logo -->
											<div class="tab-pane" id="basic-rounded-tab8">

											</div>
											<!-- /Sponsor logo -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php }
}
