<?php
/* Smarty version 3.1.30, created on 2018-07-17 12:53:26
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\photo_pages_edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4dcaa685b4e9_36140166',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8db1ea337be017ae36dbc9c66314364f838bd136' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\photo_pages_edit.tpl',
      1 => 1531824804,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4dcaa685b4e9_36140166 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Photo</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
            		                		<li><a data-action="collapse"></a></li>
            		                	</ul>
            	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li><a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab4"><i class="icon-file-picture"></i> Main Photo</a></li>
											<li class="active"><a href=""><i class="icon-stack-picture"></i> Photos</a></li>
										</ul>

										<!-- Overview -->
										<div class="tab-content">


											<!-- CROP -->
											<div class="tab-pane active" id="basic-rounded-tab4">

												<div id="vpn_back">
													<a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab5" class="btn btn-default btn-sm"><b><i class="icon-arrow-left13"></i></b> Back to List</a>
												</div>

                						         <!-- start form -->
                								<form action="photo_list.php?part=pages&set=<?php echo $_smarty_tpl->tpl_vars['set']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['pID'];?>
&crID=<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['crID'];?>
" class="form-validate-jquery" method="post" autocomplete="false">
                			                        <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['set']->value;?>
" />
                                                    <input type="hidden" name="pID" value="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['pID'];?>
" />
                                                    <input type="hidden" name="cID" value="<?php echo $_smarty_tpl->tpl_vars['panInfo']->value['crID'];?>
" />
                                                    <input type="hidden" name="part" value="pages" />

													<div class="panel-body">
														<div class="row">
															<!--First Column-->
															<div class="col-md-6">

                                                                <fieldset>
                                                                <legend class="text-semibold">info</legend>
                                                        <?php
$__section_lkey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey'] : false;
$__section_lkey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['langInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lkey_0_start = min(0, $__section_lkey_0_loop);
$__section_lkey_0_total = min(ceil(($__section_lkey_0_loop - $__section_lkey_0_start)/ 2), $__section_lkey_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = new Smarty_Variable(array());
if ($__section_lkey_0_total != 0) {
for ($__section_lkey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] = $__section_lkey_0_start; $__section_lkey_0_iteration <= $__section_lkey_0_total; $__section_lkey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] += 2){
?>
                                                                <div class="form-group">
                                                                    <label class="text-semibold">Text <?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['lang'];?>
</label>
                                                                    <textarea rows="5" cols="5" id="text_<?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['langID'];?>
" name="text_<?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['langID'];?>
" class="form-control"><?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['text'];?>
</textarea>
                                                                </div>
                                                        <?php
}
}
if ($__section_lkey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = $__section_lkey_0_saved;
}
?>
                                                                </fieldset>

															</div>
															<!--Sec Column-->
															<div class="col-md-6">
																<fieldset>
																<legend class="text-semibold">details</legend>
                                                        <?php
$__section_lkey_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey'] : false;
$__section_lkey_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['langInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lkey_1_start = min(1, $__section_lkey_1_loop);
$__section_lkey_1_total = min(ceil(($__section_lkey_1_loop - $__section_lkey_1_start)/ 2), $__section_lkey_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = new Smarty_Variable(array());
if ($__section_lkey_1_total != 0) {
for ($__section_lkey_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] = $__section_lkey_1_start; $__section_lkey_1_iteration <= $__section_lkey_1_total; $__section_lkey_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] += 2){
?>
                                                                <div class="form-group">
                                                                    <label class="text-semibold">Text <?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['lang'];?>
</label>
                                                                    <textarea rows="5" cols="5" id="text_<?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['langID'];?>
" name="text_<?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['langID'];?>
" class="form-control"><?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['text'];?>
</textarea>
                                                                </div>
                                                        <?php
}
}
if ($__section_lkey_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = $__section_lkey_1_saved;
}
?>
																</fieldset>
															</div>
														</div>
														<!--goes after closed .row div tag-->

														<!--Submit button-->
														<div class="text-right">
													        <a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab5" class="btn btn-default">Cancel</a>
															<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
														</div>
														<!--end submit button-->

													</div>

                						        </form>
                						        <!--end form-->

                                            </div>
  										    <!-- /CROP -->
                                        </div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<?php
$__section_lkey_2_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey'] : false;
$__section_lkey_2_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['langInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lkey_2_total = $__section_lkey_2_loop;
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = new Smarty_Variable(array());
if ($__section_lkey_2_total != 0) {
for ($__section_lkey_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] = 0; $__section_lkey_2_iteration <= $__section_lkey_2_total; $__section_lkey_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']++){
?>

<?php echo '<script'; ?>
>
    CKEDITOR.replace( 'text_<?php echo $_smarty_tpl->tpl_vars['langInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lkey']->value['index'] : null)]['langID'];?>
',
    {
        height : '150px'
    });
<?php echo '</script'; ?>
>

<?php
}
}
if ($__section_lkey_2_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lkey'] = $__section_lkey_2_saved;
}
?>
</body>
</html>
<?php }
}
