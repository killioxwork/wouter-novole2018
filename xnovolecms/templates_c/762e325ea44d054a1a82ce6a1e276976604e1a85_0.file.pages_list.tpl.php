<?php
/* Smarty version 3.1.30, created on 2018-07-18 20:12:00
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\pages_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4f82f0c75c76_02861916',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '762e325ea44d054a1a82ce6a1e276976604e1a85' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\pages_list.tpl',
      1 => 1531937515,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4f82f0c75c76_02861916 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<body class="navbar-top">

<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- /main sidebar --> 
    
    <!-- Main content -->
    <div class="content-wrapper"> 
      
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> - Listing</h4>
          </div>
		  <div class="heading-elements">
			 <div class="heading-btn-group">
	<?php if ($_smarty_tpl->tpl_vars['lang']->value == 1) {?>
				 <a href="pages_details_edit.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&action=add<?php echo $_smarty_tpl->tpl_vars['actionTxt']->value;?>
" class="btn btn-primary btn-lg btn-labeled"><b><i class="icon-plus-circle2"></i></b>Add Page</a>
    <?php }?>
			 </div>
		  </div>
        </div>
      </div>
      <!-- /page header --> 
      
      <!-- Content area -->
      <div class="content">

        <!-- Media library -->
        <div class="panel panel-white">
          <div class="panel-heading">
            <h6 class="panel-title text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</h6>
            <div class="heading-elements">
              <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!--li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li-->
              </ul>
            </div>
          </div>
          <div style="text-align:right; padding-top: 20px; padding-right: 20px">
    <?php
$__section_lKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey'] : false;
$__section_lKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['l']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lKey_0_total = $__section_lKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_lKey'] = new Smarty_Variable(array());
if ($__section_lKey_0_total != 0) {
for ($__section_lKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] = 0; $__section_lKey_0_iteration <= $__section_lKey_0_total; $__section_lKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']++){
?>
    	      <a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['l']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['id'];?>
" class="btn btn-primary btn-sm<?php if ($_smarty_tpl->tpl_vars['l']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['id'] == $_smarty_tpl->tpl_vars['lang']->value) {?> disabled<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['l']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['long'];?>
"><?php echo $_smarty_tpl->tpl_vars['l']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['short'];?>
</a>
    <?php
}
}
if ($__section_lKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lKey'] = $__section_lKey_0_saved;
}
?>
          </div>
          <table class="table movies-list table-hover">
            <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Type</th>
                <th class="text-center">Publish</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
    <?php
$__section_pKey_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey'] : false;
$__section_pKey_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['pInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_pKey_1_total = $__section_pKey_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_pKey'] = new Smarty_Variable(array());
if ($__section_pKey_1_total != 0) {
for ($__section_pKey_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] = 0; $__section_pKey_1_iteration <= $__section_pKey_1_total; $__section_pKey_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']++){
?>
              <tr data-sid="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['code'];?>
">
                <td class="dragAndDrop"><i class="icon-dots dragula-handle"></i></td>
                <td><?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['title'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['type'];?>
</td>
                <td class="text-center">
				    <label class="checkbox-inline checkbox-switchery switchery-xs">
					     <input type="checkbox" class="switchery"<?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['status'] == 1) {?> checked="checked"<?php }
if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['status'] == '') {?> disabled<?php }?>>&nbsp;
				    </label>
			    </td>
                <td class="text-center"><ul class="icons-list">
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-menu9"></i> </a>
                      <ul class="dropdown-menu dropdown-menu-right">
        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['status'] != '') {?>
                        <li><a href="pages_details.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><i class="icon-eye"></i> Show details</a></li>
                        <li><a href="pages_photo.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['id'];?>
"><i class="icon-stack-picture"></i> Photos</a></li>
        <?php } else { ?>
                        <li><a href="pages_details_edit.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&action=add&pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><i class="icon-plus-circle2"></i> Add Language</a></li>
        <?php }?>
		<?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['type'] == 'parent') {?>
                		<li><a href="pages_list.php?part=<?php echo $_smarty_tpl->tpl_vars['part']->value;?>
&cID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['id'];?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><i class="icon-folder-open"></i> Open </a></li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_pKey']->value['index'] : null)]['del'] == 'YES') {?>
                        <li><a href="#" class="delete-pages" data-behavior="delete"><i class="icon-trash"></i> Delete</a></li>
        <?php }?>                
                      </ul>
                    </li>
                  </ul></td>
              </tr>
    <?php }} else {
 ?>
              <tr>
                  <td></td>
                  <td>There is no data in table at this moment</td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
    <?php
}
if ($__section_pKey_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_pKey'] = $__section_pKey_1_saved;
}
?>
            </tbody>
        	<tfoot>
        		<tr>
        			<th></th>
        			<th></th>
                    <th></th>
                    <th></th>
                    <th></th>
        		</tr>
        	</tfoot>
          </table>
        </div>
        <!-- /media library --> 
        
        <!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- /footer -->
        
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container -->

</body>
</html>
<?php }
}
