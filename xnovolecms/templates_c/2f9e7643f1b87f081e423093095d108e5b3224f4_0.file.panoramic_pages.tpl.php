<?php
/* Smarty version 3.1.30, created on 2018-07-17 11:20:59
  from "W:\xampp\htdocs\wouter-novole2018\xnovolecms\templates\panoramic_pages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b4db4fbc4a061_17390926',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f9e7643f1b87f081e423093095d108e5b3224f4' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\xnovolecms\\templates\\panoramic_pages.tpl',
      1 => 1531819244,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-header.tpl' => 1,
    'file:x-navigation_top.tpl' => 1,
    'file:x-navigation_left.tpl' => 1,
    'file:x-footer.tpl' => 1,
  ),
),false)) {
function content_5b4db4fbc4a061_17390926 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body class="navbar-top">

	<!-- Main navbar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
<?php $_smarty_tpl->_subTemplateRender("file:x-navigation_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $_smarty_tpl->tpl_vars['headTitle']->value;?>
</span> · Main Photo</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Storage details -->
							<div class="panel panel-flat">
								<div class="panel-heading mt-5">
									<h5 class="panel-title">Photo - <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h5>
									<div class="heading-elements">
										<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
								</div>

								<div class="panel-body remove-hdr">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-component">
											<li class="active"><a href="" data-toggle="tab"><i class="icon-file-picture"></i> Main Photo</a></li>
                                        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['type'] == "pages") {?>
											<li><a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab5" ><i class="icon-stack-picture"></i> Photos</a></li>
                                        <?php }?>    
										</ul>

										<!-- Showdates -->
										<div class="tab-content">
											<div class="tab-pane active" id="basic-rounded-tab1">
											</div>
											<!-- /showdates -->

											<!-- Details -->
											<div class="tab-pane" id="basic-rounded-tab2">
											</div>
											<!-- /details -->

											<!-- Filters -->
											<div class="tab-pane f-list" id="basic-rounded-tab3">

											</div>
											<!-- /filters -->

											<!-- Panoramic image -->
											<div class="tab-pane active" id="basic-rounded-tab4">
												<div class="text-right">
													<a href="pages_photo.php?pID=<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
#basic-rounded-tab4" class="btn btn-default"><i class="icon-arrow-left13"></i> Back To List</a>
												</div>
												<div class="panel-body">

														<!--Uploader-->
														<form class="form-horizontal">
															<div class="form-group">
																<label class="col-lg-2 control-label text-semibold">Upload files:</label>
																<div class="col-lg-10">
																	<div id="errorBlock"></div>
																	<input type="file" class="file-input-ajax" multiple="multiple" name="imageupload">
																	<span class="help-block">Minimum size image is <?php echo $_smarty_tpl->tpl_vars['panoramMinSize']->value;?>
</span>
																</div>
															</div>
														</form>
														<!--/Uploader-->
                                                        
														<?php echo '<script'; ?>
>
														$(function() {
															// AJAX upload
															$(".file-input-ajax").fileinput({
																	uploadUrl: "panoramic_action.php", // server upload action
																	uploadAsync: true,
																	maxFileCount: 5,
																	initialPreview: [],
															        allowedFileExtensions: ["jpg", "jpeg", "png"],
                                                                    /*minImageWidth: <?php echo $_smarty_tpl->tpl_vars['minWidth']->value;?>
,
                                                                    minImageHeight: <?php echo $_smarty_tpl->tpl_vars['minHeight']->value;?>
,*/
															        elErrorContainer: "#errorBlock",
        															uploadExtraData: {
        																pID: "<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['id'];?>
",
        																part: "pages",
                                                                        action: "add"
        															},
																	fileActionSettings: {
																			removeIcon: '<i class="icon-bin"></i>',
																			removeClass: 'btn btn-link btn-xs btn-icon',
																			uploadIcon: '<i class="icon-upload"></i>',
																			uploadClass: 'btn btn-link btn-xs btn-icon',
																			indicatorNew: '<i class="icon-file-plus text-slate"></i>',
																			indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
																			indicatorError: '<i class="icon-cross2 text-danger"></i>',
																			indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
																	}
															});

														});
														<?php echo '</script'; ?>
>
                                                        
												</div>

											</div>
											<!-- /panoramic image -->

											<!-- Photos -->
											<div class="tab-pane" id="basic-rounded-tab5">
											</div>
											<!-- /photos -->

											<!-- Documents -->
											<div class="tab-pane" id="basic-rounded-tab6">

											</div>
											<!-- /documents -->

										</div>
									</div>
								</div>

							</div>
							<!-- /storage details -->

						</div>

					</div>
					<!-- /detailed task -->


					<!-- Footer -->
<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php }
}
