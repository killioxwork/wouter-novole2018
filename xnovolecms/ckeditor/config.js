/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.skin = 'bootstrapck';
    config.forcePasteAsPlainText = true;
	config.allowedContent = 'p;b;i;u;ul;li;ol;s;a[!href,target];h1;h2;h3;h4;h5;h6;pre;address;div';
	//config.width = '100%';
	config.height = '400px';
	config.toolbar = 'MyToolbar';

	config.toolbar_MyToolbar =
	[
		[ 'Format' ],
		[ 'Cut','Copy','PasteText','-','Undo','Redo' ],
        [ 'Bold','Italic', 'Underline', 'Strike','-','RemoveFormat' ],
		[ 'NumberedList', 'BulletedList' ],
        [ 'Link','Unlink','Anchor' ],		
        [ 'Source' ]
	];

	config.toolbar_MyToolbarmini =
	[
		[ 'Cut','Copy','PasteText','-','Undo','Redo' ],
        [ 'Source' ]
	];
	config.toolbar_MyToolbarempty =
	[
		['Copy','PasteText','-','Undo','Redo' ]
	];
	
};
