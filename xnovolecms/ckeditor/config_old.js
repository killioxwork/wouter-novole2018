/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.skin = 'moono';
	config.width = '600px';
	config.height = '400px';
	config.toolbar = 'MyToolbar';

	config.toolbar_MyToolbar =
	[
		[ 'Cut','Copy','PasteText','-','Undo','Redo' ],
        [ 'Bold','Italic','Strike','-','RemoveFormat' ],
        [ 'Link','Unlink','Anchor' ],
        [ 'Source' ]
	];

	config.toolbar_MyToolbarmini =
	[
		[ 'Cut','Copy','PasteText','-','Undo','Redo' ],
        [ 'Source' ]
	];

	// See the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre;address;div';
};
