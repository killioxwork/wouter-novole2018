<?php
    include_once ('lib/includes.inc.php');

    require('smarty3/Smarty.class.php');

    $smarty = new Smarty;

    $smarty->setTemplateDir('templates/');
    $smarty->setCompileDir('templates_c/');
    $smarty->setConfigDir('configs/');
    $smarty->setCacheDir('cache/');

    //$smarty->caching = 0;

    $smarty->assign('siteLink', website_url);
    $smarty->assign('facebook_link', facebook_link);
    $smarty->assign('tripadviser_link', tripadviser_link);
?>