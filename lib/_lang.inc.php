<?php
    // en
    $getInfo['en']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['en']['reservation'] = "Book Novole now!";
    $getInfo['en']['review'] = "Review us:";
    $getInfo['en']['languages'] = "Languages:";
    $getInfo['en']['formname']  = "Your name";
    $getInfo['en']['formmail']  = "Your email address";
    $getInfo['en']['formphone'] = "Your phone number";
    $getInfo['en']['formcountry'] = "Your country";
    $getInfo['en']['formmsg'] = "Your message";
    $getInfo['en']['formbutton'] = "Send message";
    $getInfo['en']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['en']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['en']['footermobile'] = "Mobile";
    $getInfo['en']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



    // it
    $getInfo['it']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['it']['reservation'] = "Book Novole now!";
    $getInfo['it']['review'] = "Review us:";
    $getInfo['it']['languages'] = "Languages:";
    $getInfo['it']['formname']  = "Your name";
    $getInfo['it']['formmail']  = "Your email address";
    $getInfo['it']['formphone'] = "Your phone number";
    $getInfo['it']['formcountry'] = "Your country";
    $getInfo['it']['formmsg'] = "Your message";
    $getInfo['it']['formbutton'] = "Send message";
    $getInfo['it']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['it']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['it']['footermobile'] = "Mobile";
    $getInfo['it']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



    // de
    $getInfo['de']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['de']['reservation'] = "Book Novole now!";
    $getInfo['de']['review'] = "Review us:";
    $getInfo['de']['languages'] = "Languages:";
    $getInfo['de']['formname']  = "Your name";
    $getInfo['de']['formmail']  = "Your email address";
    $getInfo['de']['formphone'] = "Your phone number";
    $getInfo['de']['formcountry'] = "Your country";
    $getInfo['de']['formmsg'] = "Your message";
    $getInfo['de']['formbutton'] = "Send message";
    $getInfo['de']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['de']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['de']['footermobile'] = "Mobile";
    $getInfo['de']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



    // nl
    $getInfo['nl']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['nl']['reservation'] = "Book Novole now!";
    $getInfo['nl']['review'] = "Review us:";
    $getInfo['nl']['languages'] = "Languages:";
    $getInfo['nl']['formname']  = "Your name";
    $getInfo['nl']['formmail']  = "Your email address";
    $getInfo['nl']['formphone'] = "Your phone number";
    $getInfo['nl']['formcountry'] = "Your country";
    $getInfo['nl']['formmsg'] = "Your message";
    $getInfo['nl']['formbutton'] = "Send message";
    $getInfo['nl']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['nl']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['nl']['footermobile'] = "Mobile";
    $getInfo['nl']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



    // es
    $getInfo['es']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['es']['reservation'] = "Book Novole now!";
    $getInfo['es']['review'] = "Review us:";
    $getInfo['es']['languages'] = "Languages:";
    $getInfo['es']['formname']  = "Your name";
    $getInfo['es']['formmail']  = "Your email address";
    $getInfo['es']['formphone'] = "Your phone number";
    $getInfo['es']['formcountry'] = "Your country";
    $getInfo['es']['formmsg'] = "Your message";
    $getInfo['es']['formbutton'] = "Send message";
    $getInfo['es']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['es']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['es']['footermobile'] = "Mobile";
    $getInfo['es']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



    // pl
    $getInfo['pl']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> about cookies and our privacy policy.";
    $getInfo['pl']['reservation'] = "Book Novole now!";
    $getInfo['pl']['review'] = "Review us:";
    $getInfo['pl']['languages'] = "Languages:";
    $getInfo['pl']['formname']  = "Your name";
    $getInfo['pl']['formmail']  = "Your email address";
    $getInfo['pl']['formphone'] = "Your phone number";
    $getInfo['pl']['formcountry'] = "Your country";
    $getInfo['pl']['formmsg'] = "Your message";
    $getInfo['pl']['formbutton'] = "Send message";
    $getInfo['pl']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['pl']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['pl']['footermobile'] = "Mobile";
    $getInfo['pl']['footertext'] = "Vakantiehuis, boerderij en appartementen te huur in Cortona in Toscane. De accomodaties zijn gelegen dichtbij
                                    het meer van Trasimeno in de provincie Arezzo in de buurt van Siena, Montepulciano, Perugia, San Gimignano
                                    en Florence.";



?>