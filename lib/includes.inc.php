<?php
	session_start();
	
	require_once 'settings.inc.php';
    require_once 'lang.inc.php';

	/*
	 * database
	 */
	require_once site_absolute_path . '/classes/db.php';
    require_once site_absolute_path . '/classes/PasswordHash.php';
    require_once site_absolute_path . '/classes/download_class.php';
    //require_once site_absolute_path . 'lib/classes/coding.php';
    require_once site_absolute_path . '/classes/mailing.php';
    require_once site_absolute_path . '/classes/protection.php';
    require_once site_absolute_path . '/classes/multifunction.php';
    require_once site_absolute_path . '/classes/upload.php';
    require_once site_absolute_path . '/classes/SimpleImage.php';
    require_once site_absolute_path . '/classes/backactionclass.php';
    require_once site_absolute_path . '/classes/frontquery.php';

	/*
	 * PHP mailer
	 */
	require_once site_absolute_path . '/phpmailer/class.phpmailer.cr.php';

	$db = new db();
    //$coding = new codeclass();
    $multifunc = new multifunc_class();
    $frontfunc = new front_class();
    $downfunc = new download_class();
    $mailclass = new mailingclass();
?>