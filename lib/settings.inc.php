<?php
	/*
	 * site settings
	 */
    ini_set('default_charset', 'utf-8');

    define('SESSION_KEY', md5(__FILE__));
    define('enc_charset', 'UTF-8');
    define('site_absolute_path', __DIR__);
	define('site_relative_path', '');
    define('cmscheck', 'ADMIN-NOVOLE');
    define('usrcheck', 'USER-NOVOLE');
    define('cmscheckvalue', 'ADM-NOVOLE2018');
    define('usrcheckvalue', 'USR-NOVOLE2018');
    define('website_url', 'http://localhost/wouter-novole2018/');
    define('website_root', 'http://localhost/wouter-novole2018/');
    define('website_cms', 'http://localhost/wouter-novole2018/xnovolecms/');
    define('website_from_name', 'Novole');
    define('website_from_email_address', 'root@riddler.deeldrie.org');
    define('website_to_name_contact', 'Novole contact');
    define('website_to_email_address_contact', 'mail@novole.com');
    define('website_admin_name', 'Novole administrator');
    define('website_admin_email_address', '');  // add email address in order to receive automatic BCC's of order mails

    define('site_files_depth', '../../');
    define('front_files_depth', '');

    define('facebook_link', 'https://www.facebook.com/');
    define('tripadviser_link', 'https://www.tripadvisor.com/');

	/*
	 * db settings - cms
	 */
	define('dbhost', 'localhost');
	define('dbusername', 'root');
	define('dbpassword', '');
	define('dbname', 'wnovole2018');


	/*
	 * blocks
	 */
    define('page_blok', 'pagina');

	/*
	 * type arrey
	 */
    $movType = array('.mov');
    $wordType = array('.doc','docx','.rtf','.txt');
    $pdfType = array('.pdf');
    $imgType = array('.jpg','jpeg');
    $zipType = array('.zip','.rar','gzip');

    $iconsType = array( 'mov'  => 'file-video-o',
                        'doc'  => 'file-word-o',
                        'docx' => 'file-word-o',
                        'rtf'  => 'file-word-o',
                        'txt'  => 'file-text-o',
                        'pdf'  => 'file-pdf-o',
                        'jpg'  => 'file-image-o',
                        'jpeg' => 'file-image-o',
                        'png' => 'file-image-o',
                        'zip'  => 'file-archive-o',
                        'rar'  => 'file-archive-o',
                        'gzip' => 'file-archive-o',
                        'xls'  => 'file-excel-o',
                        'none' => 'file-o');


	/*
	 * image subfolders
	 */
    $imageMaxSize = '4000x4000';
    $panoramMinSize = '1920x780';   // format is widthxheight but can be only one or empty?
    $panoramArrayinfo = array( 'column_1' => '70x70',     // cms listing
                               'column_2' => '1920x780',   // size in menu
                               'column_3' => '1920x780');  // size in details
    $imageMinSize = '792x100';
    $imageArrayinfo = array( 'column_1' => '70x70',     // cms listing
                             'column_2' => '792x',   // details page
                             'column_3' => '1200x'); // max listing

    $nameArrayInfo = array( 'pages' => 'novole_pagina');

	/*
	 * cms default image
	 */
    define('cms_default_listimage', '../assets/images/placeholder.jpg');

	/*
	 * front default image
	 */
    define('default_panoramic', website_url.'images/default-image-1920x780.gif');
    define('default_photo', website_url.'images/default_photo.jpg');
    define('default_video', website_url.'images/video_icon_small.jpg');

	/*
	 * number per page
	 */
    define('number_per_page', 10);
    define('search_per_page', 10);

	/*
	 * password check number of days
	 */
    define('day_number', 5);

	/*
	 * list of days, month
	 */
    $dayarray = array("zondag","maandag","dinsdag","woensdag","donderdag","vrijdag","zaterdag");
    $dayshortarray = array("Zo","Ma","Di","Wo","Do","Vr","Za");
    $montharray = array("januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december");
    $coursesDay = array("","Maandag","Dinsdag","Woensdag","Donderdag","Vrijdag","Zaterdag","Zondag");


    /*
    ** Useful constants for use with the third parameter of these
    ** functions (to discard or preserve the enclosing tags of the
    ** named element).
    */
    define('ELEMENT_CONTENT_ONLY', true);
    define('ELEMENT_PRESERVE_TAGS', false);

	/*
	 * iPad
	 */
     //$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');

	/*
	 * error handling
	 */
	//ini_set('display_errors','1');
	
	// Turn off all error reporting
	//error_reporting(0);

	// Report simple running errors
	//error_reporting(E_ERROR | E_WARNING | E_PARSE);
	
	// Reporting E_NOTICE can be good too (to report uninitialized
	// variables or catch variable name misspellings ...)
	//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
	
	// Report all errors except E_NOTICE
	// This is the default value set in php.ini
	//error_reporting(E_ALL ^ E_NOTICE);
    //error_reporting(E_ALL & ~E_NOTICE);
    //error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
	
	// Report all PHP errors (see changelog)
	//error_reporting(E_ALL);
	
	// Report all PHP errors
	//error_reporting(-1);

?>