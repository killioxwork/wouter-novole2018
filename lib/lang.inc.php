<?php
    // en
    $getInfo['en']['headtitle'] = "agriturismo novole";
    $getInfo['en']['policy'] = "This website uses cookies. Click <a href='policylink' title='Here you will find more information'>here</a> to learn more.";
    $getInfo['en']['reservation'] = "Book Novole now!";
    $getInfo['en']['review'] = "Review us:";
    $getInfo['en']['languages'] = "Languages:";
    $getInfo['en']['formname']  = "Your name";
    $getInfo['en']['formmail']  = "Your email address";
    $getInfo['en']['formphone'] = "Your phone number";
    $getInfo['en']['formcountry'] = "Your country";
    $getInfo['en']['formmsg'] = "Your message";
    $getInfo['en']['formbutton'] = "Send message";
    $getInfo['en']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['en']['footertitle'] = "Agriturismo Novole of Luciano Sansone";
    $getInfo['en']['footermobile'] = "Mobile";
    $getInfo['en']['footertext'] = "Farmhouse near Cortona in Tuscany, between the Trasimeno lake and Siena, Arezzo, Montepulciano, Perugia, Assisi, San Gimignano and 										Florence.";



    // it
    $getInfo['it']['headtitle'] = "agriturismo novole";
    $getInfo['it']['policy'] ="Questo sito utilizza cookie. Per ulteriori informazioni, fare clic <a href='policylink' title='Here you will find more information'>qui. 									</a>.";
	$getInfo['it']['reservation'] = "Prenota Novole ora!";
    $getInfo['it']['review'] = "Lascia una recensione:";
    $getInfo['it']['languages'] = "Lingue:";
    $getInfo['it']['formname']  = "Il tuo nome";
    $getInfo['it']['formmail']  = "La tua email";
    $getInfo['it']['formphone'] = "Il tuo n. di telefono";
    $getInfo['it']['formcountry'] = "Il tuo paese";
    $getInfo['it']['formmsg'] = "Il tuo messaggio";
    $getInfo['it']['formbutton'] = "Invia messaggio";
    $getInfo['it']['formpolicy'] = "Inviando il modulo dichiari di acconsentire al trattamento dei dati personali";
    $getInfo['it']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['it']['footermobile'] = "Cellulare";
    $getInfo['it']['footertext'] = "Agroturismo vicino a Cortona in Toscana, tra il Lago di Trasimeno, Siena, Arezzo, Montepulciano, Perugia, Assisi, San Gimignano e Firenze.";



    // de
    $getInfo['de']['headtitle'] = "agriturismo novole";
	$getInfo['de']['policy'] = "Diese Website verwendet Cookies. Für weitere Informationen <a href='policylink' title='Here you will find more information'>hier</a> 									klicken.";
    $getInfo['de']['reservation'] = "Reserviere Novole jetzt!";
    $getInfo['de']['review'] = "Bewerte uns!:";
    $getInfo['de']['languages'] = "Sprachen:";
    $getInfo['de']['formname']  = "deine Name";
    $getInfo['de']['formmail']  = "deine E-Mail Adresse";
    $getInfo['de']['formphone'] = "dein Telefon";
    $getInfo['de']['formcountry'] = "dein Land";
    $getInfo['de']['formmsg'] = "deine Nachricht";
    $getInfo['de']['formbutton'] = "Nachricht senden";
    $getInfo['de']['formpolicy'] = "Mit dem Absenden dieses Formulars erklärst du dich mit unseren Datenschutzbestimmungen einverstanden";
    $getInfo['de']['footertitle'] = "Agriturismo Novole von Luciano Sansone";
    $getInfo['de']['footermobile'] = "Handy";
    $getInfo['de']['footertext'] = "Landhaus in der Nähe von Cortona in Toskana, zwischen dem Trasimenischen See, Siena, Montepulciano, Arezzo, Perugia, Assisi, San 									Gimignano und Firenze.";



	// nl
    $getInfo['nl']['headtitle'] = "agriturismo novole";
    $getInfo['nl']['policy'] = "Deze site maakt gebruik van cookies. Klik <a href='policylink' title='Here you will find more information'>hier</a> voor meer 											informatie.";
	$getInfo['nl']['reservation'] = "Boek Novole nu!";
    $getInfo['nl']['review'] = "Beoordeel ons:";
    $getInfo['nl']['languages'] = "Talen:";
    $getInfo['nl']['formname']  = "Je naam";
    $getInfo['nl']['formmail']  = "Je e-mailadres";
    $getInfo['nl']['formphone'] = "Je telefoonnummer";
    $getInfo['nl']['formcountry'] = "Je Land";
    $getInfo['nl']['formmsg'] = "Je bericht";
    $getInfo['nl']['formbutton'] = "Stuur bericht";
    $getInfo['nl']['formpolicy'] = "Door dit formulier op te sturen ga je akkoord met onze privacy policy";
    $getInfo['nl']['footertitle'] = "Agriturismo Novole van Luciano Sansone";
    $getInfo['nl']['footermobile'] = "Mobiele telefoon";
    $getInfo['nl']['footertext'] = "Agriturismo nabij Cortona in Toscane, tussen het meer van Trasimeno, Siena, Montepulciano, Arezzo, Perugia, Assisi, San 												Gimignano en Florence.";

    
    // es
    $getInfo['es']['headtitle'] = "agriturismo novole";
    $getInfo['es']['policy'] = "Este sitio usa cookies. Hace clic <a href='policylink' title='Here you will find more information'>aquí</a> para obtener más 										información.";
    $getInfo['es']['reservation'] = "Reserva Novole ahora!";
    $getInfo['es']['review'] = "Opiniones de nosotras:";
    $getInfo['es']['languages'] = "Lenguas:";
    $getInfo['es']['formname']  = "Tu nombre";
    $getInfo['es']['formmail']  = "Tu dirección e-mail";
    $getInfo['es']['formphone'] = "Tu telefono";
    $getInfo['es']['formcountry'] = "Tu país";
    $getInfo['es']['formmsg'] = "Tu Mensaje";
    $getInfo['es']['formbutton'] = "Envía el Mensaje";
    $getInfo['es']['formpolicy'] = "Si envías este formulario, estás de acuerdo con nosotras reglas de la privacy";
    $getInfo['es']['footertitle'] = "Agriturismo Novole de Luciano Sansone";
    $getInfo['es']['footermobile'] = "Móvil";
    $getInfo['es']['footertext'] = "Agroturismo cercano a Cortona en la Toscana, entre el lago de Trasimeno, Siena, Arezzo, Montepulciano, Perugia, Assisi, San 											Gimignano y Florencia.";



    // pl
    $getInfo['pl']['headtitle'] = "agriturismo novole";
    $getInfo['pl']['policy'] = "This website uses cookies. <a href='policylink' title='Here you will find more information'>Here you will find more information</a> 									about cookies and our privacy policy.";
    $getInfo['pl']['reservation'] = "Book Novole now!";
    $getInfo['pl']['review'] = "Review us:";
    $getInfo['pl']['languages'] = "Languages:";
    $getInfo['pl']['formname']  = "Your name";
    $getInfo['pl']['formmail']  = "Your email address";
    $getInfo['pl']['formphone'] = "Your phone number";
    $getInfo['pl']['formcountry'] = "Your country";
    $getInfo['pl']['formmsg'] = "Your message";
    $getInfo['pl']['formbutton'] = "Send message";
    $getInfo['pl']['formpolicy'] = "By sending this form you agree with our privacy policy";
    $getInfo['pl']['footertitle'] = "Agriturismo Novole di Luciano Sansone";
    $getInfo['pl']['footermobile'] = "Mobile";
    $getInfo['pl']['footertext'] = "Farmhouse near Cortona in Tuscany, between the Trasimeno lake and Siena, Arezzo, Montepulciano, Perugia, Assisi, San Gimignano and 										Florence.";



?>