<?php
	class protection_class{

        public function get_ip_address() {
            if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
                 $ip=$_SERVER['HTTP_CLIENT_IP']; // share internet
            } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                 $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy
            } else {
                 $ip=$_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }
        public function get_ip_address_mm(){
            foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
                if (array_key_exists($key, $_SERVER) === true){
                    foreach (explode(',', $_SERVER[$key]) as $ip){
                        $ip = trim($ip); // just to be safe

                        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                            return $ip;
                        }
                    }
                }
            }
        }
        public function white_ip(){
            $logIP = $this->get_ip_address();
            $ipQ = db::$mysqli->query(sprintf("SELECT * FROM admin_accounts WHERE adminOnline='1'
                                                                              AND adminIP<>''
                                                                              AND adminIP ='%s'", db::$mysqli->escape_string($logIP)));
            if($ipQ->num_rows>0){
                 return true;
            } else {
                 return false;
            }
        }

        //testing for login
        public function mailcheck( $uname=''){
            $logIP = $this->get_ip_address();
            if ($uname == ""){
              return 1;
            }
            if ($uname != "") {
              $rs = db::$mysqli->query(sprintf("SELECT * FROM admin_accounts WHERE userName ='%s'", db::$mysqli->escape_string($uname)));
              if ($r = $rs->fetch_assoc() and preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $uname)) {
                 if($r['adminOnline']=='1'){
                     if($this->maillock()){
                        return $this->cms_sendmail($r['adminID'], $logIP);
                     } else {
                        return 5;
                     }
                 } else {
                     return 2;
                 }
              } else {
                 if($this->ip_number_check()){
                     return 3;
                 } else {
                     return 4;
                 }
              }
            }
        }

        public function maillock(){
            $logIP = $this->get_ip_address();
            $ipQ = db::$mysqli->query("SELECT * FROM admin_log_mail WHERE mailSentdate>='".date("Y-m-d H:i:s", mktime(date('H'), date('i')-15, date('s'), date('m'), date('d'), date('Y')))."'
                                                                      AND IPadres<>''
                                                                      AND IPadres ='".$logIP."'
                                                                      AND confirmCheck='0'
                                                                      AND mailSent='1'");
            if($ipQ->num_rows>0){
                return false;
            } else {
                return true;
            }
        }

        public function blocked_ip(){
            $logIP = $this->get_ip_address();
            $ipQ = db::$mysqli->query(sprintf("SELECT * FROM admin_log WHERE blockStartdate>='".date("Y-m-d H:i:s", mktime(date('H'), date('i'), date('s'), date('m'), date('d')-1, date('Y')))."'
                                                                        AND IPadres<>''
                                                                        AND IPadres ='%s'", db::$mysqli->escape_string($logIP)));
            if($ipQ->num_rows>0){
                 return true;
            } else {
                 return false;
            }
        }

        public function ip_number_check(){
            $logIP = $this->get_ip_address();
            $ipQ = db::$mysqli->query(sprintf("SELECT * FROM admin_log WHERE IPadres<>''
                                                                         AND IPadres ='%s' ORDER BY startLogdate DESC", db::$mysqli->escape_string($logIP)));
            if($ipInfo = $ipQ->fetch_assoc()){
                if($ipInfo['startLogdate']>date("Y-m-d H:i:s", mktime(date('H')-1, date('i'), date('s'), date('m'), date('d'), date('Y')))){
                    if($ipInfo['countNumber']==3){
                        $extra = db::$mysqli->query("UPDATE admin_log SET countNumber=4,
                                                                          blockStartdate='".date("Y-m-d H:i:s")."'
                                                                      WHERE pageID='".$ipInfo['pageID']."'");
                        return false;
                    } else {
                        $extra = db::$mysqli->query("UPDATE admin_log SET countNumber=countNumber+1
                                                                      WHERE pageID='".$ipInfo['pageID']."'");
                        return true;
                    }
                } else {
                    $extra = db::$mysqli->query("INSERT INTO admin_log SET IPadres='".$logIP."',
                                                                           startLogdate='".date("Y-m-d H:i:s")."',
                                                                           countNumber=1");
                    return true;
                }
            } else {
                $extra = db::$mysqli->query("INSERT INTO admin_log SET IPadres='".$logIP."',
                                                                       startLogdate='".date("Y-m-d H:i:s")."',
                                                                       countNumber=1");
                return true;
            }
        }

        public function cms_sendmail($id, $ip){

            $rs = db::$mysqli->query(sprintf("SELECT * FROM admin_accounts WHERE adminID ='%s'", db::$mysqli->escape_string($id)));
            $r = $rs->fetch_assoc();

            if(preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $r['userName'])){


                $datatime = date("Y-m-d H:i:s");
                $mailTemplate =  '../inc_mail/cms_mail_confirm.inc.php';

                $mailInfo['pageSubject'] = 'Novole Account Renewal Information.';

                $replacements = array(
                      "[[ipvalue]]" 	    => $ip,
                      "[[confirmlink]]" 	=> 'redactie/index.php?code='.md5($datatime),
                      "[[datetime]]" 	    => date("Y-m-d H:i:s", mktime(date('H')+1, date('i'), date('s'), date('m'), date('d'), date('Y'))),
                      "[[SITELINK]]" 	    => website_root
                );

                $uInfo['email'] = $r['userName'];
                $uInfo['name'] = db::decodeString($r['adminName']);

                $mailFeedback = mailingclass::mailsending($datatime,$mailInfo,$uInfo,$uInfo['name'],$replacements,$attachment,$mailTemplate);

                $q1= db::$mysqli->query("INSERT INTO admin_log_mail SET
                                                     adminID = '".$id."',
                                                     IPadres = '".$ip."',
                                                     mailSent='".$mailFeedback['mailissent']."',
                                                     mailSentdate = '".$datatime."',
                                                     confirmCheck = '0',
                                                     confirmCode = '".md5($datatime)."',
                                                     error = ".$mailFeedback['error'].",
                                                     errormessage = '".$mailFeedback['message']."'");

                if($mailFeedback['mailissent']==0){
                    return 6;
                }

            }

        }

        public function cms_check_code($code){
            //$solution = 'false';
            $logIP = $this->get_ip_address();
            $ipQ = db::$mysqli->query("SELECT * FROM admin_log_mail WHERE mailSentdate>='".date("Y-m-d H:i:s", mktime(date('H')-1, date('i'), date('s'), date('m'), date('d'), date('Y')))."'
                                                                      AND IPadres<>''
                                                                      AND IPadres ='".$logIP."'
                                                                      AND confirmCheck='0'
                                                                      AND mailSent='1'");
            while($ipInfo = $ipQ->fetch_assoc()){ // or $solution=='false'
                // check admin
                $rs = db::$mysqli->query("SELECT * FROM admin_accounts WHERE adminID ='".$ipInfo['adminID']."' AND adminOnline='1'");
                $r = $rs->fetch_assoc();
                if(md5($ipInfo['mailSentdate'])==$code){
                    $q1= db::$mysqli->query("UPDATE admin_log_mail SET confirmCheck='1' WHERE pageID='".$ipInfo['pageID']."'");
                    $q2= db::$mysqli->query("UPDATE admin_accounts SET adminIP='".$ipInfo['IPadres']."' WHERE adminID='".$r['adminID']."'");

                    $solution = 'true';
                }
            }
            if($solution=='true'){
                $this->generate_access_list();
            }
        }

        public function generate_access_list(){
            $fileName = "../whitelist/ip.allow";
            $handle = fopen($fileName, 'w') or die("can't open file");
            $rs = db::$mysqli->query("SELECT DISTINCT adminIP FROM admin_accounts WHERE adminIP<>''");
            while($rInfo = $rs->fetch_assoc()){
                fwrite($handle, $rInfo['adminIP']." allow\n");
            }
            fclose($handle);
        }

        //testing for login
        public function login( $uname='', $pword=''){
            if ($uname == "" and $pword == ""){
               return 1;
            }
            if ($uname == "" or $pword == ""){
               return 2;
            }
            if ($uname != "" and  $pword != "") {
                $rs = db::$mysqli->query(sprintf("SELECT * FROM admin_accounts WHERE userName ='%s'", db::$mysqli->escape_string($uname))) or die("error in line ".__LINE__." : ".mysqli_error());
                if ($r = $rs->fetch_assoc()) {
                    $hasher = new PasswordHash(8, false);
                    if ($hasher->CheckPassword($pword, $r['passWord'])) {
                        if($r['adminOnline']=='1'){
                           $extra = db::$mysqli->query("UPDATE admin_accounts SET lastTimelog='".date("Y-m-d H:i:s")."' WHERE adminID='".$r['adminID']."'") or die("error in line ".__LINE__." : ".mysqli_error());
                           $_SESSION[cmscheck] = cmscheckvalue;
                           $_SESSION[SESSION_KEY]['adminInfo'] = $r;
                           return 0;
                        } else {
                           return 5;
                        }
                    } else { return 4; }
                } else { return 3; }
            }
        }

        // detection of browser
        public function getBrowser(){
          $u_agent = $_SERVER['HTTP_USER_AGENT'];
          $bname = 'Unknown';
          $platform = 'Unknown';
          $version= "";

          //First get the platform?
          if (preg_match('/linux/i', $u_agent)) {
              $platform = 'linux';
          }
          elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
              $platform = 'mac';
          }
          elseif (preg_match('/windows|win32/i', $u_agent)) {
              $platform = 'windows';
          }

          // Next get the name of the useragent yes seperately and for good reason
          if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
          {
              $bname = 'Internet Explorer';
              $ub = "MSIE";
          }
          elseif(preg_match('/Firefox/i',$u_agent))
          {
              $bname = 'Mozilla Firefox';
              $ub = "Firefox";
          }
          elseif(preg_match('/Chrome/i',$u_agent))
          {
              $bname = 'Google Chrome';
              $ub = "Chrome";
          }
          elseif(preg_match('/Safari/i',$u_agent))
          {
              $bname = 'Apple Safari';
              $ub = "Safari";
          }
          elseif(preg_match('/Opera/i',$u_agent))
          {
              $bname = 'Opera';
              $ub = "Opera";
          }
          elseif(preg_match('/Netscape/i',$u_agent))
          {
              $bname = 'Netscape';
              $ub = "Netscape";
          }
          elseif(preg_match('/android/i',strtolower($u_agent)))
          {
              $bname = 'Android';
              $ub = "Android";
          }

          // finally get the correct version number
          $known = array('Version', $ub, 'other');
          $pattern = '#(?<browser>' . join('|', $known) .
          ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
          if (!preg_match_all($pattern, $u_agent, $matches)) {
              // we have no matching number just continue
          }

          // see how many we have
          $i = count($matches['browser']);
          if ($i != 1) {
              //we will have two since we are not using 'other' argument yet
              //see if version is before or after the name
              if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                  $version= $matches['version'][0];
              }
              else {
                  $version= $matches['version'][1];
              }
          }
          else {
              $version= $matches['version'][0];
          }

          // check if we have a number
          if ($version==null || $version=="") {$version="?";}

          return array(
              'userAgent' => $u_agent,
              'name'      => $bname,
              'shortname' => $ub,
              'version'   => $version,
              'platform'  => $platform,
              'pattern'   => $pattern
          );
        }


        public function browser_redirect(){
          $ua = $this->getBrowser();
          $versionList = explode(".", $ua['version']);

          if(($ua['shortname']=='MSIE' and (int)$versionList[0]<8) or ($ua['shortname']=='Firefox' and (int)$versionList[0]<12)
              or ($ua['shortname']=='Safari' and (int)$versionList[0]<4) or ($ua['shortname']=='Opera' and (int)$versionList[0]<10)
              or ($ua['shortname']=='Chrome' and (int)$versionList[0]<20)){

              return true;
          } else {
              return false;
          }
        }

        public function android_redirect(){
          $ua = $this->getBrowser();

          if($ua['name']=='Android'){

              return true;
          } else {
              return false;
          }
        }
    }
?>