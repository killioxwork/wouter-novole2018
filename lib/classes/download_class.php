<?php
    class download_class{
        /**
         * Function for browser detect
         */
        public function id_browser() {
            $browser=$GLOBALS['__SERVER']['HTTP_USER_AGENT'];
            if(ereg('Opera(/| )([0-9].[0-9]{1,2})', $browser)) {
                return 'OPERA';
            } else if(ereg('MSIE ([0-9].[0-9]{1,2})', $browser)) {
                return 'IE';
            } else if(ereg('OmniWeb/([0-9].[0-9]{1,2})', $browser)) {
                return 'OMNIWEB';
            } else if(ereg('(Konqueror/)(.*)', $browser)) {
                return 'KONQUEROR';
            } else if(ereg('Mozilla/([0-9].[0-9]{1,2})', $browser)) {
                return 'MOZILLA';
            } else {
                return 'OTHER';
            }
        }
        /**
         * Function for browser detect
         */
        public function get_user_browser(){
            $u_agent = $_SERVER['HTTP_USER_AGENT'];
            $ub = '';
            if(preg_match('/MSIE/i',$u_agent)){
                $ub = "IE";
            } elseif(preg_match('/Firefox/i',$u_agent)){
                $ub = "MOZILLA";
            } elseif(preg_match('/Safari/i',$u_agent)){
                $ub = "SAFARI";
            } elseif(preg_match('/Chrome/i',$u_agent)){
                $ub = "CHROME";
            } elseif(preg_match('/Flock/i',$u_agent)){
                $ub = "FLOCK";
            } elseif(preg_match('/Opera/i',$u_agent)){
                $ub = "OPERA";
            } else {
                $ub = "OTHER";
            }

            return $ub;
        }
        /**
         * Function for force file download
         */
        public function download_table_item($selectedCheck, $code, $sFolder, $publishcheck='') {		// download file
            if($selectedCheck=='d_c'){
                $sTable = 'documents';
            }
            $extraQ = "";
            if($publishcheck == 'YES'){
                $extraQ .= " AND pageOnline='1'";
            }
            $rs = db::$mysqli->query(sprintf("SELECT * FROM %s WHERE pageCode='%s'".$extraQ,
                                                                  db::$mysqli->escape_string($sTable),
                                                                  db::$mysqli->escape_string($code)));
            if($r = $rs->fetch_assoc()){
                $browser = $this->get_user_browser();

                $item = $r['pageFile'];
                $abs_item = $sFolder."/".$item;

            	header('Content-Type: '.(($browser=='IE' || $browser=='OPERA')?
            		  'application/octetstream':'application/octet-stream'));
                header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
            	header('Content-Transfer-Encoding: binary');
            	header('Content-Length: '.filesize($abs_item));
            	  if($browser=='IE') {
            		  header('Content-Disposition: inline; filename="'.$item.'"');
            		  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            		  header('Pragma: public');
            	  } else {
            		  header('Content-Disposition: attachment; filename="'.$item.'"');
            		  header('Cache-Control: no-cache, must-revalidate');
            		  header('Pragma: no-cache');
            	  }

            	@readfile($abs_item);
            	exit;
            } else {
                exit;
            }
        }
    }
?>