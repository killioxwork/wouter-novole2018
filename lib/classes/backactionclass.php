<?php
     class backaction_class{

        public function getTableName($simbol) {

            switch ($simbol) {
                case 'PG': // Pages
                case 'PGPANO':
                case 'PGDOC':
                    $partinfo['table'] = 'pages';
                    $partinfo['label'] = 'pages';
                    $partinfo['section'] = 'photo';
                    break;
                case 'LANG': // Langugaes
                    $partinfo['table'] = 'languages';
                    break;
                case 'DOC': // accounts
                    $partinfo['table'] = 'documents';
                    $partinfo['label'] = 'accounts';
                    break;
                case 'ADM': // admin
                    $partinfo['table'] = 'admin_accounts';
                    break;
            }

            return $partinfo;
        }

    }
?>