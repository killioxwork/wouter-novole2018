<?php
	class db{
		public static $mysqli = '';

		/**
		 * establish a connection
		 */
		public function __construct(){
			/*
			 * connect to database server and select the database
			 */
            if (!self::$mysqli = mysqli_init()){
                die("mysqli_init failed");
            }

            if (!self::$mysqli->real_connect(dbhost, dbusername, dbpassword, dbname, 3306, null, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT)){
                die('Connect Error (' . self::$mysqli->connect_errno . ') ' . self::$mysqli->connect_error);
            } else {
                //echo"Connected";
            }

			/*
			 * check for charset
			 */
            if (!self::$mysqli->set_charset("utf8")) {
                printf("Error loading character set utf8: %s\n", self::$mysqli->error);
            }
		}

		public function __destruct(){
			//db::$mysqli->close();
			//$this::$mysqli->close();
			//self::$mysqli->close();
		}
        
		/**
		 * encode (htmlentities)
		 * @param array $array ($_POST)
		 * @return array
		 */
		static function encodeHTMLPost($array){
			foreach ($array AS $k => $v) {
				if(!is_array($v)){
					$array[$k] = self::encodeString($v);
				}
			}

			return $array;
		}

		static function encodeString($string = ''){
			//$string = htmlentities($string, ENT_QUOTES, enc_charset);
            $string = htmlspecialchars($string, ENT_QUOTES, enc_charset);
			return $string;
		}

		/**
		 * decodes a encoded HTML string
		 * @param string $string
		 * @return string
		 */
		static function decodeString($string = '', $quotes=''){
            if($quotes==''){
			    //$string = html_entity_decode($string, ENT_QUOTES, enc_charset);
                $string = htmlspecialchars_decode($string, ENT_QUOTES);
            } else {
                //$string = html_entity_decode($string, ENT_NOQUOTES, enc_charset);
                $string = htmlspecialchars_decode($string, ENT_NOQUOTES);
            }
			return $string;
		}

		/**
		 * Convert special characters to HTML entities
		 * @param string $string
		 * @return string
		 */
		static function specialcharString($string = ''){
            $string = htmlspecialchars($string);

			return $string;
		}

	}

?>