<?php
    class codeclass{

		/**
		 * encode (htmlentities)
		 * @param array $array ($_POST)
		 * @return array
		 */
		static function encodeHTMLPost($array){
			foreach ($array AS $k => $v) {
				if(!is_array($v)){
					$array[$k] = self::encodeString($v);
				}
			}

			return $array;
		}

		static function encodeString($string = ''){
			//$string = htmlentities($string, ENT_QUOTES, enc_charset);
            $string = htmlspecialchars($string, ENT_QUOTES, enc_charset);
			return $string;
		}

		/**
		 * decodes a encoded HTML string
		 * @param string $string
		 * @return string
		 */
		static function decodeString($string = '', $quotes=''){
            if($quotes==''){
			    //$string = html_entity_decode($string, ENT_QUOTES, enc_charset);
                $string = htmlspecialchars_decode($string, ENT_QUOTES);
            } else {
                //$string = html_entity_decode($string, ENT_NOQUOTES, enc_charset);
                $string = htmlspecialchars_decode($string, ENT_NOQUOTES);
            }
			return $string;
		}

		/**
		 * Convert special characters to HTML entities
		 * @param string $string
		 * @return string
		 */
		static function specialcharString($string = ''){
            $string = htmlspecialchars($string);

			return $string;
		}

        public function RandString($length){
           $endString = "";
           $a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
           while(strlen($endString)<$length){
               $endString .= substr($a, rand(0,strlen($a)), 1);
           }
           return $endString;
        }

        // generates password
        public function GeneratePWD($length,$num){
            $letters = array('1','2','3','4','5','6','7','8','9',
                             'A','B','C','D','E','F','G','H','I','J','K','M',
                             'N','P','Q','R','S','T','U','V','W','X','Y','Z',
                             'a','b','c','d','e','f','g','h','i','j','k','m',
                             'n','p','q','r','s','t','u','v','w','x','y','z');
            srand((double) microtime() * 1000000);
            $j = 1;
            $test = "ok";
            while($j<=$num){
                $pos[$j] = rand(0,$length-1);
                for($m=1;$m<=$j;$m++){
                    if($pos[$j]!=$pos[$m-1]){
                       $test = "ok";
                    } else {
                       $test = "no";
                    }
                }
                if($test == "ok"){
                    $j++;
                }
            }
            for ($c = 0; $c < $length; $c++){
                $test = "ok";
                for($k=1;$k<$j;$k++){
                    if($c==$pos[$k]){
                       $test = "no";
                    }
                }
                if($test=="ok"){
                    $password .= $letters[rand(9,count($letters)-1)];
                } else {
                    $password .= $letters[rand(0,8)];
                }
            }
            return trim($password);
        }

    }
?>