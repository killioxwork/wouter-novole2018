<?php
    class mailingclass{

        public function mailsending($datatime,$mailInfo,$uInfo,$fullName,$replacements,$attachment,$mailTemplate){

        	/* create new mail class */
        	$mail = new PHPMailer();
      		$mail->Host = 'localhost';
      		$mail->SMTPAuth = false;
      		$mail->IsSMTP();
      		$mail->Port = 25;
      		$mail->WordWrap = 80;
      		$mail->IsHTML(true);
        	$mail->SetFrom(website_from_email_address, website_from_name);
        	$mail->AddReplyTo(website_from_email_address);

        	//$mail->DKIM_domain = 'sso.nl';
        	//$mail->DKIM_private = '../../includes/rsa.private_learn';
        	//$mail->DKIM_selector = 'invite';
        	//$mail->DKIM_passphrase = '';
        	$mail->AddCustomHeader('Precedence', 'bulk');

        	$mailFeedback['mailissent'] = 1;
        	$mailFeedback['error'] = 0;
        	$mailFeedback['message'] = '';
        	$mailFeedback['datatime'] = $datatime;

        	$temp_mail_prefix = '';
        	$domain = '';

        	$message_client = file_get_contents($mailTemplate);
    	    $message_client = str_replace("[[mailtext]]", db::decodeString($mailInfo['pageText']), $message_client);

    	    $subject_client = db::decodeString($mailInfo['pageSubject']);

        	try {

        		  list($temp_mail_prefix, $domain) = explode('@', $uInfo['email']);

        		  if(!filter_var($uInfo['email'], FILTER_VALIDATE_EMAIL) || !checkdnsrr($domain)){
        			  throw new phpmailerException('e-mail address or domain is invalid');
        		  } else {
        			  $mail->ClearAllRecipients();
        			  $mail->AddAddress(strtolower($uInfo['email']), $fullName);

        			  if (isset ($attachment) && $attachment != "" && count ($attachment) >= 1){
        				  for ($i=0; $i<count ($attachment); $i++){
        					  $mail->AddAttachment($attachment[$i]);
        				  }
        			  }

        			  $recipientSpecificTemplate = strtr($message_client, $replacements);
        			  $subject_client = strtr($subject_client, $replacements);

        			  $mail->Subject = $subject_client;
        			  $mail->MsgHTML($recipientSpecificTemplate, website_url);
        			  $result = $mail->Send();

        			  if(!$result){
        				   throw new phpmailerException($mail->ErrorInfo);
        			  }
        		  }

        	} catch (phpmailerException $e){

        		  $mailFeedback['mailissent'] = 0;
        		  $mailFeedback['error'] = 1;
        		  $mailFeedback['message'] = $e->errorMessage();
        		  $mailFeedback['datatime'] = '0000-00-00 00:00:00';
        	}

        	return $mailFeedback;
        }


        public function adminsending(){
            // list of users that will recive this mail
            $mailList[0]['id'] = 0;
            $mailList[0]['mail'] = website_admin_email_address;
            $mailList[0]['name'] = website_admin_name;
            $mailList[0]['role'] = 'CMS admin';
            // admins selected for bcc
            $i = 1;
            $admailQ = db::$mysqli->query("SELECT * FROM admin_accounts WHERE adminOnline='1'
                                                                          AND adminLevel='1'");
            while ($admailInfo = $admailQ->fetch_assoc()) {

                $mailList[$i]['id'] = $admailInfo['adminID'];
                $mailList[$i]['mail'] = $admailInfo['userName'];
                $mailList[$i]['name'] = db::decodeString($admailInfo['adminName']);
                $mailList[$i]['role'] = 'BCC admin';
                $i++;
            }
            return $mailList;
        }

    }
?>