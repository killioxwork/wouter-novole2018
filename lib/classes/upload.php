<?php
     class upload_class{
          /**
           * Unaccent the input string string. An example string like `ÀØėÿᾜὨζὅБю`
           * will be translated to `AOeyIOzoBY`. More complete than :
           *   strtr( (string)$str,
           *          "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
           *          "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn" );
           *
           * @param $str input string
           * @param $utf8 if null, function will detect input string encoding
           * @author http://www.evaisse.net/2008/php-translit-remove-accent-unaccent-21001
           * @return string input string without accent
           */
          public function remove_accents( $str, $utf8=true ){
              $str = (string)$str;
              $str = preg_replace('#/#', '', $str);
              $str = preg_replace('#&#', '', $str);
              //$str = preg_replace('#-#', '_', $str);
			  $str = preg_replace("#'#", '', $str);
			  $str = preg_replace('#"#', '', $str);
              $str = preg_replace('/\s+/', ' ', $str);
              if( is_null($utf8) ) {
                  if( !function_exists('mb_detect_encoding') ) {
                      $utf8 = (strtolower( mb_detect_encoding($str) )=='utf-8');
                  } else {
                      $length = strlen($str);
                      $utf8 = true;
                      for ($i=0; $i < $length; $i++) {
                          $c = ord($str[$i]);
                          if ($c < 0x80) $n = 0; # 0bbbbbbb
                          elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                          elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                          elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                          elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                          elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                          else return false; # Does not match any model
                          for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                              if ((++$i == $length)
                                  || ((ord($str[$i]) & 0xC0) != 0x80)) {
                                  $utf8 = false;
                                  break;
                              }

                          }
                      }
                  }

              }

              if(!$utf8)
                  $str = utf8_encode($str);

              $transliteration = array(
              'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
              'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
              'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
              'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
              'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
              'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
              'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
              'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
              'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
              'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
              'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
              'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
              'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
              'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
              'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
              'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
              'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
              'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
              'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
              'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
              'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
              'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
              'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
              'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
              'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
              'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
              'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
              'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
              'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
              'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
              'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
              'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
              'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
              'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
              'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
              'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
              'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
              'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
              'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
              'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
              'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
              'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
              'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
              'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
              'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
              'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
              'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
              'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
              'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
              'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
              'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
              'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
              'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
              'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
              'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
              'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
              'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
              'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
              'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
              'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
              'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
              'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
              'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
              'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
              'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
              'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
              'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
              'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
              'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
              'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
              'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
              'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
              'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
              'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
              'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
              'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
              'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
              'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
              'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
              'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
              'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
              'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
              'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
              'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
              'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
              'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
              'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
              'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
              'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
              'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
              'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
              'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
              'ჯ' => 'j','ჰ' => 'h','ʼ' => '', '̧' => '', 'ḩ' => 'h','ʼ' => '',
              '‘' => '', '’' => '', 'ừ' => 'u','ế' => 'e','ả' => 'a','ị' => 'i', 'ậ' => 'a',
              'ệ' => 'e','ỉ' => 'i','ộ' => 'o','ồ' => 'o','ề' => 'e','ơ' => 'o', 'ạ' => 'a',
              'ẵ' => 'a','ư' => 'u','ắ' => 'a','ằ' => 'a','ầ' => 'a','ḑ' => 'd', 'Ḩ' => 'H',
              'Ḑ' => 'D','ḑ' => 'd','ş' => 's','ā' => 'a','ţ' => 't', ',' => '', ' ' => '_',
              'é'=> 'e','/́'=> ''
              );    // ' ' => '_',
              $str = str_replace( array_keys( $transliteration ),
                                  array_values( $transliteration ),
                                  $str);
              return $str;
          }
          //- remove_accents()

          /**
           * Function for creating jpg image from source with given coordinates and width & height
           */
          public function create_with_coordinates_jpg($sourceimage, $destimage, $x, $y, $width, $height){
              //in case of problematic not round values for x, y, width, height
              $x = round($x);
              $y = round($y);
              $width = round($width);
              $height = round($height);
              $src = imagecreatefromjpeg($sourceimage);
              $im = imagecreatetruecolor($width,$height);
              imagecopyresampled($im,$src,0,0,$x,$y,$width,$height,$width,$height);
              imagejpeg($im,$destimage,95);
          }

          /**
           * Function for creating image from source with given coordinates and width & height
           */
          public function create_with_coordinates($sourceimage, $destimage, $x, $y, $width, $height){
              //in case of problematic not round values for x, y, width, height
              $x = round($x);
              $y = round($y);
              $width = round($width);
              $height = round($height);
              // added 26.05.2015 - format check
              $format = strtolower(substr(strrchr($sourceimage,"."),1));
              switch($format){
                case 'bmp':
                    $src = imagecreatefromwbmp($sourceimage);
                    break;
                case 'gif':
                    $src = imagecreatefromgif($sourceimage);
                    break;
                case 'jpg':
                case 'jpeg':
                    $src = imagecreatefromjpeg($sourceimage);
                    break;
                case 'png':
                    $src = imagecreatefrompng($sourceimage);
                    break;
                default :
                    return "Unsupported picture type!";
              }
              $im = imagecreatetruecolor($width,$height);
              // preserve transparency
              if($format == "gif" or $format == "png"){
                  imagealphablending($im, false);
                  imagesavealpha($im, true);
                  imagecolortransparent($im, imagecolorallocatealpha($im, 0, 0, 0, 127));
              }

              imagecopyresampled($im,$src,0,0,$x,$y,$width,$height,$width,$height);

              switch($format){
                case 'bmp':
                    imagewbmp($im,$destimage);
                    break;
                case 'gif':
                    imagegif($im,$destimage);
                    break;
                case 'jpg':
                case 'jpeg':
                    imagejpeg($im,$destimage,95);
                    break;
                case 'png':
                    imagepng($im,$destimage,9);
                    break;
              }
          }
          /**
           * Function for checking if image are right size min
           */
          public function upload_image_size($image, $minimumsize='', $imgfit='only-one'){
              if($minimumsize!=''){
                  list($width, $height) = explode("x", $minimumsize);
                  list($tmp_width, $tmp_height) = getimagesize($image);
                  if($imgfit=='only-one'){
                      if(($tmp_width>=$width and $width!='') or ($tmp_height>=$height and $height!='') ){
                           return true;
                      } else {
                           return false;
                      }
                  } else {
                      if(($tmp_width>=$width and $width!='') and ($tmp_height>=$height and $height!='') ){
                           return true;
                      } else {
                           return false;
                      }
                  }
              } else {
                  return true;
              }
          }
          /**
           * Function for checking if image are right size min
           */
          public function check_image_dimension($image, $minimumsize='', $imgfit='only-one'){
              if($minimumsize!=''){
                  list($width, $height) = explode("x", $minimumsize);
                  list($tmp_width, $tmp_height) = getimagesize($image);
                  if($imgfit=='only-one'){
                      if(($tmp_width>=$width and $width!='') or ($tmp_height>=$height and $height!='') ){
                           return true;
                      } else {
                           return false;
                      }
                  } elseif($imgfit=='rightsize'){
                      if(($tmp_width==$width and $width!='') and ($tmp_height==$height and $height!='') ){
                           return true;
                      } else {
                           return false;
                      }
                  } else {
                      if(($tmp_width>=$width and $width!='') and ($tmp_height>=$height and $height!='') ){
                           return true;
                      } else {
                           return false;
                      }
                  }
              } else {
                  return true;
              }
          }

          /**
           * Function for checking if image are right size - max
           */
          public function upload_image_maxsize($image, $location, $maximumsize=''){
              if($maximumsize!=''){
                  list($width, $height) = explode("x", $maximumsize);
                  list($tmp_width, $tmp_height) = getimagesize(site_files_depth.$location."/".$image);
                  if(($tmp_width>$width and $width!='') or ($tmp_height>$height and $height!='') ){
                       return true;
                  } else {
                       return false;
                  }
              } else {
                  return true;
              }
          }
          /**
           * Function for uploading files
           */
          public function upload_image($fileArray, $location, $changeName='', $minimumsize='', $imgfit=''){
             // checking the upload
             if($fileArray['name']!=""){
                 $uploaddir = site_files_depth.$location."/";
                 $fileList = explode(".", $fileArray['name']);
                 if($changeName!=''){
                    $filename = mb_strtolower($this->remove_accents(trim($changeName)),enc_charset).".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                 } else {
                    $filename = mb_strtolower($this->remove_accents($fileArray['name']),enc_charset);
                 }
                 $uploadfile = $uploaddir . $filename;
                 if(is_uploaded_file($fileArray["tmp_name"])){
                    if(@file_exists($uploadfile)){
                       return '-1';
                    }
                    if(@$this->upload_image_size($fileArray["tmp_name"],$minimumsize,$imgfit)){
                        if(move_uploaded_file($fileArray['tmp_name'], $uploadfile)){
            	           chmod($uploadfile, 0755);
                           if(@file_exists($uploadfile)){
                             // all ok
                             return $filename;
                           } else {
                             $filename = "";
                             return $filename;
                           }
                        }
                    } else {
                        return '-2';
                    }
                 }
             }
          }
          /**
           * Function for uploading files
           */
          public function upload_file($fileArray, $location, $changeName='', $chartype='lower', $uploadtype='change'){
             // checking the upload
             if($fileArray['name']!=""){
                 $uploaddir = site_files_depth.$location."/";
                 $fileList = explode(".", $fileArray['name']);
                 if($changeName!=''){
                    if($uploadtype=='change'){
                        if($chartype=='lower'){
                            $filename = mb_strtolower($this->remove_accents(trim($changeName)),enc_charset).".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                        } else {
                            $filename = $this->remove_accents(trim($changeName)).".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                        }
                    } else {
                        if($chartype=='lower'){
                            $filename = mb_strtolower($this->remove_accents(trim($changeName)),enc_charset).mb_strtolower($this->remove_accents($fileArray['name']),enc_charset);
                        } else {
                            $filename = $this->remove_accents(trim($changeName)).$this->remove_accents($fileArray['name']);
                        }
                    }
                 } else {
                    if($chartype=='lower'){
                        $filename = mb_strtolower($this->remove_accents($fileArray['name']),enc_charset);
                    } else {
                        $filename = $this->remove_accents($fileArray['name']);
                    }
                 }
                 $uploadfile = $uploaddir . $filename;
                 if(is_uploaded_file($fileArray["tmp_name"])){
                    if(@file_exists($uploadfile)){
                       return '-1';
                    }
                    if(move_uploaded_file($fileArray['tmp_name'], $uploadfile)){
        	           chmod($uploadfile, 0755);
                       if(@file_exists($uploadfile)){
                         // all ok
                         return $filename;
                       } else {
                         $filename = "";
                         return $filename;
                       }
                    }
                 }
             }
          }
          /**
           * Function for uploading files
           */
          public function upload_file_rename($fileArray, $location, $changeName='', $chartype='lower'){
             // checking the upload
             if($fileArray['name']!=""){
                 $uploaddir = site_files_depth.$location."/";
                 $fileList = explode(".", $fileArray['name']);
                 if($changeName!=''){
                    if($chartype=='lower'){
                        $filename = mb_strtolower($this->remove_accents(trim($changeName)),enc_charset).".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                    } else {
                        $filename = $this->remove_accents(trim($changeName)).".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                    }
                 } else {
                    if($chartype=='lower'){
                        $filename = mb_strtolower($this->remove_accents($fileArray['name']),enc_charset);
                    } else {
                        $filename = $this->remove_accents($fileArray['name']);
                    }
                 }
                 $uploadfile = $uploaddir . $filename;
                 if(is_uploaded_file($fileArray["tmp_name"])){
  	                $i=1;
  			        $test="OFF";
  	                while($test=="OFF"){
                        if(@file_exists($uploadfile)){
                            //return '-1';
                            if($changeName!=''){
                                if($chartype=='lower'){
                                    $filename = mb_strtolower($this->remove_accents(trim($changeName)),enc_charset)."_".$i.".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                                } else {
                                    $filename = $this->remove_accents(trim($changeName))."_".$i.".".mb_strtolower($fileList[count($fileList)-1],enc_charset);
                                }
                            } else {
                                if($chartype=='lower'){
                                    $filename = $i."_".mb_strtolower($this->remove_accents($fileArray['name']),enc_charset);
                                } else {
                                    $filename = $i."_".$this->remove_accents($fileArray['name']);
                                }
                            }
                            $uploadfile = $uploaddir . $filename;
                        }  else {
  	                        $test="ON";
                        }
  	                }
                    if(move_uploaded_file($fileArray['tmp_name'], $uploadfile)){
        	           chmod($uploadfile, 0755);
                       if(@file_exists($uploadfile)){
                         // all ok
                         return $filename;
                       } else {
                         $filename = "";
                         return $filename;
                       }
                    }
                 }
             }
          }
          /**
           * Function for delete files
           */
          public function delete_file($oldFile, $location, $oldCheck){
             //old image
             if($oldCheck=="true"){
                 $dimg_name=site_files_depth.$location."/".$oldFile;
                 if(@file_exists($dimg_name) and $oldFile!="") {
                    unlink($dimg_name);
                 }
             }
          }

        /* creates a compressed zip file */
        // $distill_subdirectories = do you want folders left or not...
        public function create_zip($files = array(),$destination = '',$overwrite = false, $distill_subdirectories = true) {    // array $files = NULL,
        	//if the zip file already exists and overwrite is false, return false
        	if(file_exists($destination) && !$overwrite) { return false; }
        	//vars
        	$valid_files = array();
        	//if files were passed in...
        	if(is_array($files)) {
        		//cycle through each file
        		foreach($files as $file) {
        			//make sure the file exists
        			if(file_exists($file)) {
        				$valid_files[] = $file;
        			}
        		}
        	}
        	//if we have good files...
        	if(count($valid_files)) {
        		//create the archive
        		$zip = new ZipArchive();
        		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
        			return false;
        		}
                //add the files
                foreach($valid_files as $file) {
                    if ($distill_subdirectories) {
                        $zip->addFile($file, basename($file) );
                    } else {
                        $zip->addFile($file, $file);
                    }
                }
        		//debug
        		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        		//close the zip -- done!
        		$zip->close();

        		//check to make sure the file exists
        		return file_exists($destination);
        	} else {
        		return false;
        	}
        }
		/**
		 * checks if url/file exist
		 * @param string $string - url
		 * @return true/false
		 */
        static function checkRemoteFile($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            // don't download content
            curl_setopt($ch, CURLOPT_NOBODY, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if(curl_exec($ch)!==FALSE){
                return true;
            } else {
                return false;
            }
        }        

     }
?>