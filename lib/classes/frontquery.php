<?php
     class front_class{

        public function getShortlang($lang='1') {
            $shortLang = 'en';

            $aQ = db::$mysqli->query("SELECT * FROM languages WHERE pageOnline='1'
                                                                AND pageID='".$lang."'");
            if($aQ->num_rows>0) {
                $aInfo = $aQ->fetch_assoc();

                $shortLang = $aInfo['shortLang'];
            }

            return $shortLang;
        }

        public function getLanguages($lang, $pID='') {
           #################################################################################################
            $pageInfo = array();

            $i = 0;

            $aQ = db::$mysqli->query("SELECT * FROM languages WHERE pageOnline='1' ORDER BY seqID");
            while ($aInfo = $aQ->fetch_assoc()) {
                $pageInfo[$i]['id'] = $aInfo['pageID'];
                $pageInfo[$i]['short'] = mb_strtoupper($aInfo['shortLang'],enc_charset);
                $pageInfo[$i]['long'] = db::decodeString($aInfo['longLang']);

                $pInfo = front_class::getPageinfo($aInfo['pageID'], $pID);

                if(!empty($pInfo)){
                    $pageInfo[$i]['link'] = $pInfo['link'];
                } else {
                    $pageInfo[$i]['link'] = website_url.$aInfo['shortLang'].'/';
                }


                if($aInfo['pageID']==$lang){
                    $pageInfo[$i]['sel'] = 'yes';
                }

                $i++;
            }

            return $pageInfo;
           #################################################################################################
        }

        public function getMenu($lang, $menuID='0') {
           #################################################################################################
            $pageInfo = array();

            $i = 0;

            $menuQ = db::$mysqli->query("SELECT pages.pageID as pID, pageMenu, pageType, pageMenutext, pageMenucolor
                                                                     FROM pages
                                                               INNER JOIN pages_lang
                                                                       ON pages.pageID = pages_lang.pageID
                                                                          AND pages_lang.langID='".$lang."'
                                                                          AND pages_lang.pageOnline='1'
                                                                    WHERE pageLabel='main'
                                                                      AND catID='0' ORDER BY seqID");

            while ($menuInfo = $menuQ->fetch_assoc()) {

                $pageInfo[$i]['id'] = $menuInfo['pID'];
                $pageInfo[$i]['title'] = db::decodeString($menuInfo['pageMenu']);
                $pageInfo[$i]['subtitle'] = db::decodeString($menuInfo['pageMenutext']);
                $pageInfo[$i]['style'] = $menuInfo['pageMenucolor'];

                switch ($menuInfo['pageType']) {
                    case 'pages':
                        $pageInfo[$i]['link'] = website_url.front_class::getShortlang($lang).'/'.$menuInfo['pID'].'/'.multifunc_class::fixUrlTitle(db::decodeString($menuInfo['pageMenu'])).'/';
                        break;
                }

                // find image
                $panImage = front_class::getPanoramic($menuInfo['pID'],'pages','single',3);
                $pageInfo[$i]['menuimage'] = $panImage[0]['bigimage'];

                if($menuInfo['pID']==$menuID){
                    $pageInfo[$i]['sel'] = 'yes';
                }

                $i++;
            }

            return $pageInfo;
           #################################################################################################

        }

        public function getPageinfo($lang, $pID) {
           #################################################################################################
            $pageInfo = array();

            $menuQ = db::$mysqli->query("SELECT pages.pageID as pID, pageMenu, pageMenucolor, pageTitle, pageText
                                                                     FROM pages
                                                               INNER JOIN pages_lang
                                                                       ON pages.pageID = pages_lang.pageID
                                                                          AND pages_lang.langID='".$lang."'
                                                                          AND pages_lang.pageOnline='1'
                                                                    WHERE pages.pageID='".$pID."'");

            if($menuQ->num_rows>0){
                $menuInfo = $menuQ->fetch_assoc();

                $pageInfo['id'] = $menuInfo['pID'];
                $pageInfo['menu'] = db::decodeString($menuInfo['pageMenu']);
                $pageInfo['style'] = $menuInfo['pageMenucolor'];
                $pageInfo['title'] = db::decodeString($menuInfo['pageTitle']);
                $pageInfo['text'] = db::decodeString($menuInfo['pageText']);
                $pageInfo['link'] = website_url.front_class::getShortlang($lang).'/'.$menuInfo['pID'].'/'.multifunc_class::fixUrlTitle(db::decodeString($menuInfo['pageMenu'])).'/';
            }

            return $pageInfo;
           #################################################################################################

        }

        public function getPanoramic($id,$label,$limit='multi',$column='3') {
           #################################################################################################
            $extraQ = "";
            if($limit=='single'){
                $extraQ .= " LIMIT 1";
            }

            $panInfo = array();

            $j = 0;

            $photoQ = db::$mysqli->query(sprintf("SELECT * FROM images WHERE pageLabel='".$label."'
                                                                         AND pageSection='panoramic'
                                                                         AND pageOnline='1'
                                                                         AND partID='%s' ORDER BY seqID".$extraQ,
                                                                                db::$mysqli->escape_string($id)));
            if($photoQ->num_rows>0){
                while ($photoInfo = $photoQ->fetch_assoc()) {

                    // there should be two images big/small
                    if(multifunc_class::checkImagelocation(front_files_depth."images/panoramic/column_1/", $photoInfo['pageImage'])){
                        $panInfo[$j]['bigimage'] = website_url."images/panoramic/column_".$column."/".$photoInfo['pageImage'];

                        $j++;
                    }
                }
            } else {
                if($column==2) $panInfo[0]['bigimage'] = default_size2;
                if($column==3) $panInfo[0]['bigimage'] = default_panoramic;
                if($column==4) $panInfo[0]['bigimage'] = default_size4;
            }

            return $panInfo;
           #################################################################################################

        }

        public function getPhoto($lang, $id, $label, $limit='multi') {
           #################################################################################################
            $extraLimit = "";
            if($limit=='single'){
                $extraLimit .= " LIMIT 1";
            }

            $phInfo = array();

            $j = 0;

            $photoQ = db::$mysqli->query(sprintf("SELECT pageImage, pageTitle
                                                                         FROM images
                                                                    LEFT JOIN images_lang
                                                                           ON images.pageID = images_lang.pageID
                                                                              AND images_lang.langID='".$lang."'
                                                                       WHERE pageLabel='".$label."'
                                                                         AND pageSection='photo'
                                                                         AND pageOnline='1'
                                                                         AND partID='%s' ORDER BY seqID".$extraLimit,
                                                                          db::$mysqli->escape_string($id)));
            if($photoQ->num_rows>0){
                while ($photoInfo = $photoQ->fetch_assoc()) {

                    $phInfo[$j]['title'] = db::decodeString($photoInfo['pageTitle']);

                    // there should be two images big/small
                    if(multifunc_class::checkImagelocation(front_files_depth."images/photo/column_2/", $photoInfo['pageImage'])){
                        $phInfo[$j]['smallimage'] = website_url."images/photo/column_2/".$photoInfo['pageImage'];
                        $phInfo[$j]['listimage'] = website_url."images/photo/column_3/".$photoInfo['pageImage'];
                        $phInfo[$j]['bigimage'] = website_url."images/photo/column_4/".$photoInfo['pageImage'];

                    }

                    $j++;
                }
            }

            return $phInfo;
           #################################################################################################
        }

        public function getDocument($lang, $id, $label, $iconsType) {
           #################################################################################################
            $docInfo = array();

            $j = 0;

            $docQ = db::$mysqli->query(sprintf("SELECT * FROM documents WHERE pageLabel='".$label."'
                                                                          AND langID='".$lang."'
                                                                          AND pageOnline='1'
                                                                          AND pageFile<>''
                                                                          AND partID='%s' ORDER BY seqID",
                                                                           db::$mysqli->escape_string($id)));
            if($docQ->num_rows>0){
                while ($dInfo = $docQ->fetch_assoc()) {

                    if(multifunc_class::checkImagelocation(front_files_depth."documents/", $dInfo['pageFile'])){

                        if($dInfo['pageTitle']!=''){
                            $docInfo[$j]['name'] = db::decodeString($dInfo['pageTitle']);
                        } else {
                            $docInfo[$j]['name'] = $dInfo['pageFile'];
                        }
                        $docInfo[$j]['size'] = multifunc_class::getfilesize($dInfo['pageSize']);
                        $docInfo[$j]['dlink'] = website_url.'download.php?t=d_c&c='.$dInfo['pageCode'];

                        $iconIfo = multifunc_class::showIcon($dInfo['pageFile'], $iconsType);

                        $docInfo[$j]['iconcode'] = $iconIfo['icon'];
                        $docInfo[$j]['icontext'] = $iconIfo['text'];

                        $j++;
                    }

                }
            }

            return $docInfo;
           #################################################################################################
        }

        public function getSearchTxt($search,$txtArray){

            if(stripos($txtArray['pText'],$search)!==false){

                $findFullText = trim(strip_tags(db::decodeString($txtArray['pText'])));
                $finalText = multifunc_class::partoftexthighlight($findFullText,db::decodeString($search));

            } elseif(stripos($txtArray['pTitle'],$search)!==false){

                $findFullText = trim(strip_tags(db::decodeString($txtArray['pTitle'])));
                $finalText = multifunc_class::partoftexthighlight($findFullText,db::decodeString($search));

            }

            return $finalText;

        }

        public function facebookGraph($title='',$link='',$image='',$desc=''){

            $graphInfo = array();
            // default values
            $graphInfo['title'] = graph_title;
            $graphInfo['link'] = graph_link;
            $graphInfo['image'] = graph_image;
            $graphInfo['imagew'] = 500;
            $graphInfo['imageh'] = 360;
            $graphInfo['desc'] = graph_desc;


            if($title!='') $graphInfo['title'] = $title;
            if($link!='') $graphInfo['link'] = $link;
            if($desc!='') $graphInfo['desc'] = strip_tags($desc);
            if($image!=''){
                $graphInfo['image'] = $image;
                $graphInfo['imagew'] = 400;
                $graphInfo['imageh'] = 400;
            }

            return $graphInfo;

        }

        public function createContact($lang, $name, $email, $phone, $country, $text) {
           #################################################################################################
            // check if this is mail
            if (preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $email)) {

                $datatime = date("Y-m-d H:i:s");
                $mailTemplate =  'inc_mail/contact_confirm.inc.php';

                $mailInfo['pageSubject'] = 'New contact added';

                $replacements = array(
                              "[[name]]"      => $name,
                              "[[mail]]"      => $email,
                              "[[phone]]"     => $phone,
                              "[[country]]"   => $country,
                              "[[text]]"      => $text
      		    );

                $uInfo['email'] = website_to_email_address_contact; //$email;
                $uInfo['name'] = website_to_name_contact; //$name;

                // if there is some general condition file to send by mail
                $attachment = array();

                $mailFeedback = mailingclass::mailsending($datatime,$mailInfo,$uInfo,$uInfo['name'],$replacements,$attachment,$mailTemplate);

                //enter data
                $query  = "INSERT INTO contacts SET pageDate='".date("Y-m-d H:i:s")."',
                                                    pageName='".db::encodeString($name)."',
                                                    pageMail='".$email."',
                                                    pagePhone='".$phone."',
                                                    pageCountry='".db::encodeString($country)."',
                                                    pageText='".db::encodeString($text)."',
                                                    langID='".$lang."',
                                                    mailSent='".$mailFeedback['mailissent']."',
                                                    error = ".$mailFeedback['error'].",
                                                    errormessage = '".$mailFeedback['message']."'";
                $q1= db::$mysqli->query($query);

                if($q1){
                    return 'created';
                } else {
                    return 'error';
                }
            } else {
                return 'not-mail';
            }
           #################################################################################################

        }

    }

?>