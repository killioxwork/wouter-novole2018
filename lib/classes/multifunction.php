<?php
     class multifunc_class{

        public function getfilesize($bytes) {
            if ($bytes >= 1099511627776) {
                 $return = round($bytes / 1024 / 1024 / 1024 / 1024, 2);
                 $suffix = "TB";
            } elseif ($bytes >= 1073741824) {
                 $return = round($bytes / 1024 / 1024 / 1024, 2);
                 $suffix = "GB";
            } elseif ($bytes >= 1048576) {
                 $return = round($bytes / 1024 / 1024, 2);
                 $suffix = "MB";
            } elseif ($bytes >= 1024) {
                 $return = round($bytes / 1024, 2);
                 $suffix = "KB";
            } else {
                 $return = $bytes;
                 $suffix = "B";
            }
            //if ($return == 1) {
                 $return .= " " . $suffix;
            //} else {
            //    $return .= " " . $suffix . "s";
            //}
            return $return;
        }
        public function getfilesizevalue($bytes) {

            $return = round($bytes / 1024 / 1024, 2);
            $suffix = "MB";

            $return .= " " . $suffix;

            return $return;
        }

        public function text_break($text='',$maxTextLenght='',$aspace=''){
            if(strlen(strip_tags($text)) > $maxTextLenght ) {
               $text = substr(trim($text),0,$maxTextLenght);
               $text = substr($text,0,strlen($text)-stripos(strrev($text),$aspace));
            }
            return $text;
        }

        public function highlightWords($string, $term){
            $term = preg_replace('/\s+/', ' ', trim($term));
            $words = explode(' ', $term);

            $highlighted = array();
            foreach ( $words as $word ){
                $highlighted[] = "<span class='highlight'>".$word."</span>";
            }

            return str_ireplace($words, $highlighted, $string);
        }

        public function partoftexthighlight($fulltext='', $findtext){
            $starttext = "‘....";
            $endtext = "....’";
            $pos = stripos($fulltext, $findtext);
            if($pos<20){ $pos = 0; } else { $pos = $pos - 20; }
            $text = substr($fulltext, $pos, 70);
            //$text = substr($text, 0, strrpos($text,' '));
            //$text = ltrim(strstr($text, ' '));
            $text = multifunc_class::highlightWords($text, $findtext);
            return $starttext.$text.$endtext;
        }

        public function textBreak($text='',$maxTextLenght='70',$aspace=' ',$endstring='...'){
            $text = strip_tags($text);
            if(strlen(strip_tags($text)) > $maxTextLenght ) {
                $text = substr(trim($text),0,$maxTextLenght);
                $text = substr($text,0,strlen($text)-strpos(strrev($text),$aspace));
                $text = trim($text).$endstring;
            }
            return $text;
        }
        public function textBreakV1($text, $length = 73, $dots = true) {
            $text = trim(preg_replace('#[\s\n\r\t]{2,}#', ' ', $text));
            $text_temp = $text;
            while (substr($text, $length, 1) != " ") { $length++; if ($length > strlen($text)) { break; } }
            $text = substr($text, 0, $length);
            return $text . ( ( $dots == true && $text != '' && strlen($text_temp) > $length ) ? '...' : '');
        }
        public function textBreakV2($string, $limit= 77, $break=" ", $pad="...") {
            // return with no change if string is shorter than $limit
            if(strlen($string) <= $limit) return $string;
            $string = substr($string, 0, $limit);
            if(false !== ($breakpoint = strrpos($string, $break))) {
               $string = substr($string, 0, $breakpoint);
            }
            return $string . $pad;
        }
        public function doURL($string){
            if($string!=''){
                if(substr($string,0,7)=='http://' or substr($string,0,8)=='https://'){
                    return $string;
                } else {
                    return 'http://'.$string;
                }
            }
        }
        public function stripBR($string){
            if($string=='<br />')	$string = str_replace('<br />','',$string);
            $string = str_replace('<br type="_moz" />','',$string);
            return $string;
        }
        public function cleanUrltitle($string){
            $charList = array('~','`','!','@','#','$','%','^','&','*','+','=',
                              '{','}','[',']',':',';','"','\'','<','>',',','.','?','/');
            $string = trim($string);
            for($i=0;$i<count($charList);$i++){
                $string = str_replace($charList[$i],'',$string);
            }
            $string = preg_replace('/\s+/', '_', $string);
            $string = strtolower($string);
            return $string;
        }
        // $pageQ - query sa strane, $numOnpage - max number of listin on page,
        // $page - page in listing you see, $exLink - link that need to be added
        public function inc_pages_old($howmany, $numOnpage, $page){
            $pageListblok = "";
            if ($howmany > 0) {
                if ($howmany > 0 and ($howmany/$numOnpage > 1)){
                    $pageListblok .= "<ul class=\"pagination\" role=\"navigation\" aria-label=\"Pagination\">";
                    $five = 5;
                  	if($page % 5 == 0){
                  	    $i = $b5 = $page-4;
                  	} else {
                  	    $i = $b5 = 1 + $page - ($page % 5);  // 1 +  first number divisible by 5, less than $page
                    }
                    // previous
                    if($page>1){
                        $pageListblok .= "<li class=\"pagination-previous\"><a href=\"#\" aria-label=\"previous page\" data-page=\"".($page-1)."\">Vorige <span class=\"show-for-sr\">page</span></a></li>";
                    } else {
                        $pageListblok .= "<li class=\"pagination-previous disabled\">Vorige <span class=\"show-for-sr\">page</span></li>";
                    }
                      // display left arrow  << 6 7 8
                    if($page > 5) {
                        $pageListblok .= "<li><a href=\"#\" aria-label=\"Page ".($b5-5)."\" data-page=\"".($b5-5)."\"> << </a></li>";
                    } else {
                        //  $pageListblok .= " << ";
                    }
                    while( ($i - 1) * $numOnpage < $howmany && $five) {
                        if($i == $page){
                             $pageListblok .= "<li class=\"current\"><span class=\"show-for-sr\">page selected</span> ".$i."</li>";
                        } else {
                             $pageListblok .= "<li><a href=\"#\" aria-label=\"Page ".$i."\" data-page=\"".$i."\">".$i."</a></li>";
                        }
                        $i++;
                        $five--;
                    } // while
                    if( ($i-1)*$numOnpage < $howmany ){
                        $pageListblok .= "<li><a href=\"#\" aria-label=\"Page ".$i."\" data-page=\"".$i."\"> >> </a></li>";
                    } else {
                        // $pageListblok .= " >> ";
                    }
                    // next
                    $numPages = (int)($howmany/$numOnpage);
                    if($howmany%$numOnpage > 0){
                        $numPages = $numPages + 1;
                    }
                    if($page<$numPages){
                        $pageListblok .= "<li class=\"pagination-next\"><a href=\"#\" aria-label=\"next page\" data-page=\"".($page+1)."\">Volgende <span class=\"show-for-sr\">page</span></a></li>";
                    } else {
                        $pageListblok .= "<li class=\"pagination-next disabled\">Volgende <span class=\"show-for-sr\">page</span></li>";
                    }
                    // echo (" page - $page     b5 - $b5  :  i - $i ");
                    // display right arrow  1 2 3 4 5 >>
                    $pageListblok .= "</ul>";
                }
            }
            return $pageListblok;
        }

        public function inc_pages($howmany, $numOnpage, $page){
            $pageListblok = "";
            if ($howmany > 0) {
                if ($howmany > 0 and ($howmany/$numOnpage > 1)){
                    $pageListblok .= "<ul class=\"pagination text-right\" role=\"navigation\" aria-label=\"Pagination\">";
                    $five = 5;
                  	if($page % 5 == 0){
                  	    $i = $b5 = $page-4;
                  	} else {
                  	    $i = $b5 = 1 + $page - ($page % 5);  // 1 +  first number divisible by 5, less than $page
                    }
                    // previous
                    if($page>1){
                        $pageListblok .= "<li class=\"pagination-previous\"><a href=\"#\" aria-label=\"previous page\" data-page=\"".($page-1)."\">Vorige <span class=\"show-for-sr\">page</span></a></li>";
                    } else {
                        $pageListblok .= "<li class=\"pagination-previous disabled\">Vorige <span class=\"show-for-sr\">page</span></li>";
                    }
                      // display left arrow  << 6 7 8
                    if($page > 5) {
                        $pageListblok .= "<li><a href=\"#\" aria-label=\"Page 1\" data-page=\"1\"> Eerste </a></li>";
                    } else {
                        //  $pageListblok .= " Eerste ";
                    }
                    while( ($i - 1) * $numOnpage < $howmany && $five) {
                        if($i == $page){
                             $pageListblok .= "<li class=\"current\"><span class=\"show-for-sr\">page selected</span> ".$i."</li>";
                        } else {
                             $pageListblok .= "<li><a href=\"#\" aria-label=\"Page ".$i."\" data-page=\"".$i."\">".$i."</a></li>";
                        }
                        $i++;
                        $five--;
                    } // while
                    // end page
                    $numPages = (int)($howmany/$numOnpage);
                    if($howmany%$numOnpage > 0){
                        $numPages = $numPages + 1;
                    }
                    if( ($i-1)*$numOnpage < $howmany ){
                        $pageListblok .= "<li><a href=\"#\" aria-label=\"Page ".$numPages."\" data-page=\"".$numPages."\"> Laatste </a></li>";
                    } else {
                        // $pageListblok .= " Laatste ";
                    }
                    // next
                    if($page<$numPages){
                        $pageListblok .= "<li class=\"pagination-next\"><a href=\"#\" aria-label=\"next page\" data-page=\"".($page+1)."\">Volgende <span class=\"show-for-sr\">page</span></a></li>";
                    } else {
                        $pageListblok .= "<li class=\"pagination-next disabled\">Volgende <span class=\"show-for-sr\">page</span></li>";
                    }
                    // echo (" page - $page     b5 - $b5  :  i - $i ");
                    // display right arrow  1 2 3 4 5 >>
                    $pageListblok .= "</ul>";
                }
            }
            return $pageListblok;
        }

        // multi select
        public function multiSelect($arrayValues){
            $dataM = "";
            if(count($arrayValues)!=0){
                for($i=0;$i<count($arrayValues);$i++){
               	    if($dataM==""){
               	       $dataM = $dataM.trim($arrayValues[$i]);
               	    }  else {
               	   	   $dataM = $dataM.",".trim($arrayValues[$i]);
               	    }
                }
            } else {
                $dataM = "";
            }
            return $dataM;
        }

        // date edit show
        public function datePicker($sDate='0000-00-00',$dateStatus='hide'){
          // to fix date
          if($sDate=="" or $sDate=="0000-00-00"){
              if($dateStatus!='hide'){
                  $sDate = date("d/m/Y");
              } else {
                  $sDate = "";
              }
          } else {
              list($year, $month, $day) = preg_split("/-/",$sDate);
              $sDate = $day . "/" . $month . "/" . $year;
          }
          return $sDate;
        }

        // date save
        public function datePickerSave($sDate){
          // to fix date
          if($sDate==""){
              $sDate = "0000-00-00";
          } else {
              list($day, $month, $year) = preg_split("#/#",$sDate);
              $sDate = $year . "-" . $month . "-" . $day;
          }
          return $sDate;
        }

        // show date front
        public function showDatefront($sDate,$separator,$year='year'){
            if($sDate!='' and $sDate!='0000-00-00'){
                if($year=='year'){
                    $sDate = substr($sDate,8,2).$separator.substr($sDate,5,2).$separator.substr($sDate,0,4);
                } else {
                    $sDate = substr($sDate,8,2).$separator.substr($sDate,5,2);
                }
            } else {
                $sDate = '';
            }
            return $sDate;
        }

        // show date front
        public function saveDate($sDate,$format='00-00-0000'){
            if($format='00-00-0000'){
                if($sDate!='' and $sDate!='00-00-0000'){
                    $sDate = substr($sDate,6,4).'-'.substr($sDate,3,2).'-'.substr($sDate,0,2);
                }
            }
            return $sDate;
        }

        // show date front
        public function showDate($sDate,$format='0000-00-00'){
            if($format='0000-00-00'){
                if($sDate!='' and $sDate!='0000-00-00'){
                    $sDate = substr($sDate,8,2).'-'.substr($sDate,5,2).'-'.substr($sDate,0,4);
                } else {
                    $sDate = '';
                }
            }
            return $sDate;
        }

        // show date front
        public function showDateMonthfront($sDate,$separator,$montharray,$year='year'){
            if($sDate!='' and $sDate!='0000-00-00'){
                if($year=='year'){
                    $sDate = (int)substr($sDate,8,2).$separator.$montharray[(int)substr($sDate,5,2)-1].$separator.substr($sDate,0,4);
                } else {
                    $sDate = (int)substr($sDate,8,2).$separator.$montharray[(int)substr($sDate,5,2)-1];
                }
            } else {
                $sDate = '';
            }
            return $sDate;
        }

        // show date front
        public function showDateTimefront($sDate,$separator,$dt_separator=' '){

          $sDate = substr($sDate,8,2).$separator.substr($sDate,5,2).$separator.substr($sDate,0,4).$dt_separator.substr($sDate,11,8);

          return $sDate;
        }

        // show date front
        public function showTimefront($sTime,$separator=':'){

            if($sTime!='' and $sTime!='00:00:00'){
                if($separator == ':'){
                    $sTime = substr($sTime,0,5);
                } else {
                    $sTime = preg_replace("/:/", $separator, substr($sTime,0,5));
                }
            } else {
                $sTime = '';
            }

            return $sTime;
        }

        // datetime edit show
        public function dateTimePicker($sDate='0000-00-00 00:00:00',$dateStatus='hide'){
          // to fix date
          if($sDate=="" or $sDate=="0000-00-00 00:00:00"){
              if($dateStatus!='hide'){
                  $sDate = date("d/m/Y hh:mm");
              } else {
                  $sDate = "";
              }
          } else {
              list($fieldDate, $fieldTime) = preg_split("/ /",$sDate);
              list($year, $month, $day) = preg_split("/-/",$fieldDate);
              $sDate = $day . "/" . $month . "/" . $year." ".substr($fieldTime,0,5);
          }
          return $sDate;
        }

        // date save
        public function dateTimePickerSave($sDate, $defaultDateTime='no'){
          // to fix date
          if($sDate==""){
              if($defaultDateTime!='no'){
                 $sDate = "0000-00-00 00:00:00";
              } else {
                 $sDate = "";
              }
          } else {
              list($fieldDate, $fieldTime) = preg_split("/ /",$sDate);
              if(strlen($fieldTime)==5){
                  $fieldTime = $fieldTime.":00";
              }

              list($day, $month, $year) = preg_split("#/#",$fieldDate);
              $sDate = $year . "-" . $month . "-" . $day." ".$fieldTime;
          }
          return $sDate;
        }

        // show date front
        public static function checkImagelocation($location, $image=''){
          $pageImage = '';

          if($image!="" and file_exists($location.$image)){
              $pageImage = $image;
          }

          return $pageImage;
        }

        // show day in week
        public function showDayinWeekLong($sDate, $language){

          $weekDay['en'] = array("sunday","monday","tuesday","wednessday","thursday","friday","saturday");
          $weekDay['nl'] = array("zondag","maandag","dinsdag","woensdag","donderdag","vrijdag","zaterdag");

          $selectedDay = date("w", mktime(0,0,0,substr($sDate,5,2),substr($sDate,8,2),substr($sDate,0,4)));
          $wDate = $weekDay[$language][$selectedDay];

          return $wDate;
        }

        // show day in week - short version
        public function showDayinWeek($sDate, $language){

          $weekDay['en'] = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
          $weekDay['de'] = array("So","Mo","Di","Mi","Do","Fr","Sa");

          $selectedDay = date("w", mktime(0,0,0,substr($sDate,5,2),substr($sDate,8,2),substr($sDate,0,4)));
          $wDate = $weekDay[$language][$selectedDay];

          return $wDate;
        }

        // show icon from  fontawesome
        public function showIcon($file, $iconarray){
            $fileparts = explode(".",$file);

            $iconInfo = array();

            if($iconarray[$fileparts[count($fileparts)-1]]!=''){
                $iconInfo['icon'] = $iconarray[$fileparts[count($fileparts)-1]];
                $iconInfo['text'] = strtoupper($fileparts[count($fileparts)-1]);
            } else {
                $iconInfo['icon'] = $iconarray['none'];
                $iconInfo['text'] = 'FILE';
            }

            return $iconInfo;
        }

        // show icon from  fontawesome
        public function removeExten($file){
            $fileparts = explode(".",$file);

            $patterns = "/.".$fileparts[count($fileparts)-1]."/";

            return preg_replace($patterns, '', $file);
        }

        // show icon from  fontawesome
        public function showFullname($firstname, $middlename, $lastname){
            $name = "";

            $name = $firstname." ";
            if($middlename!=""){
               $name .= $middlename." ";
            }
            $name = $name.$lastname;

            return $name;
        }

        // php redirect check on url
        public function checkRedirectURL_test($url) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, '60'); // in seconds
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_NOBODY, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);

            if(curl_getinfo($ch)['url'] == $url){
                $status = "not redirect";
            }else {
                $status = "redirect";
            }
        }

        // new course registration site
        public function course_reglink($workyear, $courseID){

            $replacements = array(
                              "[workyear]"          => $workyear,
                              "[courseid]" 	        => $courseID
      		                );

            $reglink = strtr(course_reg, $replacements);

            // check if page exist - if there is redirect
            if($this->checkRedirectURL($reglink)=='redirect'){
               $reglink = '';
            }

            return $reglink;
        }

        public function checkRedirectURL($oneurl) {
            //$urls = array(
            //    'http://www.apple.com/imac',
            //    'http://www.google.com/'
            //);

            $urls = array($oneurl);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            foreach($urls as $url) {
                curl_setopt($ch, CURLOPT_URL, $url);
                $out = curl_exec($ch);

                // line endings is the wonkiest piece of this whole thing
                $out = str_replace("\r", "", $out);

                // only look at the headers
                $headers_end = strpos($out, "\n\n");
                if( $headers_end !== false ) {
                    $out = substr($out, 0, $headers_end);
                }

                $headers = explode("\n", $out);
                foreach($headers as $header) {
                    if( substr($header, 0, 10) == "Location: " ) {
                        $target = substr($header, 10);

                        //echo "[$url] redirects to [$target]<br>";
                        //continue 2;

                        return 'redirect';
                    }
                }

                //echo "[$url] does not redirect<br>";

                return "[$url] does not redirect<br>";
            }
        }

        public function checkRedirectURL_v2($oneurl) {
            //$urls = array('http://www.terra.com.br/','http://www.areiaebrita.com.br/');
            $urls = array($oneurl);

            foreach ($urls as $url) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // chiborg suggestion
                curl_setopt($ch, CURLOPT_NOBODY, true);

                // ================================
                //  READ URL
                // ================================

                curl_setopt($ch, CURLOPT_URL, $url);
                $out = curl_exec($ch);

                // line endings is the wonkiest piece of this whole thing
                $out = str_replace("\r", "", $out);
                echo $out;

                $headers = explode("\n", $out);

                foreach($headers as $header) {
                    if(substr(strtolower($header), 0, 9) == "location:") {
                        // read URL to check if redirect to somepage on the server or another one.
                        // terra.com.br redirect to terra.com.br/portal. it is valid.
                        // but areiaebrita.com.br redirect to bwnet.com.br, and this is invalid.
                        // what we want is to check if the address continues being terra.com.br or changes. if changes, prints on page.

                        // if contains http, we will check if changes url or not.
                        // some servers, to redirect to a folder available on it, redirect only citting the folder. Example: net11.com.br redirect only to /heiden

                        // only execute if have http on location
                        if ( strpos(strtolower($header), "http") !== false) {
                            $address = explode("/", $header);

                            print_r($address);
                            // $address['0'] = http
                            // $address['1'] =
                            // $address['2'] = www.terra.com.br
                            // $address['3'] = portal

                            echo "url (address from array) = " . $url . "<br>";
                            echo "address[2] = " . $address['2'] . "<br><br>";

                            // url: terra.com.br
                            // address['2'] = www.terra.com.br

                            // check if string terra.com.br is still available in www.terra.com.br. It indicates that server did not redirect to some page away from here.
                            if(strpos(strtolower($address['2']), strtolower($url)) !== false) {
                                echo "URL NOT REDIRECT";
                            } else {
                                // not the same. (areiaebrita)
                                echo "SORRY, URL REDIRECT WAS FOUND: " . $url;
                            }
                        }
                    }
                }
            }
        }

          /**
           * Unaccent the input string string. An example string like `ÀØėÿᾜὨζὅБю`
           * will be translated to `AOeyIOzoBY`. More complete than :
           *   strtr( (string)$str,
           *          "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
           *          "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn" );
           *
           * @param $str input string
           * @param $utf8 if null, function will detect input string encoding
           * @author http://www.evaisse.net/2008/php-translit-remove-accent-unaccent-21001
           * @return string input string without accent
           */
          public function remove_accents_title( $str, $utf8=true ){
              $str = (string)$str;
              $str = preg_replace('#/#', '-', $str);
              $str = preg_replace('#&#', '-', $str);
              //$str = preg_replace('#-#', '_', $str);
			  $str = preg_replace("#'#", '', $str);
			  $str = preg_replace('#"#', '', $str);
              $str = preg_replace('#:#', '', $str);
              $str = preg_replace('/\s+/', ' ', $str);
              if( is_null($utf8) ) {
                  if( !function_exists('mb_detect_encoding') ) {
                      $utf8 = (strtolower( mb_detect_encoding($str) )=='utf-8');
                  } else {
                      $length = strlen($str);
                      $utf8 = true;
                      for ($i=0; $i < $length; $i++) {
                          $c = ord($str[$i]);
                          if ($c < 0x80) $n = 0; # 0bbbbbbb
                          elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                          elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                          elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                          elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                          elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                          else return false; # Does not match any model
                          for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                              if ((++$i == $length)
                                  || ((ord($str[$i]) & 0xC0) != 0x80)) {
                                  $utf8 = false;
                                  break;
                              }

                          }
                      }
                  }

              }

              if(!$utf8)
                  $str = utf8_encode($str);

              $transliteration = array(
              'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
              'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
              'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
              'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
              'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
              'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
              'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
              'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
              'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
              'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
              'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
              'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
              'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
              'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
              'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
              'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
              'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
              'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
              'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
              'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
              'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
              'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
              'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
              'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
              'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
              'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
              'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
              'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
              'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
              'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
              'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
              'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
              'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
              'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
              'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
              'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
              'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
              'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
              'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
              'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
              'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
              'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
              'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
              'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
              'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
              'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
              'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
              'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
              'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
              'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
              'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
              'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
              'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
              'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
              'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
              'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
              'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
              'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
              'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
              'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
              'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
              'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
              'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
              'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
              'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
              'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
              'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
              'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
              'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
              'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
              'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
              'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
              'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
              'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
              'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
              'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
              'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
              'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
              'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
              'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
              'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
              'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
              'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
              'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
              'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
              'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
              'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
              'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
              'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
              'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
              'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
              'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
              'ჯ' => 'j','ჰ' => 'h','ʼ' => '', '̧' => '', 'ḩ' => 'h','ʼ' => '',
              '‘' => '', '’' => '', 'ừ' => 'u','ế' => 'e','ả' => 'a','ị' => 'i', 'ậ' => 'a',
              'ệ' => 'e','ỉ' => 'i','ộ' => 'o','ồ' => 'o','ề' => 'e','ơ' => 'o', 'ạ' => 'a',
              'ẵ' => 'a','ư' => 'u','ắ' => 'a','ằ' => 'a','ầ' => 'a','ḑ' => 'd', 'Ḩ' => 'H',
              'Ḑ' => 'D','ḑ' => 'd','ş' => 's','ā' => 'a','ţ' => 't', ',' => '', ' ' => '_',
              'é'=> 'e','/́'=> ''
              );    // ' ' => '_',
              $str = str_replace( array_keys( $transliteration ),
                                  array_values( $transliteration ),
                                  $str);
              return $str;
          }
          //- remove_accents()

          /**
           * Unaccent the input string string. An example string like `ÀØėÿᾜὨζὅБю`
           * will be translated to `AOeyIOzoBY`. More complete than :
           *   strtr( (string)$str,
           *          "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
           *          "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn" );
           *
           * @param $str input string
           * @param $utf8 if null, function will detect input string encoding
           * @author http://www.evaisse.net/2008/php-translit-remove-accent-unaccent-21001
           * @return string input string without accent
           */
          public function remove_accents_xml( $str, $utf8=true ){
              $str = (string)$str;
              $str = preg_replace('#/#', ' ', $str);
              $str = preg_replace('#&#', ' ', $str);
              $str = preg_replace('#-#', ' ', $str);
			  $str = preg_replace("#'#", '', $str);
			  $str = preg_replace('#"#', '', $str);
              $str = preg_replace('#:#', '', $str);
              $str = preg_replace('/\s+/', ' ', $str);
              if( is_null($utf8) ) {
                  if( !function_exists('mb_detect_encoding') ) {
                      $utf8 = (strtolower( mb_detect_encoding($str) )=='utf-8');
                  } else {
                      $length = strlen($str);
                      $utf8 = true;
                      for ($i=0; $i < $length; $i++) {
                          $c = ord($str[$i]);
                          if ($c < 0x80) $n = 0; # 0bbbbbbb
                          elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                          elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                          elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                          elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                          elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                          else return false; # Does not match any model
                          for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                              if ((++$i == $length)
                                  || ((ord($str[$i]) & 0xC0) != 0x80)) {
                                  $utf8 = false;
                                  break;
                              }

                          }
                      }
                  }

              }

              if(!$utf8)
                  $str = utf8_encode($str);

              $transliteration = array(
              'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
              'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
              'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
              'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
              'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
              'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
              'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
              'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
              'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
              'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
              'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
              'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
              'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
              'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
              'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
              'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
              'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
              'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
              'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
              'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
              'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
              'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
              'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
              'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
              'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
              'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
              'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
              'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
              'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
              'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
              'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
              'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
              'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
              'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
              'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
              'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
              'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
              'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
              'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
              'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
              'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
              'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
              'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
              'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
              'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
              'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
              'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
              'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
              'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
              'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
              'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
              'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
              'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
              'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
              'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
              'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
              'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
              'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
              'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
              'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
              'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
              'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
              'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
              'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
              'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
              'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
              'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
              'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
              'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
              'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
              'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
              'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
              'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
              'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
              'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
              'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
              'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
              'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
              'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
              'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
              'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
              'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
              'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
              'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
              'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
              'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
              'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
              'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
              'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
              'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
              'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
              'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
              'ჯ' => 'j','ჰ' => 'h','ʼ' => '', '̧' => '', 'ḩ' => 'h','ʼ' => '',
              '‘' => '', '’' => '', 'ừ' => 'u','ế' => 'e','ả' => 'a','ị' => 'i', 'ậ' => 'a',
              'ệ' => 'e','ỉ' => 'i','ộ' => 'o','ồ' => 'o','ề' => 'e','ơ' => 'o', 'ạ' => 'a',
              'ẵ' => 'a','ư' => 'u','ắ' => 'a','ằ' => 'a','ầ' => 'a','ḑ' => 'd', 'Ḩ' => 'H',
              'Ḑ' => 'D','ḑ' => 'd','ş' => 's','ā' => 'a','ţ' => 't', ',' => '', 'é'=> 'e');
              $str = str_replace( array_keys( $transliteration ),
                                  array_values( $transliteration ),
                                  $str);
              return $str;
          }
          //- remove_accents()

        // show icon from  fontawesome
        public function fixUrlTitle($title){

            $title = multifunc_class::remove_accents_title(trim($title));

            return $title;
        }

        // show price in different format
        public function showPrice($price, $rateNum=1){
            if($price!=''){
                $price = substr_replace((int)(round($price*100)/$rateNum),",",-2,0);
                $price = preg_replace("/,00/", ",-", $price);
            }
            return $price;
        }

        // functions for special characters
        public function special($string){

        	$string=str_replace("%E2%80%99","’",$string);
        	$string=str_replace('%E2%80%93','-',$string);
        	$string=str_replace("%C2%B4","´",$string);
        	$string=str_replace("%B4","´",$string);
        	$string=str_replace("%22",'"',$string);
        	$string=str_replace("%60",'`',$string);

        	$string=str_replace("%5B",'[',$string);
        	$string=str_replace("%5D",']',$string);

        	$string=str_replace('%C3%8C','Ì',$string);
        	$string=str_replace('%C8','Ì',$string);
        	$string=str_replace('%C3%AC','ì',$string);
        	$string=str_replace('%EC','ì',$string);
        	$string=str_replace('%C3%92','Ò',$string);
        	$string=str_replace('%D2','Ò',$string);
        	$string=str_replace('%C3%B2','ò',$string);
        	$string=str_replace('%F2','ò',$string);
        	$string=str_replace('%C3%B9','ù',$string);
        	$string=str_replace('%F9','ù',$string);
        	$string=str_replace('%C3%99','Ù',$string);
        	$string=str_replace('%D9','Ù',$string);
        	$string=str_replace('%C3%A0','à',$string);
        	$string=str_replace('%E0','à',$string);
        	$string=str_replace('%C3%80','À',$string);
        	$string=str_replace('%C0','À',$string);
        	$string=str_replace('%C3%A8','è',$string);
        	$string=str_replace('%E8','è',$string);
        	$string=str_replace('%C8','È',$string);
        	$string=str_replace("%C3%88",'È',$string);
        	$string=str_replace('%C3%8A','Ê',$string);
        	$string=str_replace('%CA','Ê',$string);

        	$string=str_replace('%C3%AB','ë',$string);
        	$string=str_replace('%EB','ë',$string);
        	$string=str_replace('%C3%8B','Ë',$string);
        	$string=str_replace('%CB','Ë',$string);
        	$string=str_replace('%C3%A4','ä',$string);
        	$string=str_replace('%E4','ä',$string);
        	$string=str_replace('%C3%84','Ä',$string);
        	$string=str_replace('%C4','Ä',$string);
        	$string=str_replace('%C3%B6','ö',$string);
        	$string=str_replace('%F6','ö',$string);
        	$string=str_replace('%C3%96','Ö',$string);
        	$string=str_replace('%D6','Ö',$string);
        	$string=str_replace('%C3%AF','ï',$string);
        	$string=str_replace('%EF','ï',$string);
        	$string=str_replace('%C3%8F','Ï',$string);
        	$string=str_replace('%CF','Ï',$string);
        	$string=str_replace('%C3%BC','ü',$string);
        	$string=str_replace('%FC','ü',$string);
        	$string=str_replace('%C3%9C','Ü',$string);
        	$string=str_replace('%DC','Ü',$string);
        	$string=str_replace('%C3%BF','ÿ',$string);
        	$string=str_replace('%FF','ÿ',$string);

        	$string=str_replace('%C3%9F','ß',$string);
        	$string=str_replace('%DF','ß',$string);
        	$string=str_replace('%C3%A7','ç',$string);
        	$string=str_replace('%E7','ç',$string);

        	$string=str_replace('%C3%A9','é',$string);
        	$string=str_replace('%E9','é',$string);
        	$string=str_replace('%C3%89','É',$string);
        	$string=str_replace('%C9','É',$string);
        	$string=str_replace('%C3%A1','á',$string);
        	$string=str_replace('%E1','á',$string);
        	$string=str_replace('%C3%81','Á',$string);
        	$string=str_replace('%C1','Á',$string);
        	$string=str_replace('%C3%AD','í',$string);
        	$string=str_replace('%ED','í',$string);
        	$string=str_replace('%C3%8D','Í',$string);
        	$string=str_replace('%CD','Í',$string);
        	$string=str_replace('%C3%B3','ó',$string);
        	$string=str_replace('%F3','ó',$string);
        	$string=str_replace('%C3%93','Ó',$string);
        	$string=str_replace('%D3','Ó',$string);
        	$string=str_replace('%C3%BA','ú',$string);
        	$string=str_replace('%FA','ú',$string);
        	$string=str_replace('%C3%9A','Ú',$string);
        	$string=str_replace('%DA','Ú',$string);
        	$string=str_replace('%C3%BD','ý',$string);
        	$string=str_replace('%FD','ý',$string);
        	$string=str_replace('%C3%9D','Ý',$string);
        	$string=str_replace('%DD','Ý',$string);
        	return strtolower($string);
        }

    }
?>