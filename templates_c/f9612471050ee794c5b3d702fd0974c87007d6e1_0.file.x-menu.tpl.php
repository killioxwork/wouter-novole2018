<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:46:26
  from "W:\xampp\htdocs\wouter-novole2018\templates\x-menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b538d9270a2f7_37634388',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f9612471050ee794c5b3d702fd0974c87007d6e1' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\x-menu.tpl',
      1 => 1532202373,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b538d9270a2f7_37634388 (Smarty_Internal_Template $_smarty_tpl) {
?>
        <nav id="menu">
            <ul>
                <li><a class="mm-title close-mm"><i class="fa fa-close"></i> MENU</a></li>
                <li class="menu-lang">
                    <?php echo $_smarty_tpl->tpl_vars['lang']->value['languages'];?>

                    <span>
        <?php
$__section_lKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey'] : false;
$__section_lKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['lnInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lKey_0_total = $__section_lKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_lKey'] = new Smarty_Variable(array());
if ($__section_lKey_0_total != 0) {
for ($__section_lKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] = 0; $__section_lKey_0_iteration <= $__section_lKey_0_total; $__section_lKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']++){
$_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['last'] = ($__section_lKey_0_iteration == $__section_lKey_0_total);
?>
                        <span>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['lnInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['link'];?>
"<?php if ($_smarty_tpl->tpl_vars['lnInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['sel'] == 'yes') {?> class="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['lnInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['index'] : null)]['short'];?>
</a>
                        </span>
            <?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_section_lKey']->value['last'] : null)) {?>
                        <span>·</span>
            <?php }?>
        <?php
}
}
if ($__section_lKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lKey'] = $__section_lKey_0_saved;
}
?>
                    </span>
                </li>
                <li<?php if ($_smarty_tpl->tpl_vars['homesel']->value == 'yes') {?> class="mm-selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
">Home</a></li>
        <?php
$__section_mmKey_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_mmKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_mmKey'] : false;
$__section_mmKey_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['mmInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_mmKey_1_total = $__section_mmKey_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_mmKey'] = new Smarty_Variable(array());
if ($__section_mmKey_1_total != 0) {
for ($__section_mmKey_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index'] = 0; $__section_mmKey_1_iteration <= $__section_mmKey_1_total; $__section_mmKey_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index']++){
?>
                <li<?php if ($_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index'] : null)]['sel'] == 'yes') {?> class="mm-selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index'] : null)]['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mmKey']->value['index'] : null)]['title'];?>
</a></li>
        <?php
}
}
if ($__section_mmKey_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_mmKey'] = $__section_mmKey_1_saved;
}
?>
            </ul>
            <a href="<?php echo $_smarty_tpl->tpl_vars['contactInfo']->value['link'];?>
" class="btn booking onmenu">
                <i class="fa fa-calendar-check-o"></i>
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['reservation'];?>

            </a>
            <p class="menuTA"><?php echo $_smarty_tpl->tpl_vars['lang']->value['review'];?>
</p>
            <a href="<?php echo $_smarty_tpl->tpl_vars['tripadviser_link']->value;?>
" target="_blank" title="Trip Advisor" class="taOnMenu">
                <img src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
images/tripoAdviserMenu.png" alt="Trip Advisor" title="Trip Advisor">
            </a>
        </nav><?php }
}
