<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:04:02
  from "W:\xampp\htdocs\wouter-novole2018\templates\home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5383a2c8b790_65427197',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '033b701fc4092847dcda969b06900e488dcd975a' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\home.tpl',
      1 => 1532199839,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-head.tpl' => 1,
    'file:x-header.tpl' => 1,
    'file:x-booking.tpl' => 1,
    'file:x-footer.tpl' => 1,
    'file:x-menu.tpl' => 1,
    'file:x-cookie.tpl' => 1,
    'file:x-headend.tpl' => 1,
  ),
),false)) {
function content_5b5383a2c8b790_65427197 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:x-head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
<div id="page">
  <div id="wrapper">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <section class="row tagline">
        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['title'] != '') {?><h1><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h1><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pInfo']->value['text'] != '') {?><p class="home-intro"><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['text'];?>
</p><?php }?>
    </section>
<?php
$__section_mKey_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey'] : false;
$__section_mKey_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['mmInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_mKey_0_total = $__section_mKey_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_mKey'] = new Smarty_Variable(array());
if ($__section_mKey_0_total != 0) {
for ($__section_mKey_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] = 0; $__section_mKey_0_iteration <= $__section_mKey_0_total; $__section_mKey_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']++){
?>
    <section class="homeMenuWrap"> <a href="<?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['link'];?>
">
      <div class="row homeMenuTextWrap">
        <div class="textPosition<?php if ($_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['style'] == 'white') {?> colorWhite<?php }?>">
          <h2><?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['title'];?>
</h2>
          <p><?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['subtitle'];?>
</p>
        </div>
      </div>
      <img src="<?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['menuimage'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['mmInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_mKey']->value['index'] : null)]['title'];?>
" class="homeMenuImg"></a> </section>
<?php
}
}
if ($__section_mKey_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_mKey'] = $__section_mKey_0_saved;
}
?>
  </div>
<?php $_smarty_tpl->_subTemplateRender("file:x-booking.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  
  <!-- responsive menu -->
<?php $_smarty_tpl->_subTemplateRender("file:x-menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>
<?php $_smarty_tpl->_subTemplateRender("file:x-cookie.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</body>
<?php $_smarty_tpl->_subTemplateRender("file:x-headend.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
