<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:47:52
  from "W:\xampp\htdocs\wouter-novole2018\templates\x-documents.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b538de8787f64_98195232',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8062b36e33a809fd2e224c9eb90af844a0a6ada6' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\x-documents.tpl',
      1 => 1531480488,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b538de8787f64_98195232 (Smarty_Internal_Template $_smarty_tpl) {
$__section_dKey_2_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey'] : false;
$__section_dKey_2_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['docInfo']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_dKey_2_total = $__section_dKey_2_loop;
$_smarty_tpl->tpl_vars['__smarty_section_dKey'] = new Smarty_Variable(array());
if ($__section_dKey_2_total != 0) {
for ($__section_dKey_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] = 0; $__section_dKey_2_iteration <= $__section_dKey_2_total; $__section_dKey_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']++){
$_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['first'] = ($__section_dKey_2_iteration == 1);
$_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['last'] = ($__section_dKey_2_iteration == $__section_dKey_2_total);
?>
    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['first'] : null)) {?>
    <div class="tableWrap">
            <div class="row">
            	<div class="content">
            		<div class="tbl-wrap">
                	<div class="row">
                                <div class="columns medium-12">
                                    <h3>Downloads</h3>
                                    <table class="download-tbl download-style display responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                            <tr>
                                                <th data-priority="1"></th>
                                                <th class="text-left"></th>
                                                <th data-priority="2" class="text-right"></th>
                                            </tr>
                                    </thead>
                                    <tbody>
    <?php }?>
                                        <tr>
                                            <td class="c1"><a href="<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['dlink'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['name'];?>
</a></td>
                                            <td class="c2"><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['iconcode'];?>
"></i><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['icontext'];?>
</td>
                                            <td class="c3"><?php echo $_smarty_tpl->tpl_vars['docInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['index'] : null)]['size'];?>
</td>
                                        </tr>
    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_section_dKey']->value['last'] : null)) {?>
                                    </tbody>
                                </table>
                                </div>
                        </div>
                </div>
            	</div>
            </div>
    </div>
    <?php }
}
}
if ($__section_dKey_2_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_dKey'] = $__section_dKey_2_saved;
}
}
}
