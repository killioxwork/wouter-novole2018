<?php
/* Smarty version 3.1.30, created on 2018-07-21 22:07:15
  from "W:\xampp\htdocs\wouter-novole2018\templates\details.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5392738c3f93_02903065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '251f0c304e71d0ed0fa9465efbbe20009f461169' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\details.tpl',
      1 => 1532203633,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:x-head.tpl' => 1,
    'file:x-header.tpl' => 1,
    'file:x-panoramic.tpl' => 1,
    'file:x-photo-video.tpl' => 1,
    'file:x-contactform.tpl' => 1,
    'file:x-documents.tpl' => 1,
    'file:x-booking.tpl' => 1,
    'file:x-footer.tpl' => 1,
    'file:x-menu.tpl' => 1,
    'file:x-cookie.tpl' => 1,
    'file:x-headend.tpl' => 1,
  ),
),false)) {
function content_5b5392738c3f93_02903065 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:x-head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
<div id="page">
  <div id="wrapper" class="relative">
<?php $_smarty_tpl->_subTemplateRender("file:x-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    
    <!--Carousel-->
<?php $_smarty_tpl->_subTemplateRender("file:x-panoramic.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!--/Carousel-->
    
    <div class="row">
      <div class="content<?php if ($_smarty_tpl->tpl_vars['contactform']->value == 'yes') {?> contact<?php }?>">
        <h1><?php echo $_smarty_tpl->tpl_vars['pInfo']->value['title'];?>
</h1>
        <?php echo $_smarty_tpl->tpl_vars['pInfo']->value['text'];?>

<?php $_smarty_tpl->_subTemplateRender("file:x-photo-video.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


      </div>
    </div>
  </div>
<?php if ($_smarty_tpl->tpl_vars['contactform']->value == 'yes') {
$_smarty_tpl->_subTemplateRender("file:x-contactform.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>  
<?php $_smarty_tpl->_subTemplateRender("file:x-documents.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php if ($_smarty_tpl->tpl_vars['contactform']->value != 'yes') {
$_smarty_tpl->_subTemplateRender("file:x-booking.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
$_smarty_tpl->_subTemplateRender("file:x-footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  
  <!-- responsive menu -->
<?php $_smarty_tpl->_subTemplateRender("file:x-menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>
<?php $_smarty_tpl->_subTemplateRender("file:x-cookie.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</body>
<?php $_smarty_tpl->_subTemplateRender("file:x-headend.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
