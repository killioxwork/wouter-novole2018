<?php
/* Smarty version 3.1.30, created on 2018-07-21 22:05:21
  from "W:\xampp\htdocs\wouter-novole2018\templates\x-head.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b539201cb7999_63760028',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3c919c9e6c1215921606bb868f9ff4c804a6cdd' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\x-head.tpl',
      1 => 1532203388,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b539201cb7999_63760028 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="nl">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Novole</title>
<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
favicon.ico" type="image/x-icon">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<!--
/**
 * @license
 * MyFonts Webfont Build ID 3367388, 2017-03-31T09:29:18-0400
 *
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are
 * explicitly restricted from using the Licensed Webfonts(s).
 *
 * You may obtain a valid license at the URLs below.
 *
 * Webfont: AvenirLT-Roman by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/55-roman/
 * Licensed pageviews: 250,000
 *
 * Webfont: AvenirLT-Heavy by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/85-heavy/
 * Licensed pageviews: 500,000
 *
 * Webfont: AvenirLT-Oblique by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/55-oblique/
 * Licensed pageviews: 500,000
 *
 * Webfont: AvenirLT-HeavyOblique by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/85-heavy-oblique/
 * Licensed pageviews: 500,000
 *
 *
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=3367388
 * Webfonts copyright: Part of the digitally encoded machine readable outline data for producing the Typefaces provided is copyrighted &#x00A9; 1981 - 2007 Linotype GmbH, www.linotype.com. All rights reserved. This software is the property of Linotype GmbH, and may not be repro
 *
 * © 2017 MyFonts Inc
*/

-->
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
fonts/Avenir.css"/>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/grid.min.css">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/jquery.mmenu.all.css">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/font-awesome.min.css">
<?php if ($_smarty_tpl->tpl_vars['pageType']->value == 'details') {?>
<link href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
owl-carousel/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/dataTables.foundation.min.css">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
css/responsive.foundation.min.css">
<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/jquery-1.9.1.min.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['pageType']->value == 'details') {
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
owl-carousel/owl.carousel.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/foundation.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/jquery.mmenu.all.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['pageType']->value == 'details') {
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/plugins.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/dataTables.foundation.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/dataTables.responsive.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/responsive.foundation.min.js"><?php echo '</script'; ?>
>
<!--script src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/retina.min.js"><?php echo '</script'; ?>
-->
<!--script src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/remove-hover.min.js"><?php echo '</script'; ?>
-->
<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/jquery.cookieBar.min.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['contactform']->value == 'yes') {
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/vendor/validation/validate.min.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
js/main.js"><?php echo '</script'; ?>
>
</head><?php }
}
