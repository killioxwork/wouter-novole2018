<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:42:59
  from "W:\xampp\htdocs\wouter-novole2018\templates\x-footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b538cc3eb86d7_23835159',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2306d70d3bbc3942bf674a2610d26d76666680eb' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\x-footer.tpl',
      1 => 1532202174,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b538cc3eb86d7_23835159 (Smarty_Internal_Template $_smarty_tpl) {
?>
        <footer id="footer">
            <div class="row expanded">
                <div class="medium-8 large-6 columns">
                    <ul>
                        <li class="bold"><?php echo $_smarty_tpl->tpl_vars['lang']->value['review'];?>
</li>
                        <li class="imgWrap">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['tripadviser_link']->value;?>
" target="_blank" title="Trip Advisor">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
images/tripAdvisor.png" alt="Trip Advisor" title="Trip Advisor">
                            </a>
                        </li>
                        <li class="bold">
                            <p>Copyright © 2018
                                <br> <?php echo $_smarty_tpl->tpl_vars['lang']->value['footertitle'];?>

                                <br>
                            </p>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['privInfo']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['privInfo']->value['menu'];?>
</a>
                        </li>
                        <li>
                            <p>Fax: +39 0575 614190
                                <br> <?php echo $_smarty_tpl->tpl_vars['lang']->value['footermobile'];?>
: 388 6081730
                                <br> Email:
                                <a href="mailto:mail@novole.com">mail@novole.com</a>
                                <br>
                                <i class="fa fa-facebook-official"></i>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['facebook_link']->value;?>
" class="facebook">facebook</a>
                            </p>
                        </li>
                        <li>
                            <p>Montanare c.s. 93, 52040 Cortona (Arezzo)
                                <br> P.Iva IT 00902140516</p>
                        </li>
                        <li>
                            <p class="aboutWrap"><?php echo $_smarty_tpl->tpl_vars['lang']->value['footertext'];?>
</p>
                        </li>
                        <li>
                            <p>
                                Website:
                                <a href="http://www.deeldrie.nl/" target="_blank">deeldrie</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row expanded">
                <div class="medium-12 columns flWrap">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['siteLink']->value;?>
images/logoOlive.svg" alt="novole" class="footerLogo">
                </div>
            </div>
        </footer><?php }
}
