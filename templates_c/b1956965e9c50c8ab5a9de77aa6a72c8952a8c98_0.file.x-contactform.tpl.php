<?php
/* Smarty version 3.1.30, created on 2018-07-21 22:22:00
  from "W:\xampp\htdocs\wouter-novole2018\templates\x-contactform.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5395e8d52805_99600889',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b1956965e9c50c8ab5a9de77aa6a72c8952a8c98' => 
    array (
      0 => 'W:\\xampp\\htdocs\\wouter-novole2018\\templates\\x-contactform.tpl',
      1 => 1532204480,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5395e8d52805_99600889 (Smarty_Internal_Template $_smarty_tpl) {
?>
        <div id="contactFormSection">
            <div class="row">
                <form id="contactForm" class="contactForm" action="<?php echo $_smarty_tpl->tpl_vars['pInfo']->value['link'];?>
" method="POST" novalidate>
                    <input type="hidden" name="action" value="contact" />
                    <div class="row">
                        <div class="large-12 columns">
                            <label><?php echo $_smarty_tpl->tpl_vars['lang']->value['formname'];?>
 *
                                <input type="text" name="fullname" id="fullname" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label><?php echo $_smarty_tpl->tpl_vars['lang']->value['formmail'];?>
 *
                                <input type="text" name="email" id="email" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label><?php echo $_smarty_tpl->tpl_vars['lang']->value['formphone'];?>

                                <input type="text" name="phone" id="phone" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label><?php echo $_smarty_tpl->tpl_vars['lang']->value['formcountry'];?>

                                <input type="text" name="country" id="country" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label><?php echo $_smarty_tpl->tpl_vars['lang']->value['formmsg'];?>

                                <textarea name="text" id="text" /></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <button type="submit" class="btn btnSubmit">
                                <i class="fa fa-fighter-jet"></i> <?php echo $_smarty_tpl->tpl_vars['lang']->value['formbutton'];?>
</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="prive"><?php echo $_smarty_tpl->tpl_vars['lang']->value['formpolicy'];?>
</p>
                        </div>
                    </div>
                </form>
            </div>

        </div><?php }
}
