<?php
    include ("db_connect.php");

   // language
   #################################################################################################
    $aQ = db::$mysqli->query(sprintf("SELECT * FROM languages WHERE pageOnline='1'
                                                                AND shortLang='%s'",
                                                                 db::$mysqli->escape_string($_GET['ln'])));
    if($aQ->num_rows>0) {
        $aInfo = $aQ->fetch_assoc();

        $lang = $aInfo['pageID'];

    } else {
        $_GET['ln'] = 'en';
        $lang = 1;
    }

    $smarty->assign('selLang' , $_GET['ln']);

   #################################################################################################

   // solo page - footer
   #################################################################################################

    $privInfo = array();
    $privInfo = $frontfunc->getPageinfo($lang, 8);
    $smarty->assign('privInfo' , $privInfo);

    // link to privacy page
    $getInfo[$_GET['ln']]['policy'] = preg_replace('/policylink/', $privInfo['link'], $getInfo[$_GET['ln']]['policy']);

    $contactInfo = array();
    $contactInfo = $frontfunc->getPageinfo($lang, 7);
    $smarty->assign('contactInfo' , $contactInfo);

    $smarty->assign('lang' , $getInfo[$_GET['ln']]);

   #################################################################################################

   // find the page
   #################################################################################################
    if($_GET['pID']==''){

        $smarty->assign('homesel', 'yes');

        $pInfo = $frontfunc->getPageinfo($lang, 1);
        $smarty->assign('pInfo' , $pInfo);

        $template = "home.tpl";

    } elseif($_GET['pID']!=''){

        $pInfo = array();
        $pInfo = $frontfunc->getPageinfo($lang, $_GET['pID']);
        $smarty->assign('pInfo' , $pInfo);

        if($_GET['pID']==5 or $_GET['pID']==7){
            if($_POST['action']=='contact'){
                // send mail & enter in DB
                if($_POST['fullname']!="" and $_POST['email']!=""
                    and preg_match("/([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)/", $_POST['email'])){

                    $l = $frontfunc->createContact($lang,$_POST['fullname'],$_POST['email'],$_POST['phone'],$_POST['country'],$_POST['text']);
                    switch ($l) {
                        case 'created' :
                            //echo '';
                            break;
                        case 'error' :
                            //echo '';
                            break;
                        case 'not-mail' :
                            //echo '';
                            break;
                        default: // Some of values is incorect
                            //echo "";
                    }

                    $cInfo = $frontfunc->getPageinfo($lang, 10);
                    $smarty->assign('cInfo' , $cInfo);

                    $smarty->assign('contactResult' , $l);

                }
            }

            $smarty->assign('contactform' , 'yes');
        }

       // panoramic
       ###################################################################################
        $panInfo = array();

        $panInfo = $frontfunc->getPanoramic($_GET['pID'], 'pages', 'multi', '3');

        $smarty->assign('panInfo' , $panInfo);
       ###################################################################################

       // photo
       ###################################################################################
        $phInfo = array();

        $phInfo = $frontfunc->getPhoto($lang, $_GET['pID'], 'pages', 'multi');

        $smarty->assign('phInfo' , $phInfo);
       ###################################################################################

       // documents
       ###################################################################################
        $docInfo = array();

        $docInfo = $frontfunc->getDocument($lang, $_GET['pID'], 'pages', $iconsType);

        $smarty->assign('docInfo' , $docInfo);
       ###################################################################################

        $smarty->assign('pageType', 'details');

        $template = "details.tpl";
    }


   #################################################################################################

   // menu - end of page
   #################################################################################################
    // menu
    $mmInfo = array();

    $mmInfo = $frontfunc->getMenu($lang, $_GET['pID']);

    $smarty->assign('mmInfo' , $mmInfo);

    // languages
    $lnInfo = array();

    $lnInfo = $frontfunc->getLanguages($lang, $_GET['pID']);

    $smarty->assign('lnInfo' , $lnInfo);
   #################################################################################################

    $smarty->display($template);
?>