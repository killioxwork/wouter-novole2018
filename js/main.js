$(document).ready(function() {
  $(document).foundation();

  //MMenu
  $("#menu").mmenu({
    navbar: {
      title: ""
    },
    extensions: ["effect-slide-menu", "border-offset"],
    //keyboardNavigation 		: true,
    //screenReader 			: true,
    offCanvas: {
      position: "right",
      zposition: "front"
    }
  });
  var API = $("#menu").data("mmenu");

  $("a.close-mm").click(function() {
    API.close();
  });

  /*Owl Carusel*/
  var owlHeaderSlider = $("#header-slider");

  if (owlHeaderSlider.length > 0) {
    owlHeaderSlider.owlCarousel({
      items: 1,
      lazyLoad: true,
      loop: true,
      autoplay: true,
      autoplayTimeout: 4000,
      dots: false
    });
  }

	if ($('.download-tbl').length > 0) {
		$('.download-tbl').DataTable({
			autoWidth: false,
			paging:   false,
			ordering: false,
			info:     false,
			searching: false,
			responsive: true
		});
	}

  $(".cookie-message").cookieBar({ closeButton: ".my-close-button" });

  //Contact Form
    if ($('#contactForm').length > 0) {
      $("#contactForm").validate({
        focusInvalid: false, // do not focus the last invalid input
        rules: {
          fullname: "required",
          email: "required"
        },
        messages: {
          fullname: {
            required: "",
            fullname: ""
          },
          email: {
            required: "",
            email: ""
          }
        }
      });
    }
  $("#contactForm input").keypress(function(e) {
    if (e.which === 13) {
      if (
        $("#contactForm")
          .validate()
          .form()
      ) {
        $("#contactForm").submit();
      }
      return false;
    }
  });
});
