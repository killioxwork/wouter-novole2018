<p>
A request to update your Novole Manager Account was received today from ip-address [[ipvalue]].
</p>

<p>                       
Please click <a href="[[SITELINK]][[confirmlink]]">here</a> to update your account information. 
</p>

<p>
This link is valid for one hour, which means until [[datetime]].
</p>

<p>
If you did not made this request, you can just discard this message.
</p>

++++++++

<p>
We ontvingen zojuist het verzoek om uw Novole Manager account te updaten vanaf ip-adres [[ipvalue]].
</p>

<p>
Klik a.u.b. op deze link om uw account te updaten.
</p>

<p>
Deze link is exact 1 uur geldig, dat houdt in tot [[datetime]].
</p>

<p>
Als dit verzoek niet door u zelf is gedaan, dan kunt u dit bericht negeren.
</p>

