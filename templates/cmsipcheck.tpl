{* Smarty *}
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Novole</title>
	<link rel="shortcut icon" href="{$siteLink}favicon.ico">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{$siteLink}assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{$siteLink}assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/core/app.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/custom/ipcheck.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="{$siteLink}images/logoWhite.svg" alt="Novole"></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					<form action="index.php" class="form-validate" method="post" novalidate>
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object-dd"><img src="{$siteLink}images/logo.svg" alt="Novole"></div>
								<h5 class="content-group">
									Your ip-address is not allowed.
                                     <small class="display-block">As an existing user you can fill in your email addres in order to receive further instructions.</small>
                    {if $logError!=''}
                                    <span class="validation-error-label" style="font-size:14px; display:inline-block; margin:15px 0 0 0;">{$logError}</span>
                    {/if}
                                </h5>
							</div>
                    {if $formHide!='OK'}
							<div class="form-group has-feedback">
								<input type="email" class="form-control" id="uname" name="uname" placeholder="Your username">
								<div class="form-control-feedback">
									<i class="icon-mail5 text-muted"></i>
								</div>
							</div>
                            <input type="hidden" name="btn_login" value="log" />
							<button type="submit" class="btn bg-blue btn-block">Send <i class="icon-arrow-right14 position-right"></i></button>
                    {/if}
						</div>
					</form>
					<!-- /password recovery -->


					<!-- Footer -->
					<div class="footer text-muted text-center"> &copy; Copyright 2018 · <a href="http://www.deeldrie.nl" target="_blank">deeldrie</a> </div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
