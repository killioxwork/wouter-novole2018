        <footer id="footer">
            <div class="row expanded">
                <div class="medium-8 large-6 columns">
                    <ul>
                        <li class="bold">{$lang.review}</li>
                        <li class="imgWrap">
                            <a href="{$tripadviser_link}" target="_blank" title="Trip Advisor">
                                <img src="{$siteLink}images/tripAdvisor.png" alt="Trip Advisor" title="Trip Advisor">
                            </a>
                        </li>
                        <li class="bold">
                            <p> {$lang.footertitle}
                                <br>
                                Montanare c.s. 93, 52040 Cortona (Arezzo)
                                <br> P.Iva IT 00902140516
                            </p>
                        </li>
                        <li class="bold">
                            <p>
                                Tel/Fax: +39 0575 614190
                                <br> {$lang.footermobile}: 388 6081730
                                <br> Email:
                                <a href="mailto:mail@novole.com">mail@novole.com</a>
                                <br>
                                <i class="fa fa-facebook-official"></i>
                                <a href="{$facebook_link}" class="facebook" target="_blank">facebook</a>
                            </p>
                        </li>
                        <li>
                            <p class="aboutWrap">{$lang.footertext}</p>
                        </li>
                        <li class="bold">
                            <p><a href="{$privInfo.link}">{$privInfo.menu}</a></p>
                        </li>
                        <li>
                            <p> Copyright © 2018<br>
                                Website:
                                <a href="http://www.deeldrie.nl/" target="_blank">deeldrie</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row expanded">
                <div class="medium-12 columns flWrap">
                    <img src="{$siteLink}images/logoOlive.svg" alt="novole" class="footerLogo">
                </div>
            </div>
        </footer>