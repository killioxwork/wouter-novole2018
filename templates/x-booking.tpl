        <div id="bookingSection" class="home">
            <div class="row">
                <a href="{$contactInfo.link}" class="btn booking">
                    <i class="fa fa-calendar-check-o"></i>
                    {$lang.reservation}
                </a>
            </div>
        </div>