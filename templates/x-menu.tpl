        <nav id="menu">
            <ul>
                <li><a class="mm-title close-mm"><i class="fa fa-close"></i> MENU</a></li>
                <li class="menu-lang">
                    {$lang.languages}
                    <span>
        {section name=lKey loop=$lnInfo}
                        <span>
                            <a href="{$lnInfo[lKey].link}"{if $lnInfo[lKey].sel=='yes'} class="selected"{/if}>{$lnInfo[lKey].short}</a>
                        </span>
            {if !$smarty.section.lKey.last}
                        <span>·</span>
            {/if}
        {/section}
                    </span>
                </li>
                <li{if $homesel=='yes'} class="mm-selected"{/if}><a href="{$siteLink}{$selLang}/">Home</a></li>
        {section name=mmKey loop=$mmInfo}
                <li{if $mmInfo[mmKey].sel=='yes'} class="mm-selected"{/if}><a href="{$mmInfo[mmKey].link}">{$mmInfo[mmKey].title}</a></li>
        {/section}
            </ul>
            <a href="{$contactInfo.link}" class="btn booking onmenu">
                <i class="fa fa-calendar-check-o"></i>
                {$lang.reservation}
            </a>
            <p class="menuTA">{$lang.review}</p>
            <a href="{$tripadviser_link}" target="_blank" title="Trip Advisor" class="taOnMenu">
                <img src="{$siteLink}images/tripoAdviserMenu.png" alt="Trip Advisor" title="Trip Advisor">
            </a>
        </nav>