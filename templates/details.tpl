{include file="x-head.tpl"}
<body>
<div id="page">
  <div id="wrapper" class="relative">
{include file="x-header.tpl"}
    
    <!--Carousel-->
{include file="x-panoramic.tpl"}
    <!--/Carousel-->
    
    <div class="row">
      <div class="content{if $contactform=='yes'} contact{/if}">
        <h1>{$pInfo.title}</h1>
        {$pInfo.text}
{include file="x-photo-video.tpl"}

      </div>
    </div>
  </div>
{if $contactform=='yes'}
{include file="x-contactform.tpl"}
{/if}  
{include file="x-documents.tpl"}
{if $contactform!='yes'}
{include file="x-booking.tpl"}
{/if}
{include file="x-footer.tpl"}
  
  <!-- responsive menu -->
{include file="x-menu.tpl"}
</div>
{include file="x-cookie.tpl"}
</body>
{include file="x-headend.tpl"}