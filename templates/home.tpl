{include file="x-head.tpl"}
<body>
<div id="page">
  <div id="wrapper">
{include file="x-header.tpl"}
    <section class="row tagline">
        {if $pInfo.title!=''}<h1>{$pInfo.title}</h1>{/if}
    </section>
{section name=mKey loop=$mmInfo}
    <section class="homeMenuWrap"> <a href="{$mmInfo[mKey].link}">
      <div class="row homeMenuTextWrap">
        <div class="textPosition{if $mmInfo[mKey].style=='white'} colorWhite{/if}">
          <h2>{$mmInfo[mKey].title}</h2>
          <p>{$mmInfo[mKey].subtitle}</p>
        </div>
      </div>
      <img src="{$mmInfo[mKey].menuimage}" alt="{$mmInfo[mKey].title}" class="homeMenuImg"></a> </section>
{/section}
{if $pInfo.text!=''}
    <div class="row">
      <div class="content">{$pInfo.text}</div>
    </div>
{/if}
  </div>
{include file="x-booking.tpl"}
{include file="x-footer.tpl"}
  
  <!-- responsive menu -->
{include file="x-menu.tpl"}
</div>
{include file="x-cookie.tpl"}
</body>
{include file="x-headend.tpl"}