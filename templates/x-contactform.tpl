        <div id="contactFormSection">
            <div class="row">
        {if $contactResult=='created'}
                <div class="large-12 columns">
                    <div class="tnx-text">{$cInfo.text}</div>
                </div>
        {else}
                <form id="contactForm" class="contactForm" action="{$pInfo.link}" method="POST" novalidate>
                    <input type="hidden" name="action" value="contact" />
                    <div class="row">
                        <div class="large-12 columns">
                            <label>{$lang.formname} *
                                <input type="text" name="fullname" id="fullname" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>{$lang.formmail} *
                                <input type="text" name="email" id="email" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>{$lang.formphone}
                                <input type="text" name="phone" id="phone" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>{$lang.formcountry}
                                <input type="text" name="country" id="country" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>{$lang.formmsg}
                                <textarea name="text" id="text" /></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <button type="submit" class="btn btnSubmit">
                                <i class="fa fa-fighter-jet"></i> {$lang.formbutton}</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="prive">{$lang.formpolicy}</p>
                        </div>
                    </div>
                </form>
        {/if}
            </div>

        </div>