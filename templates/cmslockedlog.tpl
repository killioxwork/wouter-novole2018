{* Smarty *}
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Novole</title>
	<link rel="shortcut icon" href="{$siteLink}favicon.ico">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="{$siteLink}assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{$siteLink}assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="{$siteLink}assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{$siteLink}assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="{$siteLink}assets/images/logo.svg" alt="Novole"></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					<form action="index.php" class="form-validate" method="post" novalidate>
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object-dd"><img src="{$siteLink}assets/images/logo.svg" alt="Novole"></div>
								<h5 class="content-group">
								     Notification: Account Blocked
                                     <small class="display-block">For the ext 24 hours your ip will be blocked for logging on this site. For more info contact our administrator.</small>
                                </h5>
							</div>
						</div>
					</form>
					<!-- /password recovery -->


					<!-- Footer -->
					<div class="footer text-muted text-center"> &copy; Copyright 2018 · <a href="http://www.deeldrie.nl" target="_blank">deeldrie</a> </div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
