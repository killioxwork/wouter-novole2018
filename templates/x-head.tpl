{* Smarty *}
<!DOCTYPE html>
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="nl">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{$lang.headtitle}</title>
<link rel="shortcut icon" href="{$siteLink}favicon.ico" type="image/x-icon">
<link rel="icon" href="{$siteLink}favicon.ico" type="image/x-icon">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<!--
/**
 * @license
 * MyFonts Webfont Build ID 3367388, 2017-03-31T09:29:18-0400
 *
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are
 * explicitly restricted from using the Licensed Webfonts(s).
 *
 * You may obtain a valid license at the URLs below.
 *
 * Webfont: AvenirLT-Roman by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/55-roman/
 * Licensed pageviews: 250,000
 *
 * Webfont: AvenirLT-Heavy by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/85-heavy/
 * Licensed pageviews: 500,000
 *
 * Webfont: AvenirLT-Oblique by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/55-oblique/
 * Licensed pageviews: 500,000
 *
 * Webfont: AvenirLT-HeavyOblique by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/85-heavy-oblique/
 * Licensed pageviews: 500,000
 *
 *
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=3367388
 * Webfonts copyright: Part of the digitally encoded machine readable outline data for producing the Typefaces provided is copyrighted &#x00A9; 1981 - 2007 Linotype GmbH, www.linotype.com. All rights reserved. This software is the property of Linotype GmbH, and may not be repro
 *
 * © 2017 MyFonts Inc
*/

-->
<link rel="stylesheet" type="text/css" href="{$siteLink}fonts/Avenir.css"/>
<link rel="stylesheet" href="{$siteLink}css/grid.min.css">
<link rel="stylesheet" href="{$siteLink}css/jquery.mmenu.all.css">
<link rel="stylesheet" href="{$siteLink}css/style.css">
<link rel="stylesheet" type="text/css" href="{$siteLink}css/font-awesome.min.css">
{if $pageType=='details'}
<link rel="stylesheet" href="{$siteLink}css/owl.carousel.min.css">
<link rel="stylesheet" href="{$siteLink}css/owl.theme.default.min.css">
<link rel="stylesheet" href="{$siteLink}css/dataTables.foundation.min.css">
<link rel="stylesheet" href="{$siteLink}css/responsive.foundation.min.css">
{/if}
<script src="{$siteLink}js/vendor/jquery-1.9.1.min.js"></script>
{if $pageType=='details'}
<script src="{$siteLink}js/owl.carousel.min.js"></script>
{/if}
<script src="{$siteLink}js/vendor/foundation.min.js"></script>
<script src="{$siteLink}js/jquery.mmenu.all.min.js" type="text/javascript"></script>
{if $pageType=='details'}
<script src="{$siteLink}js/plugins.js"></script>
<script src="{$siteLink}js/vendor/jquery.dataTables.min.js"></script>
<script src="{$siteLink}js/vendor/dataTables.foundation.min.js"></script>
<script src="{$siteLink}js/vendor/dataTables.responsive.min.js"></script>
<script src="{$siteLink}js/vendor/responsive.foundation.min.js"></script>
<!--script src="{$siteLink}js/retina.min.js"></script-->
<!--script src="{$siteLink}js/remove-hover.min.js"></script-->
{/if}
<script src="{$siteLink}js/jquery.cookieBar.min.js"></script>
{if $contactform=='yes'}
<script src="{$siteLink}js/vendor/validation/validate.min.js"></script>
{/if}
<script src="{$siteLink}js/main.js"></script>
</head>