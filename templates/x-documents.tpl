{section name=dKey loop=$docInfo}
    {if $smarty.section.dKey.first}
    <div class="tableWrap">
            <div class="row">
            	<div class="content">
            		<div class="tbl-wrap">
                	<div class="row">
                                <div class="columns medium-12">
                                    <h3>Downloads</h3>
                                    <table class="download-tbl download-style display responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                            <tr>
                                                <th data-priority="1"></th>
                                                <th class="text-left"></th>
                                                <th data-priority="2" class="text-right"></th>
                                            </tr>
                                    </thead>
                                    <tbody>
    {/if}
                                        <tr>
                                            <td class="c1"><a href="{$docInfo[dKey].dlink}" target="_blank">{$docInfo[dKey].name}</a></td>
                                            <td class="c2"><i class="fa fa-{$docInfo[dKey].iconcode}"></i>{$docInfo[dKey].icontext}</td>
                                            <td class="c3">{$docInfo[dKey].size}</td>
                                        </tr>
    {if $smarty.section.dKey.last}
                                    </tbody>
                                </table>
                                </div>
                        </div>
                </div>
            	</div>
            </div>
    </div>
    {/if}
{/section}