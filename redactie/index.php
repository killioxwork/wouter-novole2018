<?php
    include_once ('../lib/includes.inc.php');

    require('../smarty3/Smarty.class.php');

    $smarty = new Smarty;

    $smarty->setTemplateDir('../templates/');
    $smarty->setCompileDir('../templates_c/');
    $smarty->setConfigDir('../configs/');
    $smarty->setCacheDir('../cache/');
    $smarty->assign('siteLink', website_url);

    $ipcheck = new protection_class();

    if($_GET['code']!=''){
        $ipcheck->cms_check_code($_GET['code']);
    }

    if($ipcheck->blocked_ip()){
        header("Location: lockedlog.php");
        exit();
    }
    if($ipcheck->white_ip()){
        header("Location: ".website_cms);
        exit();
    }
    if ($_POST['btn_login']=='log') {
        $l = $ipcheck->mailcheck($_POST['uname']);
        switch ($l) {
            case 0 : // mail
                $errors_text='<b>Check your email, a confirmation email has been sent</b><br /><br />
                              Wait for the mail to arrive in your inbox, and confirm it by pressing the confirmation link.';
                $hide = 'OK';
                break;
            case 1 : // Username missing
                $errors_text='Your username is missing.';
                break;
            case 2 : // User offline
                $errors_text='Your account appears to be offline. Contact the admin.';
                break;
            case 3 : // Username unknown
                $errors_text='This username is unknown.';
                break;
            case 4: // Username blocked
                header("Location: lockedlog.php");
                exit();
                break;
            case 5: // Mail sent in last 15 min
                $errors_text='An email was already sent to this address. There is a delay of 15 minutes before you can request for a new email';
                break;
            case 6: // Problem with sending mail
                $errors_text='<b>Oopss.. there is a problem!</b><br /><br />
                              It was not possible to send a confirmation
                              Email due to an issue with the sending of email.';
                $hide = 'OK';
                break;
        }

      $smarty->assign('logError' , $errors_text);
      $smarty->assign('formHide' , $hide);
    }

    $smarty->display("cmsipcheck.tpl");
?>